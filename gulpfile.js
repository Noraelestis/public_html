'use strict';

var gulp = require('gulp'),
    sass = require('gulp-sass'),
    rename = require("gulp-rename"),
    autoprefixer = require('gulp-autoprefixer'),
    sourcemaps = require('gulp-sourcemaps'),
    cleanCSS = require('gulp-clean-css'),
    browserify = require('gulp-browserify'),
    concat = require('gulp-concat'),
    gutil = require('gulp-util'),
    replace = require('gulp-replace'),
    jsmin = require('gulp-jsmin');

const path = {
    resources: 'resources/assets/',
    node_modules: 'node_modules/'
};

var layouts = {
    'svetograd-2.0': {
        name: 'svetograd-2.0',
        description: 'svetograd-2.0 theme'
    }
};

const list = {
    _cache: {
        styles: undefined,
        scripts: undefined
    },
    get styles() {
        if (typeof list._cache.styles === 'undefined') {
            list._cache.styles = [];
            var i = 0;
            for (var layout in layouts) {
                list._cache.styles[i] = 'sass-theme-' + layout;
                i++;
            }
        }
        return list._cache.styles;
    },
    get scripts() {
        if (typeof list._cache.scripts === 'undefined') {
            list._cache.scripts = [];
            var i = 0;
            for (var layout in layouts) {
                list._cache.scripts[i] = 'scripts-theme-' + layout;
                i++;
            }
        }
        return list._cache.scripts;
    },
    get copy() {
        if (typeof list._cache.copy === 'undefined') {
            list._cache.copy = [];
            var i = 0;
            for (var layout in layouts) {
                list._cache.copy[i] = 'copy-theme-' + layout;
                i++;
            }
        }
        return list._cache.copy;
    },
    path: function (theme) {
        return {
            css: 'themes/' + theme + '/assets/css/',
            js: 'themes/' + theme + '/assets/js/',
            font: 'themes/' + theme + '/assets/font/',
            img: 'themes/' + theme + '/assets/img/'
        };
    }
};

gulp.task('default', ['sass', 'scripts', 'copy']);

//themes tasks
for (var layout in layouts) {
    (function (layout) {
        //sass
        gulp.task('sass-theme-' + layout, function () {
            var pipe = gulp.src(path.resources + 'sass/themes/' + layout + '/main.scss');
            if (!gutil.env.production) {
                pipe = pipe.pipe(sourcemaps.init());
            }
            pipe.pipe(sass.sync().on('error', sass.logError));
            if (!gutil.env.production) {
                pipe = pipe.pipe(sourcemaps.write());
            } else {
                pipe = pipe.pipe(cleanCSS()).pipe(autoprefixer({
                    browsers: ['last 2 versions'],
                    cascade: false
                }));
            }
            pipe = pipe.pipe(rename('main.css'));
            return pipe.pipe(gulp.dest(list.path(layout).css));
        });
        //sass watch
        gulp.task('sass-theme-' + layout + ':watch', function () {
            gulp.watch([
                path.resources + 'sass/themes/' + layout + '/*.scss',
                path.resources + 'sass/themes/' + layout + '/*/*.scss',
                path.resources + 'sass/themes/' + layout + '/*/*/*.scss',
                path.resources + 'sass/themes/' + layout + '/*/*/*/*.scss',
                path.resources + 'sass/plugins/*',
                path.resources + 'sass/plugins/*/*',
                path.resources + 'sass/plugins/*/*/*',
                path.resources + 'sass/plugins/*/*/*/*',
                path.resources + 'sass/libs/*',
                path.resources + 'sass/libs/*/*',
                path.resources + 'sass/libs/*/*/*',
                path.resources + 'sass/libs/*/*/*/*'
            ], ['sass-theme-' + layout]);
        });
        //scripts
        gulp.task('scripts-theme-' + layout, function () {
            var pipe = gulp.src([path.resources + 'js/themes/' + layout + '.js'])
                .pipe(browserify({
                    insertGlobals: true
                }));
            if (gutil.env.production) {
                pipe = pipe.pipe(jsmin());
            }
            pipe.pipe(rename({basename: 'app'}))
                .pipe(gulp.dest(list.path(layout).js));
        });
        //scripts watch
        gulp.task('scripts-theme-' + layout + ':watch', function () {
            gulp.watch([
                path.resources + 'js/themes/' + layout + '.js',
                path.resources + 'js/partials/*.js',
                path.resources + 'js/partials/*/*.js',
                path.resources + 'js/partials/*/*/*.js',
                path.resources + 'js/libs/*.js',
                path.resources + 'js/libs/*/*.js',
                path.resources + 'js/libs/*/*/*.js'
            ], ['scripts-theme-' + layout]);
        });
        //copy assets path
        gulp.task('copy-theme-' + layout, function () {
            gulp.src(path.resources + 'fontello/font/**/*.{ttf,woff,woff2,eot,svg}')
                .pipe(gulp.dest(list.path(layout).font));
        });
    })(layout);
}

gulp.task('sass', list.styles);
gulp.task('scripts', list.scripts);
gulp.task('copy', list.copy);

gulp.task('sass:watch', function () {
    gulp.watch([
        path.resources + 'sass/_variables.scss',
        path.resources + 'sass/*/*.scss',
        path.resources + 'sass/*/*/*.scss',
        path.resources + 'sass/*/*/*/*.scss',
        path.resources + 'sass/*/*/*/*/*.scss'
    ], ['sass']);
});

gulp.task('scripts:watch', function () {
    gulp.watch([path.resources + 'js/*.js', path.resources + 'js/*/*.js', path.resources + 'js/*/*/*.js', path.resources + 'js/*/*/*/*.js'], ['scripts']);
});

gulp.task('watch', ['scripts:watch', 'sass:watch']);
