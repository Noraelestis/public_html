<?php namespace Austerus\Https;

use Austerus\Https\Models\Settings;
use System\Classes\PluginBase;
use System\Classes\SettingsManager;

/**
 * Https Plugin Information File
 */
class Plugin extends PluginBase
{

    /**
     * The URIs that should be excluded from force SSL.
     *
     * @var array
     */
    protected $except = [];

    /**
     * The application environment that should be excluded from force SSL.
     *
     * @var array
     */
    protected $exceptEnv = [];

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name' => 'Forse Https',
            'description' => 'Change http to https',
            'author' => 'Austerus',
            'icon' => 'icon-leaf'
        ];
    }

    /**
     * Boot method, called right before the request route.
     */
    public function boot()
    {
        $settings = Settings::instance();
        if ($settings->is_enabled) {
            \App::before(function ($request) {
                // Set all urls schemas equals to "https://" if HTTP_X_FORWARDED_PROTO is passed by nginx
                if (!empty($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == "https") {
                    $this->app['url']->forceScheme("https");
                }
                if (!$request->secure() && !$this->shouldPassThrough($request) && !$this->envPassThrough()) {

                    $secureUrl = 'https://' . $request->getHttpHost() . $request->getRequestUri();
                    return redirect($secureUrl);
                }
                return null;
            });
        }
    }

    /**
     * Register the settings.
     *
     * @return array
     */
    public function registerSettings()
    {
        return [
            'settings' => [
                'label' => 'Https',
                'description' => 'Включение/отключение https протокола',
                'category' => SettingsManager::CATEGORY_CMS,
                'icon' => 'icon-shield',
                'class' => 'Austerus\Https\Models\Settings',
                'order' => 1000
            ]

        ];
    }

    /**
     * @param $request
     * @return bool
     */
    protected function shouldPassThrough($request)
    {
        foreach ($this->except as $except) {
            if ($request->is($except)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return bool
     */
    protected function envPassThrough()
    {
        $appEnv = \App::environment();

        foreach ($this->exceptEnv as $except) {
            if ($appEnv === $except)
                return true;
        }

        return false;
    }
}
