<?php
namespace Austerus\Https\Models;

use Model;

/**
 * Settings Model
 */
class Settings extends Model
{
    public $implement = ['System.Behaviors.SettingsModel'];

    public $settingsCode = 'force_https_settings';

    public $settingsFields = 'fields.yaml';
}