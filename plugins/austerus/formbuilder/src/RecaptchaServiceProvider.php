<?php

namespace austerus\FormBuilder;

use Session;
use austerus\FormBuilder\Models\Settings;
use Validator;
use Illuminate\Support\ServiceProvider;
use ReCaptcha\ReCaptcha;
use Request;

/**
 * Class RecaptchaServiceProvider
 * @package austerus\FormBuilder
 */
class RecaptchaServiceProvider extends ServiceProvider
{

    /**
     * @return void
     */
    public function boot()
    {
        Validator::extend('recaptcha', function ($attribute, $value, $parameters) {
            if (Session::has('recaptcha')) {
                $response = Session::get('recaptcha');
                Session::reflash();
            } else {
                $recaptcha = new ReCaptcha(Settings::get('secret_key'));

                $response = $recaptcha->verify($value, Request::ip());

                Session::flash('recaptcha', $response);
            }

            return $response->isSuccess();

        }, trans('austerus.formbuilder::lang.recaptcha.error'));
    }

    /**
     * @inheritdoc
     */
    public function register()
    {
    }

}
