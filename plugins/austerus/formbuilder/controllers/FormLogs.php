<?php

namespace austerus\FormBuilder\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use austerus\FormBuilder\Models\FormLog;

/**
 * Class FormLogs
 * @package austerus\FormBuilder\Controllers
 */
class FormLogs extends Controller
{

    /**
     * @var array
     */
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
        'austerus.FormBuilder.Behaviors.FormLogController'
    ];

    /**
     * @var array
     */
    public $requiredPermissions = ['austerus.formbuilder.access_formlogs'];

    /**
     * @var string
     */
    public $formConfig = 'config_form.yaml';

    /**
     * @var string
     */
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();
        $this->addCss('/plugins/austerus/formbuilder/assets/css/logger.css');

        BackendMenu::setContext('austerus.FormBuilder', 'formbuilder', 'formlogs');
    }

    /**
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Symfony\Component\HttpFoundation\Response
     */
    public function preview_html()
    {
        $log = FormLog::find($this->params[0]);

        return response($log->content_html);
    }

}
