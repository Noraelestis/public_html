<?php

namespace austerus\FormBuilder\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Class FieldTypes
 * @package austerus\FormBuilder\Controllers
 */
class FieldTypes extends Controller
{

    /**
     * @var array
     */
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
        'austerus.FormBuilder.Behaviors.FieldTypeController'
    ];

    /**
     * @var array
     */
    public $requiredPermissions = ['austerus.formbuilder.access_fieldtypes'];

    /**
     * @var string
     */
    public $formConfig = 'config_form.yaml';

    /**
     * @var string
     */
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('austerus.FormBuilder', 'formbuilder', 'fieldtypes');
    }

}
