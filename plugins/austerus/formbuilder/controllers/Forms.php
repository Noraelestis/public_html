<?php

namespace austerus\FormBuilder\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use austerus\FormBuilder\Models\Field;


/**
 * Class Forms
 * @package austerus\FormBuilder\Controllers
 */
class Forms extends Controller
{

    /**
     * @var array Behaviors
     */
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
        'Backend.Behaviors.RelationController',
        'austerus.FormBuilder.Behaviors.FormController'
    ];

    /**
     * @var array
     */
    public $requiredPermissions = ['austerus.formbuilder.access_forms'];

    /**
     * @var string
     */
    public $formConfig = 'config_form.yaml';

    /**
     * @var string
     */
    public $listConfig = 'config_list.yaml';

    /**
     * @var string
     */
    public $relationConfig = 'config_relation.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('austerus.FormBuilder', 'formbuilder', 'forms');
    }

    /**
     * @param $formId
     */
    public function reorder($formId)
    {
        $this->pageTitle = trans('austerus.formbuilder::lang.fields.reorder');

        $toolbarConfig = $this->makeConfig();
        $toolbarConfig->buttons = '$/austerus/formbuilder/controllers/forms/_reorder_toolbar.htm';

        $this->vars['toolbar'] = $this->makeWidget('Backend\Widgets\Toolbar', $toolbarConfig);
        $this->vars['records'] = Field::where('form_id', $formId)->get();
        $this->vars['formId'] = $formId;
    }

}
