<?php

namespace austerus\FormBuilder\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

/**
 * Class AddCssClassColumnToFormsTable
 * @package austerus\FormBuilder\Updates
 */
class AddCssClassColumnToFormsTable extends Migration
{

    /**
     * @return void
     */
    public function up()
    {
        Schema::table('austerus_formbuilder_forms', function ($table) {
            $table->string('css_class')->nullable();
        });
    }

    /**
     * @return void
     */
    public function down()
    {
        Schema::table('austerus_formbuilder_forms', function ($table) {
            $table->dropColumn('css_class');
        });
    }

}
