<?php

namespace austerus\FormBuilder\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

/**
 * Class AddCommentColumnToFieldsTable
 * @package austerus\FormBuilder\Updates
 */
class AddCommentColumnToFieldsTable extends Migration
{

    /**
     * @return void
     */
    public function up()
    {
        Schema::table('austerus_formbuilder_fields', function ($table) {
            $table->string('comment')->after('validation')->nullable();
        });
    }

    /**
     * @return void
     */
    public function down()
    {
        Schema::table('austerus_formbuilder_fields', function ($table) {
            $table->dropColumn('comment');
        });
    }

}
