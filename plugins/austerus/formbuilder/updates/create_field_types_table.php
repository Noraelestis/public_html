<?php

namespace austerus\FormBuilder\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

/**
 * Class CreateFieldTypesTable
 * @package austerus\FormBuilder\Updates
 */
class CreateFieldTypesTable extends Migration
{

    /**
     * @return void
     */
    public function up()
    {
        Schema::create('austerus_formbuilder_field_types', function ($table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 100);
            $table->text('description')->nullable();
            $table->string('code', 100)->unique();
            $table->text('markup')->nullable();
            $table->timestamps();
        });
    }

    /**
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('austerus_formbuilder_field_types');
    }

}
