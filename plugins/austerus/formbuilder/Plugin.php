<?php

namespace austerus\FormBuilder;

use System\Classes\PluginBase;
use Backend;
use App;
use Event;

/**
 * Class Plugin
 * @package austerus\FormBuilder
 */
class Plugin extends PluginBase
{

    /**
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'austerus.formbuilder::lang.plugin.name',
            'description' => 'austerus.formbuilder::lang.plugin.description',
            'author'      => 'austerus',
            'icon'        => 'icon-code'
        ];
    }

    /**
     * @inheritdoc
     */
    public function boot()
    {
        App::register('austerus\FormBuilder\RecaptchaServiceProvider');

        Event::listen('formBuilder.sendMessage', 'austerus\FormBuilder\Listeners\FormEmail');
        Event::listen('mailer.send', 'austerus\FormBuilder\Listeners\EmailSend');
    }

    /**
     * @return array
     */
    public function registerNavigation()
    {
        return [
            'formbuilder' => [
                'label'       => 'austerus.formbuilder::lang.navigation.formbuilder',
                'url'         => Backend::url('austerus/formbuilder/forms'),
                'icon'        => 'icon-check-square-o',
                'permissions' => ['austerus.formbuilder.*'],
                'order'       => 500,
                'sideMenu'    => [
                    'forms'      => [
                        'label'       => 'austerus.formbuilder::lang.navigation.forms',
                        'icon'        => 'icon-check-square-o',
                        'url'         => Backend::url('austerus/formbuilder/forms'),
                        'permissions' => ['austerus.formbuilder.access_forms']
                    ],
                    'formlogs'   => [
                        'label'       => 'austerus.formbuilder::lang.navigation.formlogs',
                        'icon'        => 'icon-archive',
                        'url'         => Backend::url('austerus/formbuilder/formlogs'),
                        'permissions' => ['austerus.formbuilder.access_formlogs']
                    ],
                    'fieldtypes' => [
                        'label'       => 'austerus.formbuilder::lang.navigation.fieldtypes',
                        'icon'        => 'icon-code',
                        'url'         => Backend::url('austerus/formbuilder/fieldtypes'),
                        'permissions' => ['austerus.formbuilder.access_fieldtypes']
                    ]
                ]
            ]
        ];
    }

    /**
     * @return array
     */
    public function registerPermissions()
    {
        return [
            'austerus.formbuilder.access_settings'   => [
                'label' => 'austerus.formbuilder::lang.permissions.access_settings',
                'tab'   => 'austerus.formbuilder::lang.permissions.tab'
            ],
            'austerus.formbuilder.access_forms'      => [
                'label' => 'austerus.formbuilder::lang.permissions.access_forms',
                'tab'   => 'austerus.formbuilder::lang.permissions.tab'
            ],
            'austerus.formbuilder.access_formlogs'   => [
                'label' => 'austerus.formbuilder::lang.permissions.access_formlogs',
                'tab'   => 'austerus.formbuilder::lang.permissions.tab'
            ],
            'austerus.formbuilder.access_fieldtypes' => [
                'label' => 'austerus.formbuilder::lang.permissions.access_fieldtypes',
                'tab'   => 'austerus.formbuilder::lang.permissions.tab'
            ]
        ];
    }

    /**
     * @return array
     */
    public function registerSettings()
    {
        return [
            'settings' => [
                'label'       => 'austerus.formbuilder::lang.settings.label',
                'description' => 'austerus.formbuilder::lang.settings.description',
                'category'    => 'austerus.formbuilder::lang.settings.category',
                'icon'        => 'icon-google',
                'class'       => 'austerus\FormBuilder\Models\Settings',
                'order'       => 500,
                'keywords'    => 'form builder contact',
                'permissions' => ['austerus.formbuilder.access_settings']
            ]
        ];
    }

    /**
     * @return array
     */
    public function registerComponents()
    {
        return [
            'austerus\FormBuilder\Components\RenderForm' => 'renderForm',
        ];
    }

    /**
     * @return array
     */
    public function registerPageSnippets()
    {
        return [
            'austerus\FormBuilder\Components\RenderForm' => 'renderForm',
        ];
    }

    /**
     * @return array
     */
    public function registerMarkupTags()
    {
        if (class_exists('RainLab\Location\Behaviors\LocationModel')) {
            return;
        }

        return [
            'functions' => [
                'form_select_country' => function () {},
                'form_select_state'   => function () {}
            ]
        ];
    }

}
