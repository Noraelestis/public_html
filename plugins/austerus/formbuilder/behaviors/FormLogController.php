<?php

namespace austerus\FormBuilder\Behaviors;

use Flash;
use austerus\FormBuilder\Models\FormLog;

/**
 * Class FormLogController
 * @package austerus\FormBuilder\Behaviors
 */
class FormLogController extends BaseController
{

    /**
     * @return void
     */
    protected function deleteChecked()
    {
        foreach (post('checked') as $logId) {
            if ( ! $log = FormLog::find($logId)) {
                continue;
            }

            $log->delete();
        }

        Flash::success(trans('austerus.formbuilder::lang.logs.delete_success'));
    }

    /**
     * @return string
     */
    protected function getEmptyCheckMessage()
    {
        return trans('austerus.formbuilder::lang.logs.delete_empty');
    }

}
