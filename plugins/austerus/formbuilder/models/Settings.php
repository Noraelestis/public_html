<?php

namespace austerus\FormBuilder\Models;

use Model;

/**
 * Class Settings
 * @package austerus\FormBuilder\Models
 */
class Settings extends Model
{

    /**
     * @var array
     */
    public $implement = ['System.Behaviors.SettingsModel'];

    /**
     * @var string
     */
    public $settingsCode = 'austerus_formbuilder_settings';

    /**
     * @var string
     */
    public $settingsFields = 'fields.yaml';

}
