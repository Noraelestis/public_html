<?php

namespace austerus\FormBuilder\Models;

use Model;
use October\Rain\Database\Traits\Validation;

/**
 * Class FieldType
 * @package austerus\FormBuilder\Models
 */
class FieldType extends Model
{

    use Validation;

    /**
     * @var array
     */
    public $implement = ['@RainLab.Translate.Behaviors.TranslatableModel'];


    /**
     * @var string
     */
    public $table = 'austerus_formbuilder_field_types';

    /**
     * @var array
     */
    public $rules = [
        'name' => 'required|max:100',
        'code' => 'required|unique:austerus_formbuilder_field_types|max:100'
    ];

    /**
     * @var array
     */
    public $attributeNames = [
        'name' => 'austerus.formbuilder::lang.field.name',
        'code' => 'austerus.formbuilder::lang.field.code'
    ];

    /**
     * @var array
     */
    protected $fillable = ['name', 'code', 'description', 'markup'];

    /**
     * @var array
     */
    public $translatable = ['name', 'description'];

    /**
     * @param $code
     * @return bool
     */
    public static function isOriginType($code)
    {
        return in_array($code, [
            'text',
            'textarea',
            'dropdown',
            'checkbox',
            'checkbox_list',
            'radio_list',
            'recaptcha',
            'submit',
            'country_select',
            'state_select',
            'section'
        ]);
    }

}
