<?php

namespace austerus\FormBuilder\Models;

use Model;
use October\Rain\Database\Traits\Sortable;
use October\Rain\Database\Traits\Validation;
use Twig;

/**
 * Class Section
 * @package austerus\FormBuilder\Models
 */
class Section extends Model
{

    use Validation;
    use Sortable;

    /**
     * @var array
     */
    public $implement = ['@RainLab.Translate.Behaviors.TranslatableModel'];

    /**
     * @var string
     */
    public $table = 'austerus_formbuilder_sections';

    /**
     * @var array
     */
    public $rules = [
        'label' => 'max:255',
        'name'  => 'required|max:255',
        'class' => 'max:255',
    ];

    /**
     * @var array
     */
    public $attributeNames = [
        'label' => 'austerus.formbuilder::lang.field.label',
        'name'  => 'austerus.formbuilder::lang.field.name',
        'class' => 'austerus.formbuilder::lang.field.class',
    ];

    /**
     * @var array
     */
    protected $fillable = ['label', 'name', 'class'];

    /**
     * @var array
     */
    public $translatable = ['label'];

    /**
     * @var array
     */
    public $belongsTo = [
        'form' => ['austerus\FormBuilder\Models\Form'],
    ];

    /**
     * @var array
     */
    public $hasMany = [
        'fields' => ['austerus\FormBuilder\Models\Field'],
    ];

    /**
     * @param $query
     * @return mixed
     */
    public function scopeIsVisible($query)
    {
        return $query->where('is_visible', true);
    }

    /**
     * @return mixed
     */
    public function getHtmlAttribute()
    {
        $fieldType = FieldType::where('code', 'section')->remember(1)->first();

        return Twig::parse($fieldType->markup, $this->prepareFieldOptions());
    }

    /**
     * @return array
     */
    private function prepareFieldOptions()
    {
        return [
            'label'         => $this->label,
            'name'          => $this->name,
            'class'         => $this->class,
            'wrapper_begin' => $this->wrapper_begin,
            'wrapper_end'   => $this->wrapper_end,
            'fields'        => $this->fields->implode('html'),
        ];
    }

}
