<?php namespace Austerus\GeoApi;

use Austerus\Geoapi\Components\CitiesList;
use Austerus\GeoApi\Components\GeoApi;
use Backend;
use System\Classes\PluginBase;

/**
 * GeoApi Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'GeoApi',
            'description' => 'Предоставляет Api для создания геозависимых элементов',
            'author'      => 'Austerus',
            'icon'        => 'icon-globe'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return [
            GeoApi::class => 'GeoApi',
            CitiesList::class => 'CitiesList',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'austerus.geoapi.some_permission' => [
                'tab' => 'GeoApi',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return [
            'geoapi' => [
                'label'       => 'GeoApi',
                'url'         => Backend::url('austerus/geoapi/cities'),
                'icon'        => 'icon-globe',
                'permissions' => ['austerus.geoapi.*'],
                'order'       => 600,
                'sideMenu' => [
                    'cities' => [
                        'label' => 'Города',
                        'icon' => 'icon-map-marker',
                        'url' => Backend::url('austerus/geoapi/cities'),
                        'permissions' => ['austerus.geoapi.*'],
                    ],
                    'properties' => [
                        'label' => 'Фразы',
                        'icon' => 'icon-commenting',
                        'url' => Backend::url('austerus/geoapi/properties'),
                        'permissions' => ['austerus.geoapi.*'],
                    ],
                    'propertyValues' => [
                        'label' => 'Значения',
                        'icon' => 'icon-exclamation',
                        'url' => Backend::url('austerus/geoapi/propertyvalues'),
                        'permissions' => ['austerus.geoapi.*'],
                    ],
                    'help' => [
                        'label' => 'Справка',
                        'icon' => 'icon-question',
                        'url' => Backend::url('austerus/geoapi/help'),
                        'permissions' => ['austerus.geoapi.*'],
                    ],
                    'exchange' => [
                        'label' => 'Импорт/Экспорт',
                        'icon' => 'icon-file-excel-o',
                        'url' => Backend::url('austerus/geoapi/exchange'),
                        'permissions' => ['austerus.geoapi.*'],
                    ],
                ]
            ],
        ];
    }

    /**
     * Register twig extensions
     * @return array
     */
    public function registerMarkupTags()
    {
        return [
            'functions' => [
                'priceFormatting' => function ($val) {
                    return number_format($val, 0, '', ' ');
                },
                'phoneClear' => function($val) {
                    return preg_replace('/[^\d^+]/', '', $val);
                }
            ]
        ];
    }
}
