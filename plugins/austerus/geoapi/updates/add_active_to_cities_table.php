<?php namespace Austerus\GeoApi\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;


class AddActiveToCitiesTable extends Migration
{
    public function up()
    {
        Schema::table('austerus_geoapi_cities', function (Blueprint $table) {
            $table->boolean('active')->default(false);
        });
    }

    public function down()
    {
        Schema::table('austerus_geoapi_cities', function (Blueprint $table) {
            $table->dropColumn('active');
        });
    }
}