<?php namespace Austerus\GeoApi\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreatePropertyValueTable extends Migration
{
    public function up()
    {
        Schema::create('austerus_geoapi_property_values', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->text('value')->nullable();
            $table->integer('city_id')->unsigned();
            $table->integer('property_id')->unsigned();
            $table->timestamps();
            $table->foreign('city_id')->references('id')->on('austerus_geoapi_cities');
            $table->foreign('property_id')->references('id')->on('austerus_geoapi_properties');
        });
        Schema::dropIfExists('austerus_geoapi_property_texts');
        Schema::dropIfExists('austerus_geoapi_property_strings');
    }

    public function down()
    {
        Schema::dropIfExists('austerus_geoapi_property_values');

        Schema::create('austerus_geoapi_property_strings', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('value')->nullable();
            $table->integer('city_id')->unsigned();
            $table->integer('property_id')->unsigned();
            $table->timestamps();
            $table->foreign('city_id')->references('id')->on('austerus_geoapi_cities');
            $table->foreign('property_id')->references('id')->on('austerus_geoapi_properties');
        });

        Schema::create('austerus_geoapi_property_texts', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('value')->nullable();
            $table->integer('city_id')->unsigned();
            $table->integer('property_id')->unsigned();
            $table->timestamps();
            $table->foreign('city_id')->references('id')->on('austerus_geoapi_cities');
            $table->foreign('property_id')->references('id')->on('austerus_geoapi_properties');
        });

    }
}
