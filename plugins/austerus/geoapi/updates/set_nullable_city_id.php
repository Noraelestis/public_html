<?php namespace Austerus\GeoApi\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class SetNullableCityId extends Migration
{
    public function up()
    {
        Schema::table('austerus_geoapi_property_strings', function(Blueprint $table) {
            $table->integer('city_id')->unsigned()->nullable()->change();
        });

        Schema::table('austerus_geoapi_property_texts', function(Blueprint $table) {
            $table->integer('city_id')->unsigned()->nullable()->change();
        });

    }

    public function down()
    {

    }
}
