<?php namespace Austerus\GeoApi\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class SetNullableCityIdToValues extends Migration
{
    public function up()
    {
        Schema::table('austerus_geoapi_property_values', function(Blueprint $table) {
            $table->integer('city_id')->unsigned()->nullable()->change();
        });

    }

    public function down()
    {

    }
}
