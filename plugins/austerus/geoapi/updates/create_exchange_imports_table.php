<?php namespace Austerus\GeoApi\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateExchangeImportsTable extends Migration
{
    public function up()
    {
        Schema::create('austerus_geoapi_exchange_imports', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('austerus_geoapi_exchange_imports');
    }
}
