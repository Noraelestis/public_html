<?php namespace Austerus\GeoApi\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateCitiesTable extends Migration
{
    public function up()
    {
        Schema::create('austerus_geoapi_cities', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('domain')->unique();
            $table->string('name');
            $table->string('prepositional');
            $table->string('options', 510);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('austerus_geoapi_cities');
    }
}
