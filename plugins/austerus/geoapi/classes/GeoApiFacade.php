<?php namespace Austerus\GeoApi\Classes;

use Austerus\GeoApi\Components\GeoApi;
use Austerus\GeoApi\Libs\IpGeoBase\IpGeoBase;
use Austerus\GeoApi\Models\City;
use Austerus\GeoApi\Models\Property;
use Illuminate\Database\Eloquent\Collection;
use Twig_Environment;
use Twig_Loader_Array;

class GeoApiFacade
{
    /**
     * @var null|$this
     */
    private static $instance = null;

    /**
     * @var Collection
     */
    protected $properties;

    /**
     * @var array
     */
    protected $propertiesArr = [];

    /**
     * Current sub domein
     * @var string|null
     */
    protected $subDomain = null;

    /**
     * Current main domain
     * @var null|string
     */
    protected $domain = null;

    /**
     * Current domain zone
     * @var null|string
     */
    protected $zone = null;

    /**
     * Current city
     * @var City|null
     */
    protected $city = null;

    /**
     * Ip located
     * @var City|null
     */
    protected $locatedCity = null;

    /**
     * Forbid clone facade for Singleton
     */
    private function __clone()
    {
    }

    /**
     * Forbid construct facade for Singleton
     */
    private function __construct()
    {
    }

    /**
     * Init facade
     */
    public function init()
    {
        $domain = explode('.', \Request::getHost());
        //todo change tp reg
        if (count($domain) > 2) {
            $city = City::getForDomain($domain[0]);
            if ($city !== null) {
                $this->subDomain = $domain[0];
                $this->domain = $domain[1];
                $this->zone = $domain[2];
                $this->city = $city;
                $this->properties = Property::getValues($city);
                $this->propertiesArr = $this->properties->toArray();
            }
        } else {
            $this->domain = $domain[0];
            $this->zone = $domain[1];
            $this->properties = Property::getValues();
            $this->propertiesArr = $this->properties->toArray();
            $geoBase = new IpGeoBase();
            $geoBase = $geoBase->getRecord(request()->ip());
            if (isset($geoBase['city'])) {
                $this->locatedCity = City::whereName($geoBase['city'])->first();
            }
        }
        $this->propertiesArr['GeoApi'] = new GeoApi();
        $this->propertiesArr['GeoApi']->onRun();
    }

    /**
     * @return GeoApiFacade
     */
    public static function getInstance()
    {
        if (static::$instance === null) {
            static::$instance = new static();
            static::$instance->init();
        }
        return static::$instance;
    }

    /**
     * @return mixed
     */
    public static function getProperties()
    {
        return static::getInstance()->properties;
    }

    /**
     * @return mixed
     */
    public static function getPropertiesArr()
    {
        return static::getInstance()->propertiesArr;
    }

    /**
     * @return City|null
     */
    public static function getCity()
    {
        return static::getInstance()->city;
    }

    /**
     * Return geo city
     * @return City|null
     */
    public static function getLocatedCity()
    {
        if (static::getInstance()->locatedCity !== null) {
            return static::getInstance()->locatedCity;
        } else return static::getInstance()->city;
    }

    /**
     * @return mixed
     */
    public static function getSubDomain()
    {
        return static::getInstance()->subDomain;
    }

    /**
     * @param bool $withZone
     * @return string|null
     */
    public static function getDomain(bool $withZone = false)
    {
        $domain = static::getInstance()->domain;
        if ($withZone) {
            $domain = $domain . '.' . static::getInstance()->zone;
        }
        return $domain;
    }

    /**
     * @param $string
     * @return string
     */
    public static function renderString($string)
    {
        try {
            $twig = new Twig_Environment(new Twig_Loader_Array(), ['cache' => false]);
            $template = $twig->createTemplate($string);
            return $template->render(static::getInstance()->propertiesArr);
        } catch (\Exception $e) {
            return $string;
        }
    }
}