<?php namespace Austerus\GeoApi\Classes;

use October\Rain\Database\Model;

/**
 * Class Presenter
 * @package App\Extensions
 */
abstract class Presenter
{
    /**
     * @var Model
     */
    protected $model;

    /**
     * Presenter constructor.
     * @param Model $model
     */
    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    /**
     * @param $method
     * @param $args
     * @return mixed
     */
    public function __call($method, $args)
    {
        return call_user_func_array([$this->model, $method], $args);
    }

    /**
     * @param $name
     * @return mixed
     */
    public function __get($name)
    {
        return $this->model->{$name};
    }

    /**
     * Determine if an attribute or relation exists on the model.
     *
     * @param  string $key
     * @return bool
     */
    public function __isset($key)
    {
        if (isset($this->model->attributes[$key]) || isset($this->model->relations[$key])) {
            return true;
        }

        if (method_exists($this->model, $key) && $this->model->$key && isset($this->model->relations[$key])) {
            return true;
        }

        return $this->model->hasGetMutator($key) && !is_null($this->model->getAttributeValue($key));
    }

    /**
     * @return Model
     */
    public function getModel()
    {
        return $this->model;
    }
}