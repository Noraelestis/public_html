<?php namespace Austerus\GeoApi\Components;

use Austerus\GeoApi\Classes\GeoApiFacade;
use Austerus\GeoApi\Models\City;
use Cms\Classes\ComponentBase;

class GeoApi extends ComponentBase
{
    /**
     * Current city
     * @var City
     */
    public $city;

    /**
     * Store full facade
     * @var GeoApiFacade
     */
    public $api;

    /**
     * Details for cms
     * @return array
     */
    public function componentDetails()
    {
        return [
            'name' => 'GeoApi',
            'description' => 'Предоставляет гео данный для вывода на странице'
        ];
    }

    /**
     * Config current component
     *
     * @return array
     */
    public function defineProperties()
    {
        return [];
    }

    /**
     * Run plugin
     */
    public function onRun()
    {
        $this->api = GeoApiFacade::getInstance();
        $this->city = GeoApiFacade::getCity();
    }

    /**
     * Render plugin
     */
    public function onRender()
    {
        $this->page->title = $this->api->renderString($this->page->title);
        $this->page->meta_title = $this->api->renderString($this->page->meta_title);
        $this->page->meta_description = $this->api->renderString($this->page->meta_description);
        $this->page->seo_keywords = $this->api->renderString($this->page->seo_keywords);
        return '';
    }

    public function ifCity($string, $default = '')
    {
        if ($this->city === null) {
            return $default;
        }
        return $string;
    }

    public function ifNotCity($string, $default = '')
    {
        if ($this->city === null) {
            return $string;
        }
        return $default;
    }

    /**
     * @param $property
     * @param string $default
     * @return string
     */
    public function p($property, $default = '')
    {
        $property = $this->getProperty($property);
        if (!empty($property)) {
            return $property;
        }
        return $default;
    }

    /**
     * @param string $prefix
     * @param string $postfix
     * @param string $field
     * @return string
     */
    public function cityName($prefix = '', $postfix = '', $field = 'name')
    {
        if ($this->city !== null) {
            return $prefix . $this->city->$field . $postfix;
        }
        return '';
    }

    /**
     * @param string $prefix
     * @param string $postfix
     * @return string
     */
    public function cityPrepositional($prefix = '', $postfix = '')
    {
        if ($this->city !== null) {
            return $prefix . $this->city->prepositional . $postfix;
        }
        return '';
    }

    /**
     * @param string $prefix
     * @param $property
     * @param string $postfix
     * @param string $default
     * @param bool $defWrap
     * @return string
     */
    public function render($prefix = '', $property, $postfix = '', $default = '', $defWrap = true)
    {
        if (!empty($this->getProperty($property))) {
            return $prefix . $this->getProperty($property) . $postfix;
        } elseif ($defWrap) {
            return $prefix . $default . $postfix;
        } else {
            return $default;
        }
    }

    /**
     * @param $name
     * @return mixed
     */
    public function __get($name)
    {
        return $this->getProperty($name);
    }

    /**
     * Determine if an attribute or relation exists on the model.
     *
     * @param  string $key
     * @return bool
     */
    public function __isset($key)
    {
        return ($this->getProperty($key) !== null);
    }

    /**
     * @param $property
     * @return null
     */
    protected function getProperty($property)
    {
        if (GeoApiFacade::getProperties() !== null) {
            $property = GeoApiFacade::getProperties()->get($property);
            if ($property !== null) {
                return $property;
            }
        }
        return null;
    }
}
