<?php namespace Austerus\Geoapi\Components;

use Austerus\GeoApi\Classes\GeoApiFacade;
use Austerus\GeoApi\Libs\IpGeoBase\IpGeoBase;
use Austerus\GeoApi\Models\City;
use Cms\Classes\ComponentBase;

class CitiesList extends ComponentBase
{

    /**
     * Returns information about this component, including name and description.
     */
    public function componentDetails()
    {
        return [
            'name' => 'Список городов',
            'description' => 'Выводит список городов в указанный шаблон'
        ];
    }

    /**
     * Defines the properties used by this class.
     * This method should be used as an override in the extended class.
     */
    public function defineProperties()
    {
        return [
            'partial' => [
                'title' => 'Фрагмент',
                'description' => 'Фрагемент для вывода компонента',
                'default' => 'geoapi/_cities_list',
                'type' => 'text'
            ],
        ];
    }

    /**
     * Executed when this component is bound to a page or layout, part of
     * the page life cycle.
     */
    public function onRun()
    {
    }

    /**
     * Executed when this component is rendered on a page or layout.
     */
    public function onRender()
    {
        return $this->renderPartial($this->property('partial'), ['cities' => City::getActiveOnly(), 'city' => GeoApiFacade::getLocatedCity()]);
    }
}
