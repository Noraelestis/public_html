<?php namespace Austerus\GeoApi\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

/**
 * Help Back-end Controller
 */
class Help extends Controller
{
    public $implement = [];

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Austerus.GeoApi', 'geoapi', 'help');
    }

    public function index()
    {
        $this->pageTitle = 'Документация для GeoApi';
    }

}
