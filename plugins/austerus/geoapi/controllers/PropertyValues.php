<?php namespace Austerus\GeoApi\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Property Strings Back-end Controller
 */
class PropertyValues extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Austerus.GeoApi', 'geoapi', 'propertyvalue');
    }
}
