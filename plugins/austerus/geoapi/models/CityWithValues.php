<?php namespace Austerus\GeoApi\Models;


/**
 * Class CityWithValues
 * @package Austerus\GeoApi\Models
 */
class CityWithValues extends City
{

    /**
     * Save data before created Model
     * @var array
     */
    private $blocksDelay = [];

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public static function boot()
    {
        static::created(function (CityWithValues $model) {
            if (isset($model->blocksDelay['values'])) {
                foreach ($model->blocksDelay['values'] as $value) {
                    $value['city_id'] = $model->id;
                    \Log::warning($value);
                    PropertyValue::create($value);
                }
            }
        });
    }

    /**
     * @return array
     */
    public function getPropertyValuesAttribute()
    {
        $properties = Property::all()->keyBy('id');
        $values = PropertyValue::where('city_id', '=', $this->id)->get()->keyBy('property_id');
        $result = [];

        foreach ($properties as $property) {
            $value = $values->get($property->id);
            if ($value !== null) {
                $value = $value->value;
            }
            $result[] = [
                'id' => $property->id,
                'name' => $property->name,
                'slug' => $property->slug,
                'value' => $value
            ];
        }

        return $result;
    }

    /**
     * @param $properties
     */
    public function setPropertyValuesAttribute($properties)
    {
        $this->blocksDelay['values'] = [];

        foreach ($properties as $property) {
            if (!empty($property['id'])) {
                $propertyModel = Property::find($property['id']);
            } else {
                $propertyModel = new Property();
                $propertyModel->slug = str_slug($property['name']);
            }
            $propertyModel->fill([
                'name' => $property['name']
            ]);
            $propertyModel->save();

            $data = [
                'city_id' => $this->id,
                'property_id' => $propertyModel->id,
                'value' => $property['value']
            ];

            if ($this->id === null) {
                $this->blocksDelay['values'][] = $data;
            } else {
                $value = PropertyValue::where('city_id', '=', $this->id)->where('property_id', '=', $propertyModel->id)->first();
                if ($value === null) {
                    $value = new PropertyValue();
                    $value->fill($data);
                } else {
                    $value->value = $property['value'];
                }
                $value->save();
            }
        }
    }

}
