<?php

namespace Austerus\GeoApi\Models;

/**
 * ExchangeImport Model
 */
class ExchangeImport extends \Backend\Models\ImportModel
{
    protected $cities;

    protected $properties;

    /**
     * @var array The rules to be applied to the data.
     */
    public $rules = [
        'city' => 'required',
        'domain' => 'required',
        'prepositional' => 'required',
        'active' => 'required',
        'property' => 'required',
        'slug' => 'required',
        'value' => 'required',
    ];

    public function importData($results, $sessionKey = null)
    {
        $cities = City::all()->keyBy('domain');
        $properties = Property::with('value')->get()->keyBy('name');
        $defaultValues = PropertyValue::with('property')->whereNull('city_id')->get()->keyBy('property_id');

        foreach ($results as $row => $data) {

            try {
                if ($data['city'] !== 'Default') {
                    $city = $cities->get($data['domain']);
                    if ($city === null) {
                        $city = City::create([
                            'name' => $data['city'],
                            'domain' => $data['domain'],
                            'active' => true,
                        ]);
                        $this->logCreated();
                    }
                    if ((int)$city->active !== (int)$data['active'] || $city->prepositional !== $data['prepositional']) {
                        $city->update(['active' => $data['active'], 'prepositional' => $data['prepositional']]);
                        $this->logUpdated();
                    }
                }

                $property = $properties->get($data['property']);
                if ($property === null) {
                    $property = Property::create([
                        'name' => $data['property'],
                        'slug' => empty($data['slug']) ? str_slug($data['property']) : $data['slug'],
                    ]);
                    $this->logCreated();
                }
                if ($property->slug !== $data['slug']) {
                    $property->update(['slug' => $data['slug']]);
                    $this->logUpdated();
                }
                if ($data['city'] !== 'Default') {
                    $value = $property->value->where('city_id', $city->id)->first();
                } else {
                    $value = $defaultValues->get($property->id);
                    \Log::warning($value);
                }
                if ($value === null) {
                    $value = PropertyValue::create([
                        'value' => $data['value'],
                        'city_id' => $data['city'] !== 'Default' ? $city->id : null,
                        'property_id' => $property->id,
                    ]);
                    $this->logCreated();
                }
                if ($value->value !== $data['value']) {
                    \Log::warning($value->value);
                    \Log::warning($data['value']);
                    $value->update(['value' => $data['value']]);
                    $this->logUpdated();
                }
            } catch (\Exception $ex) {
                $this->logError($row, $ex->getMessage());
            }
        }
    }
}
