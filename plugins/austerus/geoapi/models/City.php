<?php namespace Austerus\GeoApi\Models;

use Model;

/**
 * City Model
 */
class City extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'austerus_geoapi_cities';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [
        'domain',
        'name',
        'prepositional',
        'options',
        'active'
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    /**
     * @param $query
     * @param $domain
     * @return mixed
     */
    public static function scopeForDomain($query, $domain)
    {
        return $query->whereDomain($domain);
    }

    /**
     * @param $domain
     * @return mixed
     */
    public static function getForDomain($domain)
    {
        return static::forDomain($domain)->first();
    }

    /**
     * @param $query
     * @return mixed
     */
    public static function scopeActiveOnly($query)
    {
        return $query->where('active', '=', true);
    }

    /**
     * @return mixed
     */
    public static function getActiveOnly()
    {
        return static::activeOnly()->get();
    }
}
