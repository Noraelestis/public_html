<?php namespace Austerus\GeoApi\Models;

use Model;
use October\Rain\Database\Traits\Sluggable;
use October\Rain\Database\Traits\Validation;

/**
 * Property Model
 */
class Property extends Model
{
    use Validation, Sluggable;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'austerus_geoapi_properties';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = ['*'];

    /**
     * @var array Generate slugs for these attributes.
     */
    protected $slugs = ['slug' => 'name'];

    /**
     * @var array The rules to be applied to the data.
     */
    public $rules = ['slug' => 'unique:austerus_geoapi_properties'];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [
        'value' => PropertyValue::class,
    ];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
    private $blocksDelay = [];

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public static function boot()
    {
        static::created(function ($model) {
            if (isset($model->blocksDelay['default_value'])) {
                $model->blocksDelay['default_value']['property_id'] = $model->id;
                PropertyValue::create($model->blocksDelay['default_value']);
            }
        });
    }

    /**
     * Dynamically rules method
     */
    public function beforeValidate()
    {
        if ($this->id !== null) {
            $this->rules['slug'] = $this->rules['slug'] . ',slug,' . $this->id;
        }
    }

    /**
     * @param City|null $city
     * @return \Illuminate\Support\Collection
     */
    public static function getValues(City $city = null)
    {
        $propValue = static::join('austerus_geoapi_property_values', 'austerus_geoapi_property_values.property_id', '=', 'austerus_geoapi_properties.id')
            ->select(['austerus_geoapi_properties.slug', 'austerus_geoapi_property_values.value']);
        if ($city !== null) {
            $propValue = $propValue->where('austerus_geoapi_property_values.city_id', '=', $city->id);
        } else {
            $propValue = $propValue->whereNull('austerus_geoapi_property_values.city_id');
        }

        return $propValue->get()->pluck('value', 'slug');
    }

    /**
     * @return string|null
     */
    public function getDefaultValueAttribute()
    {
        $value = PropertyValue::where('property_id', '=', $this->id)->whereNull('city_id')->first();
        if ($value !== null) {
            return $value->value;
        }
        return null;
    }

    /**
     * Set default property value
     * @param $value
     */
    public function setDefaultValueAttribute($value)
    {
        $propertyValue = PropertyValue::where('property_id', '=', $this->id)->whereNull('city_id')->first();
        if ($propertyValue === null) {
            $propertyValue = new PropertyValue;
        }
        $data = [
            'city_id' => null,
            'property_id' => $this->id,
            'value' => $value
        ];
        if ($this->id === null) {
            $this->blocksDelay['default_value'] = $data;
        } else {
            $propertyValue->fill($data);
            $propertyValue->save();
        }
    }

    /**
     * @return bool|null
     */
    public function delete()
    {
        $this->value->delete();
        return parent::delete();
    }
}
