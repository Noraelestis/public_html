<?php namespace Austerus\GeoApi\Models;

use Model;

/**
 * PropertyString Model
 */
class PropertyValue extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'austerus_geoapi_property_values';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = ['city_id', 'value', 'property_id'];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'city' => [City::class, 'city_id'],
        'property' => [Property::class, 'property_id']
    ];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
