<?php namespace Austerus\GeoApi\Models;

/**
 * ExchangeExport Model
 */
class ExchangeExport extends \Backend\Models\ExportModel
{
    public function exportData($columns, $sessionKey = null)
    {
        $cities = CityWithValues::all();
        $cities = $cities->map(function (CityWithValues $city) {
            $propValues = $city->propertyValues;
            return [
                'city' => $city->name,
                'domain' => $city->domain,
                'prepositional' => $city->prepositional,
                'active' => $city->active,
                'propValues' => $propValues,
            ];
        });
        $result = [];
        $defaultValues = PropertyValue::whereNull('city_id')->with('property')->get();
        foreach ($defaultValues as $value) {
            $result[] = [
                'city' => 'Default',
                'prepositional' => '',
                'domain' => '',
                'active' => '',
                'property' => $value->property->name,
                'slug' => $value->property->slug,
                'value' => $value->value,
            ];
        }
        foreach ($cities as $city) {
            foreach ($city['propValues'] as $propValue) {
                $result[] = [
                    'city' => $city['city'],
                    'prepositional' => $city['prepositional'],
                    'domain' => $city['domain'],
                    'active' => $city['active'],
                    'property' => $propValue['name'],
                    'slug' => $propValue['slug'],
                    'value' => $propValue['value'],
                ];
            }
        }
        return $result;
    }
}
