<?php namespace Austerus\Shop\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class AddProductAnaloguesAlias extends Migration
{
    public function up()
    {
        Schema::table('austerus_shop_products', function(Blueprint $table) {
            $table->string('analogue_alias')->nullable();
        });
    }

    public function down()
    {
        Schema::table('austerus_shop_products', function(Blueprint $table) {
            $table->dropColumn('analogue_alias');
        });
    }
}
