<?php namespace Austerus\Shop\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class ManufacturerOne extends Migration
{
    public function up()
    {
        Schema::drop('manufacturer_product');
        Schema::dropIfExists('manufacturers_product');

        Schema::table('austerus_shop_products', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->string('article_provider',100)->nullable();
            $table->integer('manufacturer_id')->unsigned()->nullable();
            $table->foreign('manufacturer_id')->references('id')->on('austerus_shop_manufacturers')->onDelete('set null');
        });


    }

    public function down()
    {
        Schema::create('manufacturer_product', function(Blueprint $table)
        {
            $table->integer('manufacturer_id')->unsigned();
            $table->integer('product_id')->unsigned();
            $table->primary(['manufacturer_id', 'product_id']);
        });

        Schema::table('austerus_shop_products', function(Blueprint $table) {
            $table->dropColumn('article_provider',100);
            $table->dropColumn('manufacturer_id')->unsigned()->nullable();
        });

    }
}
