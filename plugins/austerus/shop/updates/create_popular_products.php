<?php namespace Austerus\Shop\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreatePopularProducts extends Migration
{
    public function up()
    {
        Schema::table('austerus_shop_products', function (Blueprint $table){
            $table->boolean('popular')->default(false);
        });
    }

    public function down()
    {
        Schema::table('austerus_shop_products', function (Blueprint $table){
            $table->dropColumn('popular');
        });
    }
}
