<?php namespace Austerus\Shop\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateUnitsTable extends Migration
{
    public function up()
    {
        Schema::create('austerus_shop_units', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 100);
            $table->string('full_name', 100);
            $table->string('slug', 100);
            $table->timestamps();
        });

        Schema::table('austerus_shop_products', function(Blueprint $table) {
            $table->integer('unit_id')->unsigned()->nullable();
            $table->foreign('unit_id')->references('id')->on('austerus_shop_units')->onDelete('set null');
        });
    }

    public function down()
    {
        Schema::dropIfExists('austerus_shop_units');

        Schema::table('austerus_shop_products', function(Blueprint $table) {
            $table->dropColumn('unit_id');
        });
    }
}
