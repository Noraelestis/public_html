<?php namespace Austerus\Shop\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class OrdersForm extends Migration
{
    public function up()
    {
        Schema::table('austerus_shop_products', function(Blueprint $table) {
            $table->integer('discount_price')->unsigned()->nullable();
            $table->integer('discount_from')->unsigned()->nullable();
        });

        Schema::create('austerus_shop_orders', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('email')->nullable();
            $table->string('phone')->nullable();
            $table->text('details')->nullable();
            $table->integer('price')->default(0);
            $table->timestamps();
        });

    }

    public function down()
    {
        Schema::table('austerus_shop_products', function(Blueprint $table) {
            $table->dropColumn('discount_price');
            $table->dropColumn('discount_from');
        });

        Schema::dropIfExists('austerus_shop_orders');
    }
}
