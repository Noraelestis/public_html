<?php namespace Austerus\Shop\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class AddProductAnalogues extends Migration
{
    public function up()
    {
        Schema::create('austerus_shop_product_analog', function($table)
        {
            $table->integer('product_id')->unsigned();
            $table->integer('analog_id')->unsigned();
            $table->primary(['product_id', 'analog_id']);
        });
    }

    public function down()
    {
        Schema::dropIfExists('austerus_shop_product_analog');
    }
}
