<?php namespace Austerus\Shop\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class AddCheckboxField extends Migration
{
    public function up()
    {
        Schema::table('austerus_shop_categories', function(Blueprint $table) {
            $table->tinyInteger('is_param_1')->default(0);
            $table->tinyInteger('is_param_2')->default(0);
            $table->tinyInteger('is_param_3')->default(0);
            $table->tinyInteger('is_param_4')->default(0);
            $table->tinyInteger('is_param_5')->default(0);
        });
    }

    public function down()
    {
        Schema::table('austerus_shop_categories', function(Blueprint $table) {
            $table->dropColumn('is_param_1');
            $table->dropColumn('is_param_2');
            $table->dropColumn('is_param_3');
            $table->dropColumn('is_param_4');
            $table->dropColumn('is_param_5');
        });
    }
}
