<?php namespace Austerus\Shop\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateProductsTable extends Migration
{
    public function up()
    {
        Schema::create('austerus_shop_products', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('product_code',6)->unique();
            $table->string('full_name')->nullable();
            $table->string('title')->nullable();
            $table->string('description')->nullable();
            $table->string('keywords')->nullable();
            $table->text('introduction')->nullable();
            $table->text('content')->nullable();
            $table->string('slug')->nullable()->unique();
            $table->string('images')->nullable();
            $table->integer('price')->nullable()->default(0)->unsigned();
            $table->integer('product_target_id')->nullable()->unsigned();
            $table->integer('power')->nullable()->unsigned();
            $table->integer('power_consumer')->nullable()->unsigned();
            $table->integer('light_flow_from')->nullable()->unsigned();
            $table->integer('light_flow_to')->nullable()->unsigned();
            $table->string('protection')->nullable();
            $table->integer('colorful_temperature')->nullable()->unsigned();
            $table->integer('voltage_from')->nullable()->unsigned();
            $table->integer('voltage_to')->nullable()->unsigned();
            $table->string('height_suspension')->nullable();
            $table->integer('life_time')->nullable()->unsigned();
            $table->integer('guarantee')->nullable()->unsigned();
            $table->float('weight')->nullable()->unsigned();
            $table->integer('length')->nullable()->unsigned();
            $table->integer('width')->nullable()->unsigned();
            $table->integer('height')->nullable()->unsigned();
            $table->integer('suspension_id')->nullable()->unsigned();
            $table->integer('sort')->nullable()->unsigned();
            $table->text('replacing_lamp')->nullable();
            $table->string('pdf_description')->nullable();
            $table->text('additional_options')->nullable();
            $table->text('benefits_led')->nullable();
            $table->text('compliance')->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('product_target_id')->references('id')->on('austerus_shop_product_targets');
        });

        Schema::create('category_product', function($table)
        {
            $table->integer('category_id')->unsigned();
            $table->integer('product_id')->unsigned();
            $table->primary(['category_id', 'product_id']);
        });

        Schema::create('product_product_target', function($table)
        {
            $table->integer('product_target_id')->unsigned();
            $table->integer('product_id')->unsigned();
            $table->primary(['product_id', 'product_target_id']);
        });
    }

    public function down()
    {
        Schema::dropIfExists('category_product');
        Schema::dropIfExists('product_product_target');
        Schema::dropIfExists('austerus_shop_products');
    }
}
