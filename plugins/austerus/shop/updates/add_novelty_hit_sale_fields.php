<?php namespace Austerus\Shop\Updates;

use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;
use Schema;

class AddNoveltyHitSaleFields extends Migration
{
    public function up()
    {
        Schema::table('austerus_shop_products', function (Blueprint $table) {
            $table->boolean('is_novelty')->default(false);
            $table->boolean('is_hit')->default(false);
            $table->boolean('is_stock')->default(false);
        });
    }

    public function down()
    {
        Schema::table('austerus_shop_products', function (Blueprint $table) {
            $table->dropColumn('is_novelty');
            $table->dropColumn('is_hit');
            $table->dropColumn('is_stock');
        });
    }
}
