<?php namespace Austerus\Shop\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateSuspensionsTable extends Migration
{
    public function up()
    {
        Schema::create('austerus_shop_suspensions', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->string('slug');
            $table->timestamps();
        });

        Schema::create('austerus_shop_product_suspension', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('product_id')->unsigned();
            $table->integer('suspension_id')->unsigned();
            $table->foreign('product_id')->references('id')->on('austerus_shop_products')->onDelete('cascade');
            $table->foreign('suspension_id')->references('id')->on('austerus_shop_suspensions')->onDelete('cascade');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('austerus_shop_product_suspension');

        Schema::dropIfExists('austerus_shop_suspensions');
    }
}
