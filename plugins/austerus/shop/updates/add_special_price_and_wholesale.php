<?php namespace Austerus\Shop\Updates;

use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;
use Schema;

class AddSpecialPriceAndWholesale extends Migration
{
    public function up()
    {
        Schema::table('austerus_shop_products', function (Blueprint $table) {
            $table->integer('special_price')->unsigned()->nullable();
            $table->integer('wholesale_price')->unsigned()->nullable();
        });
    }

    public function down()
    {
        Schema::table('austerus_shop_products', function (Blueprint $table) {
            $table->dropColumn('special_price');
            $table->dropColumn('wholesale_price');
        });
    }
}
