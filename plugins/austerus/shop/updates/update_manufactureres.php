<?php namespace Austerus\Shop\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class UpdateManufacturers extends Migration
{
    public function up()
    {
        Schema::table('austerus_shop_manufacturers', function(Blueprint $table) {
            $table->string('slug');
        });

        Schema::table('austerus_shop_products', function(Blueprint $table) {
            $table->string('product_code', 100)->nullable()->change();
        });
    }

    public function down()
    {
        Schema::table('austerus_shop_manufacturers', function(Blueprint $table) {
            $table->dropColumn('slug');
        });

    }
}
