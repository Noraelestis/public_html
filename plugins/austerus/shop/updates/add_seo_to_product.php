<?php namespace Austerus\Shop\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class AddSeoToProduct extends Migration
{
    public function up()
    {
        Schema::table('austerus_shop_product_targets', function(Blueprint $table) {
            $table->string('title')->nullable();
            $table->text('description')->nullable();
            $table->string('keywords')->nullable();
            $table->string('image')->nullable();
        });
    }

    public function down()
    {
        Schema::table('austerus_shop_product_targets', function(Blueprint $table) {
            $table->dropColumn('title');
            $table->dropColumn('description');
            $table->dropColumn('keywords');
            $table->dropColumn('image');
        });
    }
}
