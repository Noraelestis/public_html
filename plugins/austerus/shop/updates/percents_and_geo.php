<?php namespace Austerus\Shop\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class PercentsAndGeo extends Migration
{
    public function up()
    {
        Schema::table('austerus_shop_products', function(Blueprint $table) {
            $table->decimal('purchase_price')->default(0)->nullable();
        });

        Schema::create('austerus_shop_price_config', function(Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('partner_price')->default(0)->nullable();
            $table->integer('wholesale_price')->default(0)->nullable();
            $table->integer('small_wholesale_price')->default(0)->nullable();
            $table->integer('price')->default(0)->nullable();
            $table->integer('discount_price')->default(0)->nullable();
            $table->integer('special_price')->default(0)->nullable();
            $table->boolean('is_default')->default(false);
        });

        Schema::create('austerus_shop_city_price_config', function(Blueprint $table) {
            $table->integer('city_id')->unsigned();
            $table->integer('price_config_id')->unsigned();
            $table->foreign('city_id')->references('id')->on('austerus_geoapi_cities')->onDelete('cascade');
            $table->foreign('price_config_id')->references('id')->on('austerus_shop_price_config')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::drop('austerus_shop_price_config');
        Schema::table('austerus_shop_products', function(Blueprint $table) {
            $table->dropColumn('purchase_price');
        });
    }
}
