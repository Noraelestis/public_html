<?php namespace Acme\Users\Updates;

use Adrenth\Redirect\Models\Redirect;
use Austerus\Shop\Models\Category;
use Austerus\Shop\Models\Product;
use Seeder;

class SetRedirects extends Seeder
{
    public function run()
    {
        \DB::transaction(function () {
            $products = Product::all();
            $startOrder = 16;
            foreach ($products as $product) {
                Redirect::create([
                    'match_type' => 'exact',
                    'target_type' => 'path_or_url',
                    'from_url' => '/product/' . $product->slug,
                    'to_url' => '/' . $product->slug,
                    'status_code' => '301',
                    'hits' => 0,
                    'sort_order' => ++$startOrder,
                    'is_enabled' => 1,
                    'system' => 0
                ]);
            }

            $categories = Category::all();
            foreach ($categories as $category) {
                Redirect::create([
                    'match_type' => 'exact',
                    'target_type' => 'path_or_url',
                    'from_url' => '/categories/' . $category->slug,
                    'to_url' => '/' . $category->slug,
                    'status_code' => '301',
                    'hits' => 0,
                    'sort_order' => ++$startOrder,
                    'is_enabled' => 1,
                    'system' => 0
                ]);
            }
        });
    }
}