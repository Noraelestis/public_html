<?php namespace Austerus\Shop\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class AddImageToManufacturers extends Migration
{
    public function up()
    {
        Schema::table('austerus_shop_manufacturers', function(Blueprint $table) {
            $table->string('image')->nullable();
        });
    }

    public function down()
    {
        Schema::table('austerus_shop_manufacturers', function(Blueprint $table) {
            $table->dropColumn('image');
        });

    }
}
