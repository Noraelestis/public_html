<?php namespace Austerus\Shop\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class AddSeoToProduct extends Migration
{
    public function up()
    {
        Schema::table('austerus_shop_products', function(Blueprint $table) {
            $table->text('passport')->nullable();
            $table->text('IES')->nullable();
            $table->text('descriptionPDF')->nullable();
            $table->text('certificate')->nullable();
        });
    }

    public function down()
    {
        Schema::table('austerus_shop_products', function(Blueprint $table) {
            $table->dropColumn('passport');
            $table->dropColumn('IES');
            $table->dropColumn('descriptionPDF');
            $table->dropColumn('certificate');
        });
    }
}
