<?php namespace Austerus\Shop\Updates;

use Austerus\Shop\Models\Category;
use Austerus\Shop\Models\Product;
use Austerus\Shop\Models\ShopRouter;
use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class FillShopRoutersTable extends Migration
{
    public function up()
    {
        $categories = Category::all();
        foreach ($categories as $category) {
            $category->route()->create(['slug' => $category->slug]);
        }
        $products = Product::all();
        foreach ($products as $product) {
            $product->route()->create(['slug' => $product->slug]);
        }
    }

    public function down()
    {
    }
}
