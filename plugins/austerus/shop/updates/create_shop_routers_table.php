<?php namespace Austerus\Shop\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateShopRoutersTable extends Migration
{
    public function up()
    {
        Schema::create('austerus_shop_shop_routers', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('slug')->unique();
            $table->integer('routeable_id')->unsigned();
            $table->string('routeable_type');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('austerus_shop_shop_routers');
    }
}
