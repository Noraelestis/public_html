<?php namespace Austerus\Shop\Updates;

use Austerus\Shop\Models\Product;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;
use Schema;

class LightFlowOnly extends Migration
{
    public function up()
    {

        \DB::transaction(function () {
            $products = Product::all();

            Schema::table('austerus_shop_products', function (Blueprint $table) {
                $table->dropColumn('light_flow_from');
                $table->dropColumn('light_flow_to');
                $table->integer('light_flow');
            });

            foreach ($products as $product) {
                $product->update(['light_flow' => $product->light_flow_to]);
            }
        });


    }

    public function down()
    {

    }
}
