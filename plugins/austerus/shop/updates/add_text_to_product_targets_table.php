<?php namespace Austerus\Shop\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class AddTextToProductTargetsTable extends Migration
{
    public function up()
    {
        Schema::table('austerus_shop_product_targets', function(Blueprint $table) {
            $table->text('content')->nullable();
        });
    }

    public function down()
    {
        Schema::table('austerus_shop_product_targets', function(Blueprint $table) {
            $table->dropColumn('content');
        });
    }
}
