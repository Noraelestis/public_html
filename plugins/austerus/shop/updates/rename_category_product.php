<?php namespace Austerus\Shop\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class RenameCategoryProduct extends Migration
{
    public function up()
    {
        Schema::rename('category_product', 'austerus_shop_category_product');
    }

    public function down()
    {
        Schema::rename('austerus_shop_category_product', 'category_product');
    }
}
