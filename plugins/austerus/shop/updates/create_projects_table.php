<?php namespace Austerus\Shop\Updates;

use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;
use Schema;

class CreateProjectsTable extends Migration
{
    public function up()
    {
        Schema::create('austerus_shop_projects', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('title')->nullable();
            $table->string('slug')->unique();
            $table->string('location')->nullable();
            $table->string('provider')->nullable();
            $table->timestamp('data')->nullable();
            $table->text('content')->nullable();
            $table->text('images')->nullable();
            $table->timestamps();
        });

        Schema::create('austerus_shop_products_projects', function (Blueprint $table) {
            $table->integer('project_id')->unsigned();
            $table->integer('product_id')->unsigned();
            $table->foreign('project_id')->references('id')->on('austerus_shop_projects')->onDelete('cascade');
            $table->foreign('product_id')->references('id')->on('austerus_shop_products')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::dropIfExists('austerus_shop_products_projects');

        Schema::dropIfExists('austerus_shop_projects');
    }
}
