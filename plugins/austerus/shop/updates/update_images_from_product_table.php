<?php namespace Austerus\Shop\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class UpdateImagesFromProductTable extends Migration
{
    public function up()
    {
        Schema::table('austerus_shop_products', function(Blueprint $table) {
            $table->text('images')->nullable()->change();
        });
    }

    public function down()
    {
        Schema::table('austerus_shop_products', function(Blueprint $table) {
            $table->string('images')->nullable()->change();
        });
    }
}
