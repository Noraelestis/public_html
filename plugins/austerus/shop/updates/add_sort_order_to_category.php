<?php namespace Austerus\Shop\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class AddSortOrderToCategory extends Migration
{
    public function up()
    {
        Schema::table('austerus_shop_categories', function(Blueprint $table) {
           $table->integer('sort_order')->unsigned();
        });
    }

    public function down()
    {
        Schema::table('austerus_shop_categories', function(Blueprint $table) {
            $table->dropColumn('sort_order');
        });
    }
}
