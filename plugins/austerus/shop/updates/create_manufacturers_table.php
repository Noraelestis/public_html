<?php namespace Austerus\Shop\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateManufacturersTable extends Migration
{
    public function up()
    {
        Schema::create('austerus_shop_manufacturers', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name');
            $table->timestamps();
        });

        Schema::create('manufacturer_product', function($table)
        {
            $table->integer('manufacturer_id')->unsigned();
            $table->integer('product_id')->unsigned();
            $table->primary(['manufacturer_id', 'product_id']);
        });
    }

    public function down()
    {
        Schema::dropIfExists('manufacturer_product');
        Schema::dropIfExists('austerus_shop_manufacturers');
    }
}
