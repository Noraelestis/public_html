<?php namespace Austerus\Shop\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateProductAlsoNeedTable extends Migration
{
    public function up()
    {
        Schema::create('austerus_product_also_need', function($table)
        {
            $table->integer('product_id')->unsigned();
            $table->integer('also_need_id')->unsigned();
            $table->primary(['product_id', 'also_need_id']);
        });

        Schema::table('austerus_shop_products', function ($table){
            $table->string('also_need_name')->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('austerus_product_also_need');

        Schema::table('austerus_shop_products', function ($table){
            $table->dropColumn('also_need_name');
        });
    }
}
