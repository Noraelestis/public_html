<?php namespace Austerus\Shop\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateAusterusShopCategories extends Migration
{
    public function up()
    {
        Schema::table('austerus_shop_categories', function($table)
        {
            $table->string('seo_h1', 255)->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('austerus_shop_categories', function($table)
        {
            $table->dropColumn('seo_h1');
        });
    }
}
