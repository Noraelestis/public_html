<?php namespace Austerus\Shop\Updates;

use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;
use Schema;

class FixCodeBug extends Migration
{
    public function up()
    {
        Schema::table('austerus_shop_products', function (Blueprint $table) {
            $table->string('product_code', 10)->nullable()->change();
        });

    }

    public function down()
    {
        Schema::table('austerus_shop_products', function (Blueprint $table) {
            $table->string('product_code', 6)->change();
        });
    }
}
