<?php namespace Austerus\Shop\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateProductRelationTable extends Migration
{
    public function up()
    {
        Schema::create('austerus_product_relation', function($table)
        {
            $table->integer('product_id')->unsigned();
            $table->integer('relation_id')->unsigned();
            $table->primary(['product_id', 'relation_id']);
        });

        Schema::table('austerus_shop_products', function ($table){
            $table->string('relation_name')->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('austerus_product_relation');

        Schema::table('austerus_shop_products', function ($table){
            $table->dropColumn('relation_name');
        });
    }
}
