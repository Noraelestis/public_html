<?php namespace Austerus\Shop\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class SetLightFlowNullalble extends Migration
{
    public function up()
    {
        Schema::table('austerus_shop_products', function(Blueprint $table) {
            $table->integer('light_flow')->unsigned()->nullable()->change();
        });
    }

    public function down()
    {
        Schema::table('austerus_shop_products', function(Blueprint $table) {
            $table->integer('light_flow')->unsigned()->change();
        });
    }
}
