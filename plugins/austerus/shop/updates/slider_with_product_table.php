<?php namespace Austerus\Shop\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class SliderWithProductTable extends Migration
{
    public function up()
    {
        Schema::create('austerus_shop_slider_product', function(Blueprint $table) {
            $table->integer('slider_id')->unsigned();
            $table->integer('product_id')->unsigned();
            $table->index(['product_id', 'slider_id']);
            $table->foreign('product_id')->references('id')->on('austerus_shop_products')->onDelete('cascade');
            $table->foreign('slider_id')->references('id')->on('austerus_shop_sliders')->onDelete('cascade');
        });

        Schema::create('austerus_shop_slider_category', function(Blueprint $table) {
            $table->integer('slider_id')->unsigned();
            $table->integer('category_id')->unsigned();
            $table->index(['category_id', 'slider_id']);
            $table->foreign('category_id')->references('id')->on('austerus_shop_categories')->onDelete('cascade');
            $table->foreign('slider_id')->references('id')->on('austerus_shop_sliders')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::drop('austerus_shop_slider_products');
        Schema::drop('austerus_shop_slider_category');
    }
}
