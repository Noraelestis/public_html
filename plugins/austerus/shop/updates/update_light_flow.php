<?php namespace Austerus\Shop\Updates;

use Austerus\Shop\Models\Product;
use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class UpdateLightFlow extends Migration
{
    public function up()
    {
        $products = Product::all();
        foreach ($products as $product) {
            if(empty($product->light_flow_from) || $product->light_flow_from == 0) {
                $product->light_flow_from = $product->light_flow_to;
                $product->save();
            }
            if(empty($product->light_flow_to) || $product->light_flow_to == 0) {
                $product->light_flow_to = $product->light_flow_from;
                $product->save();
            }
        }
    }

    public function down()
    {

    }
}
