<?php namespace Austerus\Shop\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class CreateCategoriesTable extends Migration
{
    public function up()
    {
        Schema::create('austerus_shop_categories', function(Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('parent_id')->nullable()->unsigned();
            $table->integer('nest_left')->nullable();
            $table->integer('nest_right')->nullable();
            $table->integer('nest_depth')->nullable();
            $table->string('slug',100)->nullable();
            $table->string('name',100)->nullable();
            $table->string('full_name')->nullable();
            $table->string('description')->nullable();
            $table->text('introduction')->nullable();
            $table->text('content')->nullable();
            $table->string('image')->nullable();
            $table->boolean('primary')->default(true);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('austerus_shop_categories');
    }
}
