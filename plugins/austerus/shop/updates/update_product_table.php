<?php namespace Austerus\Shop\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class UpdateProductTable extends Migration
{
    public function up()
    {
        Schema::table('austerus_shop_products', function(Blueprint $table) {
            $table->dropColumn('introduction');
            $table->text('custom_text')->nullable();
            $table->string('product_code',6)->nullable()->change();
            $table->string('temperature_range')->nullable();
            $table->float('power_consumer')->nullable()->unsigned()->change();
            $table->float('power')->nullable()->unsigned()->change();
            $table->string('colorful_temperature')->nullable()->change();
        });
    }

    public function down()
    {
        Schema::table('austerus_shop_products', function(Blueprint $table) {
            $table->text('introduction')->nullable();
            $table->dropColumn('custom_text');
            $table->string('product_code',6)->unique()->change();
            $table->dropColumn('temperature_range');
            $table->integer('power_consumer')->nullable()->unsigned()->change();
            $table->integer('power')->nullable()->unsigned()->change();
            $table->integer('colorful_temperature')->nullable()->change();
        });
    }
}
