<?php namespace Austerus\Shop\Updates;

use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;
use Schema;

class AddFieldsToOrdersForm extends Migration
{
    public function up()
    {
        Schema::table('austerus_shop_orders', function (Blueprint $table) {
            $table->text('description')->nullable();
        });
    }

    public function down()
    {
        Schema::table('austerus_shop_orders', function (Blueprint $table) {
            $table->dropIfExists('description');
        });
    }
}
