<?php namespace Austerus\Shop\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class AddCustomTitles extends Migration
{
    public function up()
    {
        Schema::table('austerus_shop_products', function(Blueprint $table) {
            $table->string('custom_title_content')->nullable();
            $table->string('custom_title_additional_options')->nullable();
            $table->string('custom_title_benefits_led')->nullable();
            $table->string('custom_title_specifications')->nullable();
            $table->string('custom_title_compliance')->nullable();
        });
    }

    public function down()
    {
        Schema::table('austerus_shop_products', function(Blueprint $table) {
            $table->dropColumn('custom_title_content');
            $table->dropColumn('custom_title_additional_options');
            $table->dropColumn('custom_title_benefits_led');
            $table->dropColumn('custom_title_specifications');
            $table->dropColumn('custom_title_compliance');
        });
    }
}
