<?php namespace Austerus\Shop\Updates;

use Schema;
use October\Rain\Database\Schema\Blueprint;
use October\Rain\Database\Updates\Migration;

class UpdateProductTableUnnameParams extends Migration
{
    public function up()
    {
        Schema::table('austerus_shop_products', function(Blueprint $table) {
            $table->dropColumn('power');
            $table->dropColumn('power_consumer');
            $table->dropColumn('protection');
            $table->dropColumn('colorful_temperature');
            $table->dropColumn('voltage_from');
            $table->dropColumn('voltage_to');
            $table->dropColumn('height_suspension');
            $table->dropColumn('life_time');
            $table->dropColumn('suspension_id');
            $table->dropColumn('replacing_lamp');
            $table->dropColumn('temperature_range');
            $table->dropColumn('light_flow');
            $table->dropColumn('article_provider');
            $table->string('param_1', 255 * 2)->nullable();
            $table->string('param_2', 255 * 2)->nullable();
            $table->string('param_3', 255 * 2)->nullable();
            $table->string('param_4', 255 * 2)->nullable();
            $table->string('param_5', 255 * 2)->nullable();
        });

        Schema::table('austerus_shop_categories', function(Blueprint $table) {
            $table->string('name_param_1', 255)->nullable();
            $table->string('name_param_2', 255)->nullable();
            $table->string('name_param_3', 255)->nullable();
            $table->string('name_param_4', 255)->nullable();
            $table->string('name_param_5', 255)->nullable();
        });
    }

    public function down()
    {
        Schema::table('austerus_shop_products', function(Blueprint $table) {

        });
    }
}
