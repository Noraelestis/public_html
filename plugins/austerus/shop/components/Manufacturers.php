<?php namespace Austerus\Shop\Components;

use Austerus\Shop\Models\Manufacturer;
use Cms\Classes\ComponentBase;

class Manufacturers extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name' => 'Компонент производителей',
            'description' => 'Компонент поддоменов по производителям (в разработке)'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    /**
     * @var bool
     */
    public $isManufacturerPage = false;

    /**
     * @var string
     */
    public $subDomain = '';

    /**
     * @var Manufacturer
     */
    protected $manufacturer;

    public function onRun()
    {
        $domain = explode('.', \Request::getHost());
        if (count($domain) > 2) {
            $manufacturer = Manufacturer::getForSlug($domain[0]);
            if($manufacturer !== null) {
                $this->isManufacturerPage = true;
                $this->subDomain = $domain[0];
                $this->manufacturer = $manufacturer;
            }
        }
    }

    public function onRender()
    {
        echo $this->manufacturer->name;
    }
}
