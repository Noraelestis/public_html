<?php namespace Austerus\Shop\Components;

use Cms\Classes\ComponentBase;
use Cms\Classes\Page;
use Cms\Classes\Theme;
use Austerus\Shop\Components\Shop;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\URL;

class Sitemap extends ComponentBase
{

    public $map = [];

    protected $components = ['Shop' => Shop::class];

    protected $ignore = ['sitemap.htm', 'product_code.htm'];

    public function componentDetails()
    {
        return [
            'name'        => 'Карта сайта светограда',
            'description' => 'Специально для светограда'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $config['product-page'] = '/';
        $partial = 'sitemap/xml';

        if (!($theme = Theme::getActiveTheme())) {
            throw new ApplicationException(Lang::get('cms::lang.theme.edit.not_found'));
        }


        $pages = Page::listInTheme($theme, true)->map(function ($page){
            preg_match('/(\:)[^\?]+([\?]*)/is', $page->url, $result);
            $params = [];
            if(empty($result)){
                $params['url'] = url($page->url);
            }
            $params['title'] = $page->title;

            preg_match('/\{% component \'([^)]+)\' %\}/', $page->content, $result);

            if(isset($result[1]) && isset($this->components[$result[1]])){
                $params['children'] = $this->components[$result[1]]::getSiteMap();
            }

            return $params;
        });

        foreach ($pages as $file_name => $page){
            if(!in_array($file_name, $this->ignore)){
                if($page !== null){
                    $this->map[] = $page;
                }
            }
        }
        $this->map = array_prepend($this->map, ['title' => 'Главная', 'url' => url('/')]);

        return Response::make($this->renderPartial($partial, ['map' => $this->map]))->header('Content-type', 'text/xml');
    }

}