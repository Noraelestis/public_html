<?php namespace Austerus\Shop\Components;

use Cms\Classes\ComponentBase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Austerus\Shop\Classes\Cart\Favorites as FavoritesClass;

class Favorites extends ComponentBase
{
    /**
     * @var FavoritesClass
     */
    protected $favorites;

    /**
     * @return array
     */
    public function componentDetails()
    {
        return [
            'name' => 'Избранные товары',
            'description' => 'Компонент реализует избранные товары'
        ];
    }

    /**
     * Properties for use in components
     * @return array
     */
    public function defineProperties()
    {
        return [];
    }

    public function onRun(): void 
    {
        $this->favorites = new FavoritesClass();
    }

    /**
     * @return mixed
     */
    public function onRender()
    {
        return $this->renderPartial('@default', [
            'products' => $this->favorites->getProducts(),
            'labels' => $this->getLabels(),
        ]);
    }

    public function getLabels(): array
    {
        return [
            'product' => $this->getProductLabel(),
        ];
    }

    public function getFavorites(): FavoritesClass
    {
        return $this->favorites;
    }

    public function getCount(): int
    {
        return $this->favorites->getCount();
    }

    public function onAddProductToFavorites(): JsonResponse
    {
        $this->onRun();
        try {
            $product = $this->favorites->addProduct((int)(post('id')));
            return \Response::json([
                'status' => 'success',
                'render' => $this->renderPartial('@item', ['product' => $product]),
                'count' => $this->favorites->getCount(),
                'labels' => $this->getLabels(),
            ]);
        } catch (\Exception $e) {
            return \Response::json(['status' => 'error']);
        }
    }

    public function onProductDeleteFromFavorites(): JsonResponse
    {
        try {
            $this->onRun();
            $this->favorites->deleteProduct((int)post('id'));
            return \Response::json([
                'status' => 'success',
                'count' => $this->favorites->getCount(),
                'labels' => $this->getLabels(),
            ]);
        } catch (\Exception $e) {
            return \Response::json(['status' => 'error']);
        }
    }

    public function onProductsResetFromFavorites(): JsonResponse
    {
        try {
            $this->onRun();
            $this->favorites->clearFavorites();
            return \Response::json([
                'status' => 'success',
            ]);
        } catch (\Exception $e) {
            return \Response::json(['status' => 'error']);
        }
    }

    protected function getProductLabel(): string
    {
        if ($this->favorites->getCount() === 1) {
            return 'товар';
        }
        return $this->favorites->getCount() < 5 ? 'товара' : 'товаров';
    }
}