<?php namespace Austerus\Shop\Components;

use Austerus\Breadcrumbs\Components\Breadcrumbs;
use Austerus\GeoApi\Classes\GeoApiFacade;
use Austerus\Shop\Classes\Presenters\ProductVariablesPresenter;
use Austerus\Shop\Models\Category;
use Austerus\Shop\Models\CategoryGeoPresenter;
use Austerus\Shop\Models\Product;
use Austerus\Shop\Models\ShopRouter;
use Cms\Classes\ComponentBase;

class Shop extends ComponentBase
{
    public $type;

    /**
     * @var Category
     */
    protected $category;

    /**
     * @var Product
     */
    protected $product;

    /**
     * @var ShopRouter
     */
    protected $route;

    /**
     * @var integer
     */
    protected $perPage;

    /**
     * @var array
     */
    protected $scatterPage = [];

    public function componentDetails()
    {
        return [
            'name' => 'Магазин',
            'description' => 'Каталог товаров для светограда'
        ];
    }

    public function defineProperties()
    {
        return [
            'mode' => [
                'title' => 'Режим',
                'description' => 'Выбор режима работы компонента',
                'default' => 'shop',
                'type' => 'dropdown',
                'options' => [
                    'shop' => 'shop',
                    'root' => 'root'
                ]
            ],
            'filterEnable' => [
                'title' => 'Учитывать фильтр',
                'description' => 'Если включен, будет учитываться фильтр при запросе в бд',
                'default' => true,
                'type' => 'checkbox'
            ],
            'perPage' => [
                'title' => 'Товаров на страницу',
                'description' => 'Колличество товаров на одну страницу, чтобы отключить поставьте 0',
                'default' => '20',
                'type' => 'text',
            ],
            'scatterPage' => [
                'title' => 'Варианты кол-ва на страницу',
                'description' => 'Через заяпятую по сколько можно показывать на одну страницу',
                'default' => '10,20,50,100',
                'type' => 'text',
            ],
            'defaultImage' => [
                'title' => 'Изображение по умолчанию',
                'description' => 'Будет использоваться если у товара нет своего изображения',
                'default' => '/themes/protal/assets/images/no-products.png',
                'type' => 'text',

            ],
            'colorFullBr' => [
                'title' => 'Перенос строки для температуры',
                'description' => 'Если включено, то цветовая температура будет переносится на новую строку каждая',
                'default' => true,
                'type' => 'checkbox'
            ]
        ];
    }

    /**
     * @return array
     */
    public static function filterAttributes()
    {
        return [
            'filter_price_from',
            'filter_price_to',
            'param_1',
            'param_2',
            'param_3'.
            'param_4',
            'param_5'
        ];
    }

    /**
     * @return string
     */
    public function onRun()
    {
        if ($this->property('mode') === 'shop' && $this->param('slug') === null) {
            return null;
        }
        if ($this->property('mode') !== 'root' || ($this->property('mode') === 'root' && $this->param('slug') !== null)) {
            $this->route = ShopRouter::getForSlug($this->param('slug'));
            if ($this->route === null) {
                return \Response::make(\View::make('cms::404'), 404);
            }
        }
        $this->scatterPage = explode(',', $this->property('scatterPage'));

        if (\Input::get('per_page') !== null) {
            $this->perPage = \Input::get('per_page');
            if ($this->perPage > max($this->scatterPage)) {
                $this->perPage = max($this->scatterPage);
            }
        } else {
            $this->perPage = $this->property('perPage');
        }

        if ($this->property('mode') === 'root' && $this->param('slug') === null) {
            $this->type = 'root';
        }
        if ($this->route->routeable_type === Product::class) {
            $this->type = 'product';
        }
        if ($this->route->routeable_type === Category::class) {
            $this->type = 'category';
        }
    }

    /**
     * @return mixed
     */
    public function onRender(): string
    {
        switch ($this->type) {
            case 'root':
                return $this->renderRoot();
            case 'product':
                return $this->renderProduct('shop', $this->route);
            case 'category':
                return $this->renderCategory($this->route);
            default:
                return '';
        }
    }

    /**
     * @param string $mode
     * @param $route
     * @return mixed
     */
    protected function renderProduct($mode = 'shop', $route = null)
    {
        if ($route === null && $mode === 'shop') {
            return $this->renderPartial('products/no-product.htm', ['products' => Product::all()]);
        } else {
            $this->product = $route->routeable;
        }
        $this->product->editMode = false;
        if ($this->product !== null) {
            $this->product->load('unit', 'manufacturer', 'analogues', 'product_relations', 'product_also_needs');
            $this->product = new ProductVariablesPresenter($this->product, array_merge(['email' => config('mail.from.address')], GeoApiFacade::getPropertiesArr()));
            if ($this->product->primary_category !== null) {
                Breadcrumbs::addAssignBefore(['title' => $this->product->primary_category->name, 'url' => '/' . $this->product->primary_category->slug, 'originalUrl' => '/:slug?', 'baseFileName' => '/']);
            }
            Breadcrumbs::addAssign(['title' => $this->product->full_name, 'url' => '/:slug?' . $this->product->slug, 'originalUrl' => '/:slug?']);

            $this->setSeo($this->product->title, $this->product->description, $this->product->keywords, $this->product->image->image);

            return $this->renderPartial('@product', ['product' => $this->product, 'defaultImage' => $this->property('defaultImage')]);
        }
        return $this->renderPartial('@no-product', ['products' => Product::all()]);
    }

    /**
     * Render category template
     * @param $route
     * @return mixed
     */
    protected function renderCategory($route)
    {
        $this->category = $route->routeable;
        $this->category->load('products.categories');
        $this->category = new CategoryGeoPresenter($this->category);
        if ($this->category->parent !== null) {
            $currentLevel = $this->category;
            $chain = [];
            while ($parent = $currentLevel->parent) {
                $chain[] = $parent;
                $currentLevel = $parent;
            }
            $chain = array_reverse($chain);
            foreach ($chain as $parent) {
                Breadcrumbs::addAssignBefore(['title' => $parent->name, 'url' => '/' . $parent->slug, 'originalUrl' => $this->page->url]);
            }
        }
        Breadcrumbs::addAssign(['title' => $this->category->name, 'url' => '/' . $this->category->slug, 'originalUrl' => $this->page->url]);
        $this->setSeo($this->category->full_name, $this->category->description, $this->category->keywords, $this->category->image);
        $products = $this->category->products()->filter(\Input::only(static::filterAttributes(), ['per_page']))->sort()->paginate($this->perPage);
        $products->appends(\Input::only(array_merge(static::filterAttributes(), ['per_page'])));
        $this->setPageTitle();
        return $this->renderPartial('@category', [
            'products' => $products,
            'category' => $this->category,
            'perPage' => [
                'default' => $this->perPage,
                'variants' => $this->scatterPage
            ]
        ]);
    }

    /**
     * Render all products
     * @return mixed
     */
    protected function renderRoot()
    {
        $products = new Product();
        $products = $products->filter(\Input::only($this->filterAttributes(), ['per_page']))->sort()->paginate($this->perPage);

        $products->appends(\Input::only(array_merge($this->filterAttributes(), ['per_page'])));
        $this->setPageTitle();
        return $this->renderPartial('@root', [
            'products' => $products,
            'perPage' => [
                'default' => $this->perPage,
                'variants' => $this->scatterPage
            ]
        ]);
    }

    /**
     * Set seo attributes
     * @param $title
     * @param $description
     * @param $keywords
     * @param $image
     */
    protected function setSeo($title, $description, $keywords, $image = null)
    {
        $this->page->title = $title;
        $this->page->meta_title = $title;
        $this->page->meta_description = $description;
        $this->page->seo_keywords = $keywords;
        if ($image !== null) {
            $this->page->image = $image;
        }
    }

    /**
     * If has var page, set to title page number
     */
    protected function setPageTitle()
    {
        if (\Input::has('page')) {
            $this->page->title = $this->page->title . ' (стр. ' . \Input::get('page') . ')';
            $this->page->meta_title = $this->page->title;
        }
    }

    /**
     * @return array
     */
    public static function getSiteMap()
    {
        $partial = [];
        $categories = Category::with('products')->primaryOnly()->get();
        $i = 0;
        foreach ($categories as $category) {
            $partial[$i] = [
                'id' => $category->id,
                'slug' => $category->slug,
                'url' => url('/' . $category->slug),
                'title' => $category->title
            ];
            $i++;
        }
        $products = Product::all();
        foreach ($products as $product) {
            $partial[$i] = [
                'id' => $product->id,
                'slug' => $product->slug,
                'url' => url('/' . $product->slug),
                'title' => $product->title
            ];
            $i++;
        }
        return $partial;
    }
}
