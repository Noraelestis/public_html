<?php namespace Austerus\Shop\Components;

use Cms\Classes\ComponentBase;
use Austerus\Shop\Classes\Cart\Favorites;

class FavoritesFullComponent extends ComponentBase
{
    /**
     * @var Favorites
     */
    protected $favorites;

    public function componentDetails(): array
    {
        return [
            'name'        => 'Избранное полностью',
            'description' => 'Компонент для отображения страницы избранного'
        ];
    }

    public function onRun(): void
    {
        $this->favorites = new Favorites();
    }

    public function onRender(): string
    {
        return $this->renderPartial('@default', ['products' => $this->favorites->getProducts()]);
    }
}
