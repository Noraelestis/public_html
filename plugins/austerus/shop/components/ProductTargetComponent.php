<?php namespace Austerus\Shop\Components;

use Austerus\Shop\Models\Product as Goods;
use Austerus\Shop\Models\ProductTarget;
use Cms\Classes\ComponentBase;
use Illuminate\Support\Facades\Input;
use Austerus\Breadcrumbs\Components\Breadcrumbs;

class ProductTargetComponent extends ComponentBase
{
    protected $target;

    public function componentDetails()
    {
        return [
            'name'        => 'Цели спользования',
            'description' => 'Используется для вывода товаров с применением фильтра по целям использования'
        ];
    }

    public function defineProperties()
    {
        return [
            'partial' => [
                'title'             => 'Шаблон',
                'description'       => 'Рендеремый шаблон',
                'default'           => '@default',
                'type'              => 'text',
            ]
        ];
    }

    public function onRender()
    {
        $products = new Goods();
        $this->target = ProductTarget::whereSlug($this->param('slug'))->first();
        if ($this->target !== null) {
            $this->setSeo();
            $products = $products->whereHas('targets', function ($query) {
                $query->where('slug', '=', $this->target->slug);
            })->get();
            Breadcrumbs::addAssign(['title' => $this->target->title, 'url' => '/product-targets/', 'originalUrl' => $this->page->url]);
        }

        return $this->renderPartial($this->property('partial'), ['products' => $products, 'target' => $this->target]);
    }

    /**
     * Set seo attributes
     */
    protected function setSeo(){
        $this->page->title = $this->target->title;
        $this->page->meta_description = $this->target->description;
        $this->page->seo_keywords = $this->target->keywords;
        if(isset($this->target->image)){
            $this->page->image = $this->target->image;
        }
    }
}