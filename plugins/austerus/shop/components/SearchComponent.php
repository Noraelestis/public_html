<?php namespace Austerus\Shop\Components;

use Austerus\Shop\Models\Product;
use Austerus\Shop\Models\Category;
use Cms\Classes\ComponentBase;

class SearchComponent extends ComponentBase
{

    protected $fuzzy = 0.6;

    protected $result = [];

    public function componentDetails()
    {
        return [
            'name' => 'Компонент поиска',
            'description' => 'Осуществляет еластичный поиск по каталогу'
        ];
    }

    public function defineProperties()
    {
        return [
            'partial' => [
                'title' => 'Шаблон',
                'description' => 'Рендеремый шаблон',
                'default' => '@default',
                'type' => 'text',
            ]
        ];
    }

    public function onRun()
    {
        $this->result = ['products' => collect([]), 'categories' => collect([])];
        $query = \Input::get('q');
        if (is_numeric($query)) {
            $product = Product::getBuilder()->where('product_code', '=', $query)->first();
            if ($product !== null) {
                return redirect('/' . $product->slug);
            }
        } else {
            $this->result['products'] = Product::getBuilder()->searchWhere($query, ['product_code', 'name', 'content'])->get()->keyBy('id');
            $this->result['categories'] = Category::getBuilder()->searchWhere($query, ['name'])->get();
        }
    }

    public function onRender()
    {
        return $this->renderPartial($this->property('partial'), ['result' => $this->result]);
    }

    protected function filterQuery($query)
    {
        return preg_replace('/(,)([^ ])/u', ', ${2}', $query);
    }
}
