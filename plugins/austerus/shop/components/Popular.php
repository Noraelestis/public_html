<?php namespace Austerus\Shop\Components;

use Cms\Classes\ComponentBase;
use Austerus\Shop\Models\Product as Goods;

class Popular extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name' => 'Популярные товары',
            'description' => 'Выводит популярные товары'
        ];
    }

    public function defineProperties(): array
    {
        return [
            'partial' => [
                'title' => 'Фрагмент',
                'description' => 'Фрагемент для вывода компонента',
                'default' => '@default',
                'type' => 'text'
            ],
            'limit' => [
                'title' => 'Лимит',
                'description' => 'Максимальное кол-во записей',
                'default' => 9,
                'type' => 'string',
                'validationPattern' => '^[0-9]+$',
                'validationMessage' => 'Только цифры'
            ]
        ];
    }

    public function onRender(): string
    {
        $products = Goods::getPopular($this->property('limit'))->with('categories')->get();
        if (!$products->isEmpty()) {
            return $this->renderPartial($this->property('partial'), ['products' => $products]);
        }
        return '';
    }
}