<?php namespace Austerus\Shop\Components;

use Austerus\Shop\Classes\Cart\Cart;
use Austerus\Shop\Models\Product;
use Cms\Classes\ComponentBase;
use Symfony\Component\HttpFoundation\JsonResponse;

class ShoppingCart extends ComponentBase
{
    /**
     * @var Cart
     */
    protected $cart;

    /**
     * @return array
     */
    public function componentDetails(): array
    {
        return [
            'name' => 'Корзина заказов',
            'description' => 'Компонент реализует карзину товаров'
        ];
    }

    /**
     * Properties for use in components
     * @return array
     */
    public function defineProperties(): array
    {
        return [];
    }

    public function onRun()
    {
        $this->cart = new Cart();
    }

    /**
     * @return mixed
     */
    public function onRender()
    {
        return $this->renderPartial('@default', [
            'products' => $this->cart->getProducts(),
            'price' => $this->cart->getResultPrice(),
            'labels' => $this->getLabels(),
        ]);
    }

    public function getCart(): Cart
    {
        return $this->cart;
    }

    public function getLabels(): array
    {
        return [
            'product' => $this->getProductLabel(),
        ];
    }

    public function onAddProductToCart(): JsonResponse
    {
        $this->onRun();
        try {
            $product = $this->cart->addProduct((int)(post('id')), (int)(post('count', 1)));
            return \Response::json([
                'status' => 'success',
                'renderFull' => $this->renderPartial('ShoppingCartFull/item', ['product' => $product]),
                'render' => $this->renderPartial('@item', ['product' => $product]),
                'priceResult' => $this->cart->getResultPrice(),
                'count' => $this->cart->getCount(),
                'countFull' => $this->cart->getFullCount(),
                'sum' => $product->getSumPrice(),
                'discount' => $this->cart->getResultDiscount(),
                'quantity' => $product->getCount(),
                'labels' => $this->getLabels(),
                'fromFullCart' => post('full'),
            ]);
        } catch (\Exception $e) {
            return \Response::json(['status' => 'error']);
        }
    }

    public function onChangeProductCount(): JsonResponse
    {
        $this->onRun();
        if ($product = $this->cart->setCount((int)post('id'), (int)post('count'))) {
            return \Response::json([
                'status' => 'success',
                'priceResult' => $this->cart->getResultPrice(),
                'count' => $this->cart->getCount(),
                'countFull' => $this->cart->getFullCount(),
                'sum' => $product->getSumPrice(),
            ]);
        }
        return \Response::json(['status' => 'error']);
    }

    public function onProductPlusFromCart(): JsonResponse
    {
        $this->onRun();
        if ($product = $this->cart->plusCount((int)(post('id')))) {
            return \Response::json([
                'status' => 'success',
                'priceResult' => $this->cart->getResultPrice(),
                'count' => $this->cart->getCount(),
                'countFull' => $this->cart->getFullCount(),
                'sum' => $product->getSumPrice(),
                'discount' => $this->cart->getResultDiscount(),
            ]);
        }
        return \Response::json(['status' => 'error']);
    }

    public function onProductMinusFromCart(): JsonResponse
    {
        $this->onRun();
        if ($product = $this->cart->minusCount((int)(post('id')))) {
            return \Response::json([
                'status' => 'success',
                'priceResult' => $this->cart->getResultPrice(),
                'count' => $this->cart->getCount(),
                'countFull' => $this->cart->getFullCount(),
                'sum' => $product->getSumPrice(),
                'discount' => $this->cart->getResultDiscount(),
            ]);
        }
        return \Response::json(['status' => 'error']);
    }

    public function onProductDeleteFromCart()
    {
        try {
            $this->onRun();
            $this->cart->deleteProduct((int)post('id'));
            return \Response::json([
                'status' => 'success',
                'discount' => $this->cart->getResultDiscount(),
                'priceResult' => $this->cart->getResultPrice(),
                'count' => $this->cart->getCount(),
                'countFull' => $this->cart->getFullCount(),
                'labels' => $this->getLabels(),
            ]);
        } catch (\Exception $e) {
            return \Response::json(['status' => 'error']);
        }
    }


    public function onProductsResetFromCart()
    {
        try {
            $this->onRun();
            $this->cart->clearCart();
            return \Response::json([
                'status' => 'success',
            ]);
        } catch (\Exception $e) {
            return \Response::json(['status' => 'error']);
        }
    }

    protected function getProductLabel(): string
    {
        if ($this->cart->getCount() === 1) {
            return 'товар';
        }
        return $this->cart->getCount() < 5 ? 'товара' : 'товаров';
    }
}
