<?php namespace Austerus\Shop\Components;

use Austerus\Shop\Models\Banner;
use Cms\Classes\ComponentBase;

class BannerComponent extends ComponentBase
{
    /**
     * @var Banner|null
     */
    public $banners;

    public function componentDetails()
    {
        return [
            'name' => 'Баннеры',
            'description' => 'Компонент выводящий баннеры на страницы'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $url = $this->page->controller->getRouter()->getUrl();
        if ($url[0] !== '/') {
            $url = '/' . $url;
        }
        $this->banners = Banner::getForUrl($url);
    }

    /**
     * Return all banners
     * @return Banner|null
     */
    public function banners()
    {
        return $this->banners;
    }

    /**
     * return first banner
     * @return mixed
     */
    public function banner()
    {
        return $this->banners->first();
    }

}
