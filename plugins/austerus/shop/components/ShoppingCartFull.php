<?php namespace Austerus\Shop\Components;

use Austerus\Shop\Classes\Cart\Cart;
use Cms\Classes\ComponentBase;

class ShoppingCartFull extends ComponentBase
{
    /**
     * @var Cart
     */
    protected $cart;

    /**
     * @return array
     */
    public function componentDetails()
    {
        return [
            'name' => 'Корзина',
            'description' => 'Компонент реализует карзину товаров с формой заказов'
        ];
    }

    /**
     * Properties for use in components
     * @return array
     */
    public function defineProperties()
    {
        return [
            'order_path' => [
                'title' => 'Путь к форме заявки',
                'default' => '/order',
                'type' => 'text'
            ],
        ];
    }

    /**
     * Run component
     */
    public function onRun()
    {
        $this->cart = new Cart();
    }

    /**
     * @return mixed
     */
    public function onRender()
    {
        return $this->renderPartial('@default', [
            'products' => $this->cart->getProducts(),
            'order_id' => $this->cart->getCartId(),
            'count' => $this->cart->getCount(),
            'countFull' => $this->cart->getFullCount(),
            'discount' => $this->cart->getResultDiscount(),
            'price' => $this->cart->getResultPrice(),
            'options' => $this->properties,
        ]);
    }
}
