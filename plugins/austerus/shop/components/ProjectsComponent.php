<?php namespace Austerus\Shop\Components;

use Austerus\Breadcrumbs\Components\Breadcrumbs;
use Austerus\Shop\Models\Project;
use Cms\Classes\ComponentBase;

class ProjectsComponent extends ComponentBase
{

    protected $projects = Project::class;

    protected $view;

    public function componentDetails()
    {
        return [
            'name' => 'Проекты',
            'description' => 'Компонент для вывода проектов'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    /**
     * @return \Illuminate\Http\Response|null
     */
    public function onRun()
    {
        $this->projects = new $this->projects();
        if ($this->param('slug') === null) {
            $projects = $this->projects->paginate(20);
            $this->view = $this->renderPartial('projects/main', ['projects' => $projects]);
        } else {
            $project = $this->projects->whereSlug($this->param('slug'))->first();
            if ($project === null) {
                return \Response::make(\View::make('cms::404'), 404);
            }
            else {
                $this->page->title = $project->title;
                $this->page->meta_description = \Str::limit($project->content, 160);
                if (!empty($project->images)) {
                    $this->page->image = $project->images[0]->url;
                }
                Breadcrumbs::addAssignAfter(['title' => $project->title, 'url' => '/' . $project->slug, 'originalUrl' => '/project/'. $project->slug]);
            }
            $this->view = $this->renderPartial('projects/full', ['project' => $project]);
        }
        return null;
    }

    /**
     * @return mixed
     */
    public function onRender()
    {
        return $this->view;
    }
}
