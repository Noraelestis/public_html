<?php namespace Austerus\Shop\Components;

use austerus\FormBuilder\Models\FormLog;
use Austerus\Shop\Classes\Cart\Cart;
use Austerus\Shop\Classes\Cart\Invoice;
use Austerus\Shop\Classes\Presenters\ProductCartPresenter;
use Austerus\Shop\Models\Order;
use Cms\Classes\ComponentBase;
use Illuminate\Contracts\Mail\Mailable;
use Illuminate\Mail\Message;
use October\Rain\Database\Collection;
use System\Models\File;

class OrderComponent extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name' => 'Форма заявки',
            'description' => 'Форма заказа товаров'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRender()
    {
        $cart = new Cart();
        return $this->renderPartial('@default', [
            'options' => $this->properties,
            'cart' => $cart,
            'price' =>  $cart->getResultPrice(),
        ]);
    }

    public function onSubmitOrder()
    {
        try {
            $card = new Cart();
            $data = \Input::all();
            $order = new Order();
            $order->name = $data['name'];
            $order->email = post('email', '');
            $order->phone = $data['phone'];
            $order->description = post('description', '');

            if ($order->phone !== '') {
                $invoice = new Invoice($this, $order, post('type', 0));
                $card->getProducts()->each(function (ProductCartPresenter $product) use ($invoice) {
                    $invoice->addProduct($product, $product->getCount());
                });
                $order->details = $invoice->getDetails();
                $order->save();
                $files = $this->getFiles();
                $callback = null;
                if ($invoice->getCustomerType() === 2) {
                    $callback = function ($message) use ($files) {
                        if ($files->count() > 0) {
                            $files->each(function (File $file) use ($message) {
                                $message->attach($file->getLocalPath());
                            });
                        }
                    };
                }

//                todo при разработке не работал Mail
                \Mail::queue('austerus.shop::mail.order', ['order' => $order, 'invoice' => $invoice->invoice()], function (Mailable $message) use ($callback) {
                    $message->to(config('mail.from.address'), config('mail.from.name'));
                    $message->subject('Заказ на сайте');
                    if (null !== $callback) {
                        $callback($message);
                    }
                });
                $card->clearCart();
                return \Response::json([
                    'status' => 'success',
                    'result' => $this->renderPartial('@success'),
                ]);
            }
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
        }
        return \Response::json([
            'status' => 'error',
            'result' => $this->renderPartial('@error'),
        ]);
    }

    private function getFiles(): Collection
    {
        if ($sessionKey = $this->getSessionKey()) {
            $model = new FormLog();

            $deferredQuery = $model
                ->files()
                ->withDeferred($sessionKey)
                ->orderBy('id', 'desc');
            return $deferredQuery->get();
        }
        return new Collection([]);
    }

    public function getSessionKey()
    {
        return post('_session_key');
    }
}
