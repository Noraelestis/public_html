<?php namespace Austerus\Shop\Components;

use Austerus\Shop\Models\Manufacturer;
use Cms\Classes\ComponentBase;

class ManufacturersSlider extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Слайдер производителей',
            'description' => 'Выводит логотипы производителей в виде слайдера'
        ];
    }

    public function defineProperties()
    {
        return [
            'partial' => [
                'title' => 'Шаблон',
                'description' => 'Рендеремый шаблон',
                'default' => 'products/manufacturers-slider.htm',
                'type' => 'text',
            ],
        ];
    }

    public function onRender()
    {
        $manufacturers = Manufacturer::whereNotNull('image')->get();
        return $this->renderPartial($this->property('partial'), compact('manufacturers'));
    }
}
