<?php namespace Austerus\Shop\Components;

use Austerus\Shop\Models\Product;
use Cms\Classes\ComponentBase;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class FastAdding extends ComponentBase
{
    public $result = [];

    public function onType(  )
    {
        $inputText = null;
        $inputText = post('fastAddingInput');
        if ($inputText){
            $this->result['products'] = Product::getBuilder()->searchWhere($inputText, ['product_code', 'name'])->limit(10)->get()->keyBy('id');
            return $this->result;
        }
        return [];
    }

    public function componentDetails()
    {
        return [
            'name' => 'FastAdding Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }


    public function onRender()
    {
//        return $this->renderPartial('@default', ['product' => Product::getForSlug($this->param('slug'))]);
    }
}
