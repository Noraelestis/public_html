<?php namespace Austerus\Shop\Components;

use Cms\Classes\ComponentBase;

class QuizComponent extends ComponentBase
{

    public function onSendQuizForm()
    {

        try {
            $data = [];
            $data['page'] = \URL::current();
            $data['answer1'] = post('answer1');
            $data['answer2'] = post('answer2');
            $data['answer3'] = post('answer3');
            $data['name'] = post('name');
            $data['phone'] = post('phone');
            $data['email'] = post('email');
            \Mail::queue('austerus.shop::emails.get-quiz-result', $data, function ($message) {
                $message->to(config('mail.from.address'), config('mail.from.name'));
                $message->subject('Получить специальную цену');
            });
            return \Response::json([
                'status' => 'success',
                'data' => $data,
            ]);
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
        }
        return \Response::json([
            'status' => 'error',
        ]);
    }

    public function componentDetails()
    {
        return [
            'name'        => 'QuizComponent Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

}
