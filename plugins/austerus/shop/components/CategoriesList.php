<?php namespace Austerus\Shop\Components;

use Austerus\Shop\Models\Category;
use Cms\Classes\ComponentBase;

class CategoriesList extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name' => 'Список категорий',
            'description' => 'Выводит категории в выбранный шаблон'
        ];
    }

    public function defineProperties()
    {
        return [
            'partial' => [
                'title' => 'Фрагмент',
                'description' => 'Фрагемент для вывода компонента',
                'default' => '@default',
                'type' => 'text'
            ],
            'limit' => [
                'title' => 'Максимум',
                'description' => 'Ограничение на кол-во выводимых категорий(если 0 то выводятся все). ',
                'default' => 0,
                'type' => 'string',
                'validationPattern' => '^[0-9]+$',
                'validationMessage' => 'Только число'
            ],
            'primary' => [
                'title' => 'Только основные',
                'description' => 'Если да, то будут выводиться только категории выделенные как основные',
                'default' => false,
                'type' => 'checkbox'
            ]
        ];
    }

    public function onRender()
    {
        $categories = new Category();
        if ($this->property('primary')) {
            $categories = $categories->primaryOnly();
        }
        if ($this->property('limit') != 0) {
            $categories = $categories->limit($this->property('limit'));
        }
        $categories = $categories->orderAsc()->get();

        return $this->renderPartial($this->property('partial'), ['categories' => $categories]);
    }
}