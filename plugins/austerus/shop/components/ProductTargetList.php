<?php namespace Austerus\Shop\Components;

use Austerus\Shop\Models\ProductTarget;
use Cms\Classes\ComponentBase;

class ProductTargetList extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Цели использования',
            'description' => 'Выводит ссылки на товары по целям использования'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRender(){
        $productsTarget = ProductTarget::all();
        $content = '';
        foreach ($productsTarget as $productTarget){
            $content.= $this->renderPartial('@default', compact('productTarget'));
        }
        return $content;
    }
}