<?php namespace Austerus\Shop\Components;

use Cms\Classes\ComponentBase;

class FreeProjectForm extends ComponentBase
{
    protected $fields = [
        'inside' => [
            'length_inside',
            'width_inside',
            'height_inside',
//            'material_inside',
//            'color_inside',
//            'suspension_height_inside',
//            'type_fastening_inside',
//            'current_lamp_inside',
//            'count_lamp_inside',
            'lux_inside',
            'type_room_inside',
            'type_room_other_inside'
        ],
        'outside' => [
            'type_room_outside',
            'type_room_other_outside',
            'length_outside',
            'width_outside',
            'suspension_height_outside',
//            'type_fastening_outside',
//            'current_lamp_outside',
//            'count_lamp_outside',
            'lux_outside'
        ],
        'common' => [
            'name',
            'email',
            'phone',
            'company',
            'state',
            'city',
            'files'
        ]
    ];

    private $status = false;

    public function componentDetails()
    {
        return [
            'name' => 'Форма бесплатный проект освещения',
            'description' => 'Компонент для вывода формы бесплатного освещения и обработки данных'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        if (\Input::method() === 'POST') {
            if (!\Validator::make(\Input::all(), [
                'file1' => 'image',
                'file2' => 'image',
                'file3' => 'image'
            ])->fails()
            ) {
                $fileName = [];
                $type = \Input::get('order_type');
                if ($type === 'inside') {
                    $data = \Input::only(array_merge($this->fields['inside'], $this->fields['common']));
                    $data['title'] = 'Заявка на расчет внутреннего освещения';
                } else {
                    $data = \Input::only(array_merge($this->fields['outside'], $this->fields['common']));
                    $data['title'] = 'Заявка на расчет наружнего освещения';
                }
                $data['files'] = $fileName;

                if (!empty($data['phone'])) {
                    $this->status = true;
                    $result = [];
                    if (\Input::hasFile('file1')) {
                        $fileName[0] = str_random(32) . '.' . \Input::file('file1')->getClientOriginalExtension();
                        \Input::file('file1')->move(storage_path() . '/app/uploads/projects', $fileName[0]);
                    }
                    if (\Input::hasFile('file2')) {
                        $fileName[1] = str_random(32) . '.' . \Input::file('file2')->getClientOriginalExtension();
                        \Input::file('file2')->move(storage_path() . '/app/uploads/projects', $fileName[1]);
                    }
                    if (\Input::hasFile('file3')) {
                        $fileName[2] = str_random(32) . '.' . \Input::file('file3')->getClientOriginalExtension();
                        \Input::file('file3')->move(storage_path() . '/app/uploads/projects', $fileName[2]);
                    }
                    foreach ($data as $key => $value) {
                        $key = strtr($key, ['_inside' => '', '_outside' => '']);
                        $result[$key] = $value;
                    }
                    $data = $result;
                    $data['page'] = \URL::current();
                    \Mail::queue('austerus.shop::emails.get-free-project', $data, function ($message) use ($data) {

                        $message->to(config('mail.from.address'), config('mail.from.name'));
                        $message->subject($data['title']);

                    });
                }
            }
        }
    }

    public function onRender()
    {
        return $this->renderPartial('partials/free_project_form', ['projectFormResponse' => $this->status]);
    }
}
