<?php namespace Austerus\Shop\Components;

use Austerus\Shop\Models\Category;
use Austerus\Shop\Models\Slider;
use Cms\Classes\ComponentBase;
use October\Rain\Database\Collection;

class SliderComponent extends ComponentBase
{
    /**
     * @var Collection
     */
    protected $slides;

    /**
     * @var string
     */
    protected $type = 'images';

    public function componentDetails()
    {
        return [
            'name' => 'Слайдер',
            'description' => 'Компонент выводит слайды на главной странице'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $this->slides = new Collection();
        $slug = $this->controller->getRouter()->getParameter('slug');
        if ($slug !== null) {
            $category = Category::whereSlug($slug)->first();
            if ($category !== null) {
                $this->slides = Slider::findByCategory($category);
//                if (!$this->slides->isEmpty()) {
//                    $this->type = 'products';
//                }
            }
        }
    }

    /**
     * @return mixed|string
     */
    public function onRender(): string
    {
        if (!$this->slides->isEmpty()) {
            return $this->renderPartial('@default', ['slides' => $this->slides, 'type' => $this->type]);
        }
        return $this->renderPartial('@default', ['slides' => $this->slides, 'type' => 'empty']);
    }
}
