<?php namespace Austerus\Shop\Components;

use Austerus\Shop\Models\Product;
use Cms\Classes\ComponentBase;

class Callback extends ComponentBase
{
    public function onSendLowPriceForm()
    {
        try {
            $data = [];
            $data['page'] = \URL::current();
            $data['currentPrice'] = post('typeLowPrice');
            $data['otherPrice'] = post('otherLowPrice');
            $data['name'] = post('name');
            $data['link'] = post('linkToOtherPrice');
            $data['phone'] = post('phone');
            $data['quantity'] = post('quantity')[post('productId')];
            $data['productId'] = post('productId');
            \Mail::queue('austerus.shop::emails.callback', $data, function ($message) {
                $message->to(config('mail.from.address'), config('mail.from.name'));
                $message->subject('Найти товар дешевле');
            });
            return \Response::json([
                'status' => 'success',
                'data' => $data,
            ]);
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
        }
        return \Response::json([
            'status' => 'error',
        ]);
    }

    public function componentDetails()
    {
        return [
            'name' => 'Callback Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRender()
    {
        return $this->renderPartial('@default', ['product' => Product::getForSlug($this->param('slug'))]);
    }
}
