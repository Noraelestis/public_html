<?php namespace Austerus\Shop\Components;

use Austerus\Shop\Models\Category;
use Austerus\Shop\Models\Manufacturer;
use Austerus\Shop\Models\Product;
use Austerus\Shop\Models\Suspension;
use Cms\Classes\ComponentBase;
use Redirect;

class Filter extends ComponentBase
{
    protected $currentCategory;


    public function componentDetails()
    {
        return [
            'name' => 'Фильтр товаров',
            'description' => 'Компонент выводит о обрабатывает данные для фильтра товаров'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $router = $this->page->controller->getRouter();
        $this->currentCategory = Category::whereSlug($router->getParameter('slug'))->first();
        $fullUrl = \Request::fullUrl();
    }

    public function onRender()
    {
        $counter = new Product();
        $counter->setAppends([]);
        $filters = [];
        $filters ['param_1'] = ['name' => $this->currentCategory->name_param_1];
        $filters ['param_1']['values'] = $this->currentCategory->products()->groupBy('param_1')->get(['param_1'])->map(function (Product $product){
            return $product->param_1;
        });$filters ['param_2'] = ['name' => $this->currentCategory->name_param_2];
        $filters ['param_2']['values'] = $this->currentCategory->products()->groupBy('param_2')->get(['param_2'])->map(function (Product $product){
            return $product->param_2;
        });$filters ['param_3'] = ['name' => $this->currentCategory->name_param_3];
        $filters ['param_3']['values'] = $this->currentCategory->products()->groupBy('param_3')->get(['param_3'])->map(function (Product $product){
            return $product->param_3;
        });$filters ['param_4'] = ['name' => $this->currentCategory->name_param_4];
        $filters ['param_4']['values'] = $this->currentCategory->products()->groupBy('param_4')->get(['param_4'])->map(function (Product $product){
            return $product->param_4;
        });$filters ['param_5'] = ['name' => $this->currentCategory->name_param_5];
        $filters ['param_5']['values'] = $this->currentCategory->products()->groupBy('param_5')->get(['param_5'])->map(function (Product $product){
            return $product->param_5;
        });

        $counter = $counter->select([
            \DB::raw('MAX(price) as price')])->first()->toArray();
        return $this->renderPartial('@default', [
            'currentCategory' => $this->currentCategory,
            'counter' => $counter,
            'filters' => $filters
        ]);
    }
}
