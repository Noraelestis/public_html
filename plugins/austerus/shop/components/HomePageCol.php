<?php namespace Austerus\Shop\Components;

use Cms\Classes\ComponentBase;

class HomePageCol extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name' => 'Столбцы на главной',
            'description' => 'Компонент для показа столбцов с товарами на главной'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRender()
    {
        $cols = \Austerus\Shop\Models\HomePageCol::all()->keyBy('id');
        $styles = '';
        foreach ($cols as $col) {
            $styles = $styles . '#home-grid__item--' . $col->id . '{background-color:' . $col->color . '; background-image: url(/storage/app/media/' . $col->image . ')}';
        }

        return $this->renderPartial('home/grid', ['cols' => $cols, 'styles' => $styles]);
    }

}