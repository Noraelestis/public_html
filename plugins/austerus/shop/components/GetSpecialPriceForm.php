<?php namespace Austerus\Shop\Components;

use Austerus\Shop\Models\Product;
use Cms\Classes\ComponentBase;

class GetSpecialPriceForm extends ComponentBase
{
    public function onSendSpecialPriceForm()
    {
        try {
            $data = [];
            $data['page'] = \URL::current();
            $data['comment'] = post('comment');
            $data['companyName'] = post('companyName');
            $data['name'] = post('name');
            $data['quantity'] = post('quantity');
            $data['phone'] = post('phone');
            \Mail::queue('austerus.shop::emails.get-special-price', $data, function ($message) {
                $message->to(config('mail.from.address'), config('mail.from.name'));
                $message->subject('Получить специальную цену');
            });
            return \Response::json([
                'status' => 'success',
                'data' => $data,
            ]);
        } catch (\Exception $e) {
            \Log::error($e->getMessage());
        }
        return \Response::json([
            'status' => 'error',
            'data' => $data,
        ]);
    }

    public function componentDetails()
    {
        return [
            'name' => 'Callback Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRender()
    {
        return $this->renderPartial('@default', ['product' => Product::getForSlug($this->param('slug'))]);
    }
}
