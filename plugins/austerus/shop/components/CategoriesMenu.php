<?php namespace Austerus\Shop\Components;

use Austerus\Shop\Models\Category;
use Cms\Classes\ComponentBase;

class CategoriesMenu extends ComponentBase
{

    public function componentDetails()
    {
        return [
            'name' => 'Меню категорий',
            'description' => 'Выводит категории в виде меню'
        ];
    }

    public function defineProperties()
    {
        return [
            'partial' => [
                'title' => 'Шаблон',
                'description' => 'Рендеремый шаблон',
                'default' => '@default',
                'type' => 'text',
            ],
            'page' => [
                'title' => 'Страница категорий',
                'description' => 'Подставляется к ссылке перед псевдонимом',
                'default' => '/',
                'type' => 'text',
            ],
            'slugParam' => [
                'title' => 'Параметр псевдонима',
                'description' => 'Принимается из страницы для определения активной категории',
                'default' => 'slug',
                'type' => 'text',
            ],
            'root' => [
                'title' => 'Считать корнем',
                'description' => 'Считать началом',
                'default' => 'root',
                'type' => 'dropdown',
                'options' => [
                    'root' => 'root',
                    'current' => 'current'
                ]
            ],
            'pushUp' => [
                'title' => 'Текущая первой',
                'description' => 'Поднимать вверх текущую категорию',
                'type' => 'checkbox',
                'default' => true
            ]
        ];
    }

    public function onRender()
    {
        $parents = [];
        $categories = Category::orderAsc()->getNested();
        if ($categories !== null) {
            $active = $this->param($this->property('slugParam'));
            if ($active !== null && $this->property('pushUp')) {
                $category = Category::where('slug', $active)->first();
                if ($category !== null) {
                    $parents = $category->getParents();
                    if ($parents !== null) {
                        $parents = $parents->keyBy('id')->keys()->toArray();
                        if (!empty($parents)) {
                            $start = $categories->find($parents[0]);
                            $categories->forget($parents[0]);
                            $categories->prepend($start);
                        } else {
                            $start = $categories->where('slug', $active)->first();
                            $categories->forget($start->id);
                            $categories->prepend($start);
                        }
                    } else {
                        $parents = [];
                    }
                } else {
                    $parents = [];
                }
            }


            return $this->renderPartial($this->property('partial'), [
                'categories' => $categories,
                'page' => $this->property('page'),
                'active' => $active,
                'parents' => $parents
            ]);
        }
    }

}