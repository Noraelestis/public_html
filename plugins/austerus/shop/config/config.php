<?php

return [
    'aliases' => array_merge(config('app.aliases'), [

        'Excel' => Maatwebsite\Excel\Facades\Excel::class,
        'Datatables' => Yajra\Datatables\Facades\Datatables::class

    ])
];