<?php namespace Austerus\Shop;

use App;
use Austerus\Shop\Components\Favorites;
use Austerus\Shop\Components\FavoritesFullComponent;
use Austerus\Shop\Components\OrderComponent;
use Austerus\Shop\Components\SearchComponent;
use Austerus\Shop\Components\ShoppingCart;
use Austerus\Shop\Components\ShoppingCartFull;
use Austerus\Shop\Models\Settings;
use Backend;
use Config;
use Maatwebsite\Excel\ExcelServiceProvider;
use System\Classes\PluginBase;
use System\Classes\SettingsManager;
use Yajra\Datatables\DatatablesServiceProvider;

/**
 * shop Plugin Information File
 */
class Plugin extends PluginBase
{

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name' => 'shop',
            'description' => 'Магазин для светограда',
            'author' => 'Austerus',
            'icon' => 'icon-cart-plus'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {
    }

    /**
     * Boot method, called right before the request route.
     */
    public function boot()
    {
        Config::set('aliases', Config::get('austerus.shop::config.aliases'));
        App::register(ExcelServiceProvider::class);
        App::register(DatatablesServiceProvider::class);
    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return [
            'Austerus\Shop\Components\Callback' => 'Callback',
            'Austerus\Shop\Components\GetSpecialPriceForm' => 'GetSpecialPriceForm',
            'Austerus\Shop\Components\QuizComponent' => 'QuizComponent',
            'Austerus\Shop\Components\FastAdding' => 'FastAdding',
            'Austerus\Shop\Components\Shop' => 'Shop',
            'Austerus\Shop\Components\Popular' => 'Popular',
            'Austerus\Shop\Components\ProductTargetList' => 'ProductTargetList',
            'Austerus\Shop\Components\ProductTargetComponent' => 'ProductTargetComponent',
            'Austerus\Shop\Components\CategoriesList' => 'CategoriesList',
            'Austerus\Shop\Components\CategoriesMenu' => 'CategoriesMenu',
            'Austerus\Shop\Components\Sitemap' => 'Sitemap',
            'Austerus\Shop\Components\Filter' => 'Filter',
            'Austerus\Shop\Components\HomePageCol' => 'HomePageCol',
            'Austerus\Shop\Components\ManufacturersSlider' => 'ManufacturersSlider',
            'Austerus\Shop\Components\ProjectsComponent' => 'ProjectsComponent',
            'Austerus\Shop\Components\FreeProjectForm' => 'FreeProjectForm',
            'Austerus\Shop\Components\Manufacturers' => 'Manufacturers',
            'Austerus\Shop\Components\SliderComponent' => 'SliderComponent',
            'Austerus\Shop\Components\BannerComponent' => 'BannerComponent',
            ShoppingCart::class => 'ShoppingCart',
            Favorites::class => 'Favorites',
            FavoritesFullComponent::class => 'FavoritesFullComponent',
            ShoppingCartFull::class => 'ShoppingCartFull',
            SearchComponent::class => 'SearchComponent',
            OrderComponent::class => 'OrderComponent',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return [
            'austerus.shop.categories' => [
                'tab' => 'Каталог',
                'label' => 'Управление категориями'
            ],
            'austerus.shop.products' => [
                'tab' => 'Каталог',
                'label' => 'Управление товарами'
            ],
            'austerus.shop.product_targets' => [
                'tab' => 'Каталог',
                'label' => 'Управление целями использования'
            ],
            'austerus.shop.manufacturers' => [
                'tab' => 'Каталог',
                'label' => 'Управление производителями'
            ],
            'austerus.shop.suspensions' => [
                'tab' => 'Каталог',
                'label' => 'Управление типами крепления'
            ],
            'austerus.shop.csvmanager' => [
                'tab' => 'Каталог',
                'label' => 'Импорт/экспорт товаров'
            ],
            'austerus.shop.homepagecol' => [
                'tab' => 'Каталог',
                'label' => 'Управление колонками на главной'
            ],
            'austerus.shop.orders' => [
                'tab' => 'Каталог',
                'label' => 'Просмотр заказов'
            ],
            'austerus.shop.units' => [
                'tab' => 'Каталог',
                'label' => 'Управление единицами измерений'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return [
            'shop' => [
                'label' => 'Каталог',
                'url' => Backend::url('austerus/shop/products'),
                'icon' => 'icon-shopping-cart',
                'permissions' => ['austerus.shop.*'],
                'order' => 500,
                'sideMenu' => [
                    'categories' => [
                        'label' => 'Категории',
                        'icon' => 'icon-tags',
                        'url' => Backend::url('austerus/shop/categories'),
                        'permissions' => ['austerus.shop.categories'],
                    ],
                    'products' => [
                        'label' => 'Товары',
                        'icon' => 'icon-cart-plus',
                        'url' => Backend::url('austerus/shop/products/'),
                        'permissions' => ['austerus.shop.products'],
                    ],
                    'priceconfigs' => [
                        'label' => 'Цены',
                        'icon' => 'icon-money',
                        'url' => Backend::url('austerus/shop/priceconfigs'),
                        'permissions' => ['austerus.shop.*'],
                    ],
                    'producttargets' => [
                        'label' => 'Где используется',
                        'icon' => 'icon-dot-circle-o',
                        'url' => Backend::url('austerus/shop/producttargets'),
                        'permissions' => ['austerus.shop.product_targets'],
                    ],
                    'manufacturers' => [
                        'label' => 'Производители',
                        'icon' => 'icon-industry',
                        'url' => Backend::url('austerus/shop/manufacturers'),
                        'permissions' => ['austerus.shop.manufacturers'],
                    ],
                    'suspensions' => [
                        'label' => 'Типы крепления',
                        'icon' => 'icon-paperclip',
                        'url' => Backend::url('austerus/shop/suspensions'),
                        'permissions' => ['austerus.shop.suspensions'],
                    ],
                    'csvmanager' => [
                        'label' => 'Импорт/Экспорт',
                        'icon' => 'icon-file-excel-o',
                        'url' => Backend::url('austerus/shop/csvmanager'),
                        'permissions' => ['austerus.shop.csvmanager'],
                    ],
                    'newHomePageCol' => [
                        'label' => 'Колонки на главной',
                        'icon' => 'icon-home',
                        'url' => Backend::url('austerus/shop/HomePageCol'),
                        'permissions' => ['austerus.shop.homepagecol'],
                    ],
                    'orders' => [
                        'label' => 'Заказы',
                        'icon' => 'icon-credit-card',
                        'url' => Backend::url('austerus/shop/orders'),
                        'permissions' => ['austerus.shop.orders'],
                    ],
                    'units' => [
                        'label' => 'Единицы измерения',
                        'icon' => 'icon-balance-scale',
                        'url' => Backend::url('austerus/shop/units'),
                        'permissions' => ['austerus.shop.*'],
                    ],
                    'projects' => [
                        'label' => 'Проекты',
                        'icon' => 'icon-map-o',
                        'url' => Backend::url('austerus/shop/projects'),
                        'permissions' => ['austerus.shop.*'],
                    ],
                    'sliders' => [
                        'label' => 'Слайды',
                        'icon' => 'icon-picture-o',
                        'url' => Backend::url('austerus/shop/sliders'),
                        'permissions' => ['austerus.shop.*'],
                    ],
                    'banner' => [
                        'label' => 'Баннеры',
                        'icon' => 'icon-object-group',
                        'url' => Backend::url('austerus/shop/banners'),
                        'permissions' => ['austerus.shop.*'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Register twig extensions
     * @return array
     */
    public function registerMarkupTags()
    {
        return [
            'functions' => [
                'priceFormatting' => function ($val) {
                    return number_format($val, 0, '', ' ');
                },
                'in_array' => function ($needle, $haystack) {
                     return is_array($haystack) && is_array($needle) ? in_array($needle, $haystack) : false;
//            if (is_array($haystack))
//                    return in_array($needle, $haystack);
                },
            ]
        ];
    }


    /**
     * Register the settings.
     *
     * @return array
     */
    public function registerSettings()
    {
        return [

            'settings' => [
                'label' => 'Настройки магазина',
                'description' => 'Настраиваются компонента магазина',
                'category' => 'Магазин',
                'icon' => 'icon-shopping-cart',
                'class' => Settings::class,
                'order' => 100
            ]

        ];
    }
}
