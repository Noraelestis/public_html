<?php namespace Austerus\Shop\Traits;


use Austerus\Shop\Models\Category;

trait FastAccessProducts
{
    private $_categoryList = false;

    public function getCategoryList(){
        if(!$this->_categoryList){
            $this->_categoryList = Category::with('products')->get();
        }
        return $this->_categoryList;
    }
}
