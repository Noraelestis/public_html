<?php namespace Austerus\Shop\Controllers;

use Austerus\Shop\Models\Category;
use BackendMenu;
use Backend\Classes\Controller;
use Illuminate\Database\Eloquent\Builder;

/**
 * Categories Back-end Controller
 */
class Categories extends Controller
{

    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
        'Backend.Behaviors.ReorderController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';
    public $reorderConfig = 'config_reorder.yaml';

    public function __construct()
    {
        parent::__construct();
        $this->addJs('/plugins/austerus/shop/assets/sticky.js', '0.0.1');

        BackendMenu::setContext('Austerus.Shop', 'shop', 'categories');
    }
}