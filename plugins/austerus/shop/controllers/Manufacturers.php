<?php namespace Austerus\Shop\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Manufacturers Back-end Controller
 */
class Manufacturers extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        $this->addJs('/plugins/austerus/shop/assets/sticky.js', '0.0.1');
        BackendMenu::setContext('Austerus.Shop', 'shop', 'manufacturers');
    }
}