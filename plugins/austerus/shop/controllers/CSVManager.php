<?php namespace Austerus\Shop\Controllers;

use Austerus\Shop\Models\Category;
use Austerus\Shop\Models\Manufacturer;
use Austerus\Shop\Models\Product;
use Backend\Classes\Controller;
use BackendMenu;
use Config;
use Illuminate\Support\Facades\Input;
use Maatwebsite\Excel\Facades\Excel;
use October\Rain\Halcyon\Exception\ModelException;

/**
 * C S V Manager Back-end Controller
 */
class CSVManager extends Controller
{

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    protected $availableDelimiters = [';', ','];
    protected $availableExtensions = ['csv', 'xls', 'xlsx'];

    protected $availableFields = [
        'id',
        'name',
        'product_code',
        'full_name',
        'title',
        'description',
        'keywords',
        'custom_title_content',
        'content',
        'slug',
        'purchase_price',
        'price',
        'special_price',
        'wholesale_price',
        'product_target_id',
        'guarantee',
        'weight',
        'length',
        'width',
        'height',
        'manufacturer_id',
        'custom_title_additional_options',
        'additional_options',
        'custom_title_benefits_led',
        'benefits_led',
        'custom_title_compliance',
        'compliance',
        'custom_title_specifications',
        'sort',
        'pdf_description',
        'images',
        'deleted_at',
        'custom_text',
        'unit_id',
        'param_1',
        'param_2',
        'param_3',
        'param_4',
        'param_5',
        'video'
    ];

    protected $appendAvailableFields = ['rubric', 'active', 'categories', 'suspensions', 'targets', 'images', 'product_relations', 'product_also_needs', 'analogues'];

    public function __construct()
    {
        parent::__construct();
        Config::set('excel.csv.delimiter', ';');
        $this->addJs('/plugins/austerus/shop/assets/sticky.js', '0.0.1');
        BackendMenu::setContext('Austerus.Shop', 'shop', 'csvmanager');
    }

    public function index()
    {
        $this->pageTitle = 'Импорт/Экспорт';
    }

    public function export()
    {
        $this->pageTitle = 'Экспорт товаров';
        if (\Input::isMethod('post')) {
            $extension = \Input::get('extension');
            if (!in_array($extension, $this->availableExtensions)) {
                $extension = 'xls';
            }
            if ($extension === 'csv') {
                $delimiter = \Input::get('delimiter');
                if (!in_array($delimiter, $this->availableDelimiters)) {
                    $delimiter = ';';
                }
                \Config::set('excel.csv.delimiter', $delimiter);
            }

            $products = new Product;
            $products = $products
                ->select($this->availableFields)
                ->with('categories', 'suspensions', 'targets', 'manufacturer', 'product_relations', 'product_also_needs', 'analogues')
                ->withTrashed()
                ->get()
                ->toArray();
            $label = [];

            $manufacturers = Manufacturer::all()->keyBy('id');

            foreach ($products as $key => $product) {
                $implodeCategories = [];
                foreach ($product['categories'] as $category) {
                    $implodeCategories[] = $category['name'];
                }
                $implodeCategories = implode(';', $implodeCategories);
                $products[$key]['categories'] = $implodeCategories;

                $implodeTargets = [];
                foreach ($product['targets'] as $targets) {
                    $implodeTargets[] = $targets['id'];
                }
                $implodeTargets = implode(';', $implodeTargets);
                $products[$key]['targets'] = $implodeTargets;

                //suspensions
                $implodeSuspensions = [];
                foreach ($product['suspensions'] as $suspensions) {
                    $implodeSuspensions[] = $suspensions['id'];
                }
                $implodeSuspensions = implode(';', $implodeSuspensions);
                $products[$key]['suspensions'] = $implodeSuspensions;

                //product_relations
                $implodeRelative = [];
                foreach ($product['product_relations'] as $relation) {
                    $implodeRelative[] = $relation['id'];
                }
                $implodeRelative = implode(';', $implodeRelative);
                $products[$key]['product_relations'] = $implodeRelative;

                $implodeAlsoNeed = [];
                foreach ($product['product_also_needs'] as $relation) {
                    $implodeAlsoNeed[] = $relation['id'];
                }
                $implodeAlsoNeed = implode(';', $implodeAlsoNeed);
                $products[$key]['product_also_needs'] = $implodeAlsoNeed;

                $implodeAnalogue = [];
                foreach ($product['analogues'] as $analogue) {
                    $implodeAnalogue[] = $analogue['id'];
                }
                $implodeAnalogue = implode(';', $implodeAnalogue);
                $products[$key]['analogues'] = $implodeAnalogue;

                $implodeImages = [];
                foreach ($product['images'] as $images) {
                    if (isset($images['image'])) {
                        $implodeImages[] = $images['image'];
                    }
                }
                $implodeImages = implode(';', $implodeImages);
                $products[$key]['images'] = $implodeImages;
                if ($product['active'] === true) {
                    $products[$key]['active'] = '+';
                } else {
                    $products[$key]['active'] = '-';
                }

                $manufacturer = $manufacturers->get($product['manufacturer_id']);
                if ($manufacturer !== null) {
                    $products[$key]['manufacturer_id'] = $manufacturer->name;
                }
                unset($products[$key]['manufacturer']);
            }
            foreach ($products[0] as $key => $product) {
                $label[trans('austerus.shop::products.labels.' . $key)] = $product;
            }
            $products[0] = $label;
            return Excel::create(\env('APP_NAME') . 'Export_' . date('d_m_Y'), function ($excel) use ($products) {

                $excel->sheet(date('d_m_Y'), function ($sheet) use ($products) {

                    $sheet->fromArray($products, null, 'A1', false, true);
                });

            })->download($extension);
        }
    }

    public function import()
    {
        \October\Rain\Database\Model::unguard();

        $errors = false;
        $this->pageTitle = 'Импорт';
        $this->addJs('/plugins/austerus/shop/controllers/csvmanager/assets/bootstrap.file-input.js');
        if (Input::isMethod('post')) {
            $validator = \Validator::make(['file' => Input::file('file')], ['file' => 'required']);
            if ($validator->fails()) {
                \Flash::error('Выберите файл');
            }
            if (in_array(Input::file('file')->getClientOriginalExtension(), $this->availableExtensions)) {
                $file = Input::file('file');
                $newName = 'import_file' . '.' . $file->getClientOriginalExtension();
                $path = storage_path() . '/app/import';
                try {
                    $file->move($path, $newName);
                    Config::set('excel.csv.delimeter', ';');
                    Excel::load($path . '/' . $newName, function ($reader) {
                        $products = Product::withTrashed()->get()->keyBy('id');
                        $import = $reader->all()->toArray();

                        if (isset($import[0]) && !isset($import[0]['id'])) {
                            $import = $import[0];
                        }

                        $this->availableFields = array_merge($this->availableFields, $this->appendAvailableFields);
                        \DB::transaction(function () use ($import, $products) {
                            $manufacturers = Manufacturer::all()->keyBy('name');
                            $stockTransArray = trans('austerus.shop::products.labels');
                            $transArray = [];
                            foreach ($stockTransArray as $key => $transItem) {
                                $transArray[str_slug($transItem, '_')] = str_slug($key, '_');
                            }

                            foreach ($import as $item) {
                                $product = $products->get((int)$item['id']);
                                if ($product !== null) {
                                    $products->forget($product->id);
                                }

                                if (!empty($item['proizvoditel'])) {
                                    $manufacturer = $manufacturers->get($item['proizvoditel']);
                                } else {
                                    $manufacturer = null;
                                }

                                $original = [];
                                $i = 0;

                                foreach ($item as $key => $value) {
                                    $key = str_slug($key, '_');
                                    if (isset($transArray[$key])) {
                                        $original[$transArray[$key]] = $value;
                                    } else {
                                        $original[$key] = $value;
                                    }
                                    $i++;
                                }
                                if ($original['active'] === '-') {
                                    $original['active'] = false;
                                } else {
                                    $original['active'] = true;
                                }

                                $relations = [
                                    'categories' => $original['categories'],
                                    'targets' => $original['targets'],
                                    'images' => $original['images'],
                                    'suspensions' => $original['suspensions'],
                                    'product_relations' => $original['product_relations'],
                                    'product_also_needs' => $original['product_also_needs'],
                                    'analogues' => $original['analogues'],
                                ];

                                unset($original['categories']);
                                unset($original['targets']);
                                unset($original['images']);
                                unset($original['suspensions']);
                                unset($original['manufacturer_id']);
                                unset($original['product_relations']);
                                unset($original['product_also_needs']);
                                unset($original['analogues']);

                                //manufacturers
                                if ($manufacturer !== null) {
                                    $manufacturer = $manufacturer->id;
                                }
                                $original['manufacturer_id'] = $manufacturer;

                                if ($product !== null) {
                                    $product->update($original);
                                } else {
                                    try {
                                        $product = Product::create($original);
                                    } catch (\Exception $e) {
                                        $original['slug'] = $original['slug'] . '-2';
                                        $product = Product::create($original);
                                    }
                                }

                                if ($relations['categories'] !== null) {
                                    $categories = explode(';', $relations['categories']);
                                    $catQuery = new Category();
                                    foreach ($categories as $category) {
                                        $catQuery = $catQuery->orWhere('name', '=', trim($category));
                                    }
                                    $categories = $catQuery->get()->pluck('id');
                                    $product->categories()->sync($categories->toArray());
                                }
                                if ($relations['targets'] !== null) {
                                    $product->targets()->sync(explode(';', $relations['targets']));
                                }
                                if ($relations['suspensions'] !== null) {
                                    $product->suspensions()->sync(explode(';', $relations['suspensions']));
                                }
                                if ($relations['product_relations'] !== null) {
                                    $product->product_relations()->sync(explode(';', $relations['product_relations']));
                                }
                                if ($relations['product_also_needs'] !== null) {
                                    $product->product_also_needs()->sync(explode(';', $relations['product_also_needs']));
                                }
                                if ($relations['analogues'] !== null) {
                                    $product->analogues()->sync(explode(';', $relations['analogues']));
                                }

                                $images = [];
                                if ($relations['images'] !== null) {
                                    foreach (explode(';', $relations['images']) as $image) {
                                        $images[]['image'] = $image;
                                    }
                                }
                                $product->images = $images;

                                $product->forceSave();

                            }

                            if (!$products->isEmpty()) {
                                $products->each(function (Product $product) {
                                    $product->forceDelete();
                                });
                            }
                        });
                    });
                } catch (\Exception $e) {
                    \DB::rollBack();
                    dd($e);
                    if ($e instanceof \October\Rain\Database\ModelException || $e instanceof ModelException) {
                        \Flash::error($e->getMessage() . '. Ошбика произошла при обработки записи с id: ' . (($e->getModel()->id === null) ? '(отсутствует)' : ($e->getModel()->id)) . ', название:' . $e->getModel()->name);
                    } else {
                        \Flash::error('Произошла ошибка');
                    }
                    $errors = true;
                }
                if (!$errors)
                    \Flash::success('Данные успешно импортированы');
            } else {
                \Flash::error('Выберите файл формата xls!');
            }
        }
        \October\Rain\Database\Model::reguard();
    }

}