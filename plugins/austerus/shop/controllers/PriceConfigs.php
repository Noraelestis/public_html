<?php namespace Austerus\Shop\Controllers;

use Austerus\Shop\Models\PriceConfig;
use Backend\Classes\Controller;
use BackendMenu;

/**
 * Price Configs Back-end Controller
 */
class PriceConfigs extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
        'Backend.Behaviors.RelationController',
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';
    public $relationConfig = 'config_relation.yaml';

    public function __construct()
    {
        parent::__construct();

        $this->appendDynamicAttributes();
        $this->addJs('/plugins/austerus/shop/assets/sticky.js', '0.0.1');
        BackendMenu::setContext('Austerus.Shop', 'shop', 'priceconfigs');
    }

    /**
     * Add attributes to fields list
     */
    private function appendDynamicAttributes()
    {
        \Event::listen('system.extendConfigFile', function (string $publicFile, array $config): array {
            $route = \explode('/', request('slug'));
            $id = \end($route);
            if ($publicFile === '/plugins/austerus/shop/models/priceconfig/fields.yaml') {
                $priceConfig = PriceConfig::where('is_default', '=', true)->first();
                if ($priceConfig === null) {
                    return $config;
                }
                if ($id === null || (int)$id !== $priceConfig->id) {
                    unset($config['fields']['is_default']);
                }
            }
            return $config;
        });
    }
}
