<?php namespace Austerus\Shop\Controllers;

use Austerus\Shop\Models\Product;
use Austerus\Shop\Traits\FastAccessProducts;
use Backend\Classes\Controller;
use BackendMenu;
use Flash;
use Lang;

/**
 * Products Back-end Controller
 */
class Products extends Controller
{
    use FastAccessProducts;

    private $cashNext = null;
    private $cashPrev = null;

    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
        'Austerus.Shop.Behaviors.Copy.Behavior',
        'Austerus.Shop.Behaviors.Delete.Behavior',
        'Austerus.Shop.Behaviors.Quick.Behavior'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public $attachMany = ['attachments' => ['System\Models\File']];

    public function __construct()
    {
        parent::__construct();

        $this->addJs('/plugins/austerus/shop/assets/sticky.js', '0.0.1');
        BackendMenu::setContext('Austerus.Shop', 'shop', 'products');
        $this->appendDynamicAttributes();
    }

    public function listExtendQuery($query)
    {
        $query->withTrashed();
    }

    public function formExtendQuery($query)
    {
        $query->withTrashed();
    }

    /**
     * @return bool|integer
     */
    public function getNext()
    {
        if ($this->cashNext === null) {
            $next = Product::where('id', '>', $this->vars['formModel']->id)->first();
            if ($next === null) {
                $this->cashNext = false;
            } else {
                $this->cashNext = $next->id;
            }
        }
        return $this->cashNext;
    }

    /**
     * @return bool|integer
     */
    public function getPrev()
    {
        if ($this->cashPrev === null) {
            $prev = Product::where('id', '<', $this->vars['formModel']->id)->orderBy('id', 'desc')->first();
            if ($prev === null) {
                $this->cashPrev = false;
            } else {
                $this->cashPrev = $prev->id;
            }
        }
        return $this->cashPrev;
    }

    public function onDelete()
    {
        $checkedIds = post('checked');

        if (!$checkedIds || !is_array($checkedIds) || !count($checkedIds)) {
            Flash::error(Lang::get('backend::lang.list.delete_selected_empty'));
            return $this->listRefresh();
        }

        $records = Product::whereIn('id', $checkedIds)->withTrashed()->get();

        if ($records->count()) {
            $records->each(function (Product $product) {
                $product->forceDelete();
            });

            Flash::success(Lang::get('backend::lang.list.delete_selected_success'));
        }
        else {
            Flash::error(Lang::get('backend::lang.list.delete_selected_empty'));
        }

        return $this->listRefresh('list');
    }

    private function appendDynamicAttributes()
    {
        \Event::listen('system.extendConfigFile', function ($publicFile, $config) {
            if (strpos($publicFile, 'fields.yaml')) {
                $product = Product::find($this->params[0]);
                if ($product !== null) {
                    $category = $product->categories()->first();
                    if ($category !== null) {
                        if ($category->name_param_1) {
                            $config['tabs']['fields']['param_1']['label'] = $category->name_param_1;
                        }
                        if ($category->name_param_2) {
                            $config['tabs']['fields']['param_2']['label'] = $category->name_param_2;
                        }
                        if ($category->name_param_3) {
                            $config['tabs']['fields']['param_3']['label'] = $category->name_param_3;
                        }
                        if ($category->name_param_4) {
                            $config['tabs']['fields']['param_4']['label'] = $category->name_param_4;
                        }
                        if ($category->name_param_5) {
                            $config['tabs']['fields']['param_5']['label'] = $category->name_param_5;
                        }
                    }
                }
            }
            return $config;
        });
    }
}