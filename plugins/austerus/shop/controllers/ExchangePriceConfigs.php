<?php namespace Austerus\Shop\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Exchange Back-end Controller
 */
class ExchangePriceConfigs extends Controller
{
    public $implement = [
        'Backend.Behaviors.ImportExportController',
    ];

    public $importExportConfig = 'config_import_export.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('Austerus.Shop', 'shop', 'priceconfigs');
    }
}
