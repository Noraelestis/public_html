<?php namespace Austerus\Shop\Controllers;

use Austerus\Shop\Models\Category;
use Austerus\Shop\Models\Product as Goods;
use BackendMenu;
use Backend\Classes\Controller;
use Austerus\Shop\Traits\FastAccessProducts;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Lang;
use October\Rain\Support\Facades\Flash;

/**
 * Home Page Col Back-end Controller
 */
class HomePageCol extends Controller
{
    use FastAccessProducts;

    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public function __construct()
    {
        parent::__construct();

        $this->addJs('/plugins/austerus/shop/assets/sticky.js', '0.0.1');
        BackendMenu::setContext('Austerus.Shop', 'shop', 'homepagecol');
    }
}