<?php namespace Austerus\Shop\Behaviors\Quick;

use Austerus\Shop\Behaviors\Copy\Behavior as FormController;

class Behavior extends FormController {

    public function quick($recordId = null, $context = null)
    {
        try {
            $this->context = strlen($context) ? $context : $this->getConfig('update[context]', self::CONTEXT_UPDATE);
            $this->controller->pageTitle = $this->controller->pageTitle ?: $this->getLang(
                'quick[title]',
                'backend::lang.form.update_title'
            );
            $this->config->form = "$/austerus/shop/models/product/fields_quick.yaml";
            $model = $this->controller->formFindModelObject($recordId);
            $this->initForm($model);
        }
        catch (\Exception $ex) {
            $this->controller->handleError($ex);
        }
    }

    public function quick_onSave($recordId = null, $context = null)
    {
        return $this->update_onSave($recordId, $context);
    }

    public function quick_onCopy($recordId = null, $context = null)
    {
        return $this->update_onCopy($recordId, $context);
    }
}
