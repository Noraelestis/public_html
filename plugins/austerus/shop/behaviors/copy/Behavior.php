<?php namespace Austerus\Shop\Behaviors\Copy;

use Backend;
use Backend\Behaviors\FormController;

class Behavior extends FormController
{

    public function copy($recordId = null, $context = null)
    {
        try {
            $this->context = strlen($context) ? $context : $this->getConfig('create[context]', self::CONTEXT_CREATE);
            $this->controller->pageTitle = $this->controller->pageTitle ?: $this->getLang(
                'copy[title]',
                'backend::lang.form.create_title'
            );

            $model = $this->controller->formFindModelObject($recordId);
            $model = $this->controller->formExtendModel($model) ?: $model;

            $this->initForm($model);
        } catch (Exception $ex) {
            $this->controller->handleError($ex);
        }
    }

    public function copy_onSave($context = null)
    {
        return $this->create_onSave($context);
    }

    public function update_onCopy($recordId = null, $context = null)
    {
        $this->update_onSave($recordId, $context);
        return Backend::redirect('austerus/shop/products/copy/' . $recordId);
    }
}
