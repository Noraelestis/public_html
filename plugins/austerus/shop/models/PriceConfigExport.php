<?php namespace Austerus\Shop\Models;

use Austerus\GeoApi\Models\City;

/**
 * ExchangeExport Model
 */
class PriceConfigExport extends \Backend\Models\ExportModel
{
    public function exportData($columns, $sessionKey = null): array
    {
        $configs = PriceConfig::all()->each(function (PriceConfig $config) use ($columns) {
            $config->addVisible($columns);
        });
        $cities = City::whereNotIn('id', $configs->map(function (PriceConfig $config) {
            return $config->cities->map(function (City $city) {
                return $city->id;
            });
        })->collapse()->unique()->toArray());
        $cities->each(function (City $city) use ($configs, $columns) {
            $configs->push(\array_merge([
                'dns' => $city->domain,
            ]), $columns);
        });
        return $configs->toArray();
    }
}
