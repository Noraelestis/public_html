<?php namespace Austerus\Shop\Models;

use Model;
use October\Rain\Database\Traits\Validation;
use \October\Rain\Database\Traits\Sluggable;

/**
 * ProductTarget Model
 */
class ProductTarget extends Model
{
    use Validation, Sluggable;

    /**
     * @var array The rules to be applied to the data.
     */
    public $rules = [
        'name' => 'required|max:255'
    ];

    /**
     * @var array Generate slugs for these attributes.
     */
    protected $slugs = ['slug' => 'name'];

    /**
     * @var array The array of custom attribute names.
     *
     * @var array
     */
    public $attributeNames = [
        'name' => 'Название категории',
        'prepositional' => 'Предложный падеж',
        'sort_order' => 'Порядок сортировки'
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'austerus_shop_product_targets';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    /**
     * @param $value
     * @return mixed
     */
    public function getPrepositionalAttribute($value){
        if(empty($value)){
            return $this->attributes['name'];
        }
        return $value;
    }

    /**
     * If title is empty, then return name
     *
     * @param $value
     * @return mixed|string
     */
    public function getTitleAttribute($value){
        if(empty($value)){
            return $this->name;
        }
        return $value;
    }
}