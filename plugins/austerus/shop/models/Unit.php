<?php namespace Austerus\Shop\Models;

use Model;
use October\Rain\Database\Traits\Sluggable;
use October\Rain\Halcyon\Traits\Validation;

/**
 * Unit Model
 */
class Unit extends Model
{
    use Validation, Sluggable;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'austerus_shop_units';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Generate slugs for these attributes.
     */
    protected $slugs = ['slug' => 'full_name'];

    /**
     * @var array The rules to be applied to the data.
     */
    public $rules = [
        'name' => 'required|max:255',
        'full_name' => 'required|max:255'
    ];

    /**
     * @var array The array of custom attribute names.
     *
     * @var array
     */
    public $attributeNames = [
        'name' => 'Сокращённое название',
        'full_name' => 'Полное название'
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [
        'products' => Product::class
    ];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
