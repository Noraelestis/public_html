<?php namespace Austerus\Shop\Models;

use Model;
use October\Rain\Database\Traits\Sluggable;
use October\Rain\Database\Traits\Validation;

/**
 * Manufacturer Model
 */
class Manufacturer extends Model
{
    use Validation, Sluggable;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'austerus_shop_manufacturers';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = ['name', 'slug', 'image'];

    /**
     * @var array Generate slugs for these attributes.
     */
    protected $slugs = ['slug' => 'name'];

    /**
     * @var array The rules to be applied to the data.
     */
    public $rules = [
        'name' => 'required|max:255',
        'slug' => 'required|max:255'
    ];

    /**
     * @var array The array of custom attribute names.
     *
     * @var array
     */
    public $attributeNames = [
        'name' => 'Название производителя',
        'image' => 'Изображение',
        'slug' => 'Псевдоним'
    ];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [
        'products' => Product::class,
    ];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    /**
     * @param $query
     * @param $slug
     * @return mixed
     */
    public static function scopeForSlug($query, $slug)
    {
        return $query->whereSlug($slug);
    }

    /**
     * @param $slug
     * @return mixed
     */
    public static function getForSlug($slug)
    {
        return static::forSlug($slug)->first();
    }

}