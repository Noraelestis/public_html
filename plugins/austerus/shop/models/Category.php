<?php namespace Austerus\Shop\Models;

use Illuminate\Database\Eloquent\Builder;
use Model;
use October\Rain\Database\Traits\NestedTree;
use October\Rain\Database\Traits\Sortable;
use October\Rain\Database\Traits\Validation;

/**
 * Category Model
 */
class Category extends Model
{
    use NestedTree, Sortable, Validation;

    const SORT_ORDER = 'sort_order';

    /**
     * @var array The rules to be applied to the data.
     */
    public $rules = [
        'name' => 'required|max:255',
        'slug' => 'required|unique:austerus_shop_categories|unique:austerus_shop_shop_routers'
    ];

    /**
     * @var array The array of custom attribute names.
     *
     * @var array
     */
    public $attributeNames = [
        'name' => 'Название категории'
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'austerus_shop_categories';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [
        'children' => [Category::class, 'parent_id']
    ];
    public $belongsTo = [
        'parent' => [Category::class, 'parent_id']
    ];
    public $belongsToMany = [
        'products' => [Product::class, 'table' => 'austerus_shop_category_product', 'key' => 'category_id', 'otherKey' => 'product_id']
    ];
    public $morphTo = [];
    public $morphOne = [
        'route' => [ShopRouter::class, 'name' => 'routeable']
    ];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    /**
     *
     * The "booting" method of the model.
     *
     * @return void
     */
    public static function boot()
    {

        parent::boot();

        static::saved(function ($model) {
            if ($model->route === null) {
                $model->route()->create(['slug' => $model->slug]);
                $model->setRelation('route', $model->route()->first());
            } else {
                $model->route->slug = $model->slug;
                $model->route->save();
            }
        });

        static::deleted(function ($model) {
            if ($model->route !== null) {
                $model->route->delete();
            }
        });
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public static function scopeGetBuilder(Builder $query)
    {
        return $query;
    }

    /**
     * Dynamically rules method
     */
    public function beforeValidate()
    {
        if ($this->exists) {
            $this->rules['slug'] = explode('|', $this->rules['slug']);
            $this->rules['slug'][1] = $this->rules['slug'][1] . ',slug,' . $this->id;
            $this->rules['slug'][2] = $this->rules['slug'][2] . ',slug,' . $this->route->id;
            $this->rules['slug'] = implode('|', $this->rules['slug']);
        }
    }

    /**
     * @param $slug
     * @return mixed
     */
    public static function getForSlug($slug)
    {
        return static::where('slug', $slug)->first();
    }

    /**
     * @param $query
     * @return mixed
     */
    public static function scopePrimaryOnly($query)
    {
        return $query->where('primary', '=', true);
    }

    /**
     * @param $query
     * @return mixed
     */
    public static function scopeOrderAsc($query)
    {
        return $query->orderBy('sort_order', 'asc');
    }

    /**
     * Setter slug
     *
     * @param $value
     */
    public function setSlugAttribute($value)
    {
        if (empty($value)) {
            $value = str_slug($this->attributes['seo_h1']);
        }
        $this->attributes['slug'] = str_slug($value);
    }

    /**
     * @param $value
     * @return mixed
     */
    public function getFullNameAttribute($value)
    {
        if ($value === "") {
            return $this->name;
        }
        return $value;
    }

    public function getSeoH1Attribute($value)
    {
        if (empty($value)) {
            return $this->name;
        }
        return $value;
    }
}