<?php

namespace Austerus\Shop\Models;

use Austerus\GeoApi\Models\City;
use October\Rain\Database\Collection;

/**
 * ExchangeImport Model
 */
class PriceConfigImport extends \Backend\Models\ImportModel
{
    /**
     * @var array The rules to be applied to the data.
     */
    public $rules = [
        'dns' => 'required',
        'name' => 'required',
    ];

    /**
     * @var Collection
     */
    private $cities;
    /**
     * @var Collection
     */
    private $configs;

    private $currentRow = 0;

    public function __construct()
    {
        $this->cities = City::all()->keyBy('domain');
        $this->configs = PriceConfig::all()->keyBy('name');
    }

    protected $categories;

    public function importData($results, $sessionKey = null)
    {
        try {
            foreach ($results as $key => $result) {
                $this->currentRow = $key;
                /**
                 * @var PriceConfig $config
                 */
                $config = $this->configs->get($result['name']);
                if (!$config) {
                    if (!$this->isValidRow($result)) {
                        if (!empty($result['name'])) {
                            $deletedCandidate = $this->configs->get($result['name']);
                            if ($deletedCandidate) {
                                $deletedCandidate->delete();
                                $this->logWarning($this->currentRow, "{$result['name']} - был удалён");
                            }
                        }
                        continue;
                    }
                    $config = new PriceConfig();
                    if (empty($result['name'])) {
                        $result['name'] = $result['dns'];
                        $this->logWarning($this->currentRow, "Отсутствует название, было сгенерировано автоматически - {$result['name']}");
                    }
                    $config->name = $result['name'];
                    $this->logCreated();
                } else {
                    $this->configs->forget($result['name']);
                    $this->logUpdated();
                }
                $this->insertPrice($config, $result);
            }
            $this->configs->each(function (PriceConfig $config) {
                $config->delete();
            });
        } catch (\Exception $e) {
            $this->logError($this->currentRow, $e->getMessage());
        }
    }

    private function insertPrice(PriceConfig $config, array $result)
    {
        $config->fill([
            'special_price' => $result['special_price'],
            'wholesale_price' => $result['wholesale_price'],
            'price' => $result['price'],
            'discount_price' => $result['discount_price'],
            'is_default' => $result['is_default'],
        ]);
        $config->save();
        if ($result['dns']) {
            $ids = [];
            foreach (\explode(',', $result['dns']) as $dns) {
                $city = $this->cities->get(\trim($dns));
                if ($city) {
                    $ids[] = $city->id;
                } else {
                    $this->logSkipped($this->currentRow, "Домен {$dns} не найден, и был пропущен");
                }
            }
            if (\count($ids) > 0) {
                $config->cities()->sync($ids);
            }
        }
    }

    private function isValidRow($result): bool
    {
        return !(empty($result['special_price']) && empty($result['wholesale_price']) && empty($result['price']) && empty($result['discount_price']));
    }
}
