<?php namespace Austerus\Shop\Models;

use Model;
use October\Rain\Database\Builder;
use October\Rain\Database\Collection;

/**
 * Slider Model
 */
class Slider extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'austerus_shop_sliders';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [
        'products' => [Product::class, 'table' => 'austerus_shop_slider_product'],
        'categories' => [Category::class, 'table' => 'austerus_shop_slider_category'],
    ];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public static function findByCategory(Category $category): Collection
    {
        return static::findByCategorySlug($category->slug);
    }

    public static function findByCategorySlug(string $slug): Collection
    {
        return static::byCategorySlug($slug)->get();
    }

    public static function scopeByCategorySlug(Builder $query, string $slug): Builder
    {
        return $query->whereHas('categories', function (Builder $query) use ($slug) {
            return $query->where('slug', '=', $slug);
        });
    }
}
