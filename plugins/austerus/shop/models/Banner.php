<?php namespace Austerus\Shop\Models;

use Model;

/**
 * Banner Model
 */
class Banner extends Model
{
    /**
     * @var string The database table used by the model.
     */
    public $table = 'austerus_shop_banners';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    /**
     * @param $query
     * @param $url
     * @return mixed
     */
    public static function scopeForUrl($query, $url)
    {
        return $query->whereUrl($url);
    }

    /**
     * @param $url
     * @return mixed
     */
    public static function getForUrl($url)
    {
        return static::forUrl($url)->get();
    }
}
