<?php namespace Austerus\Shop\Models;

use Model;
use October\Rain\Database\Traits\Sluggable;
use October\Rain\Halcyon\Traits\Validation;

/**
 * Suspension Model
 */
class Suspension extends Model
{
    use Validation, Sluggable;

    /**
     * @var array The rules to be applied to the data.
     */
    public $rules = [
        'name' => 'required|max:255'
    ];

    /**
     * @var array Generate slugs for these attributes.
     */
    protected $slugs = ['slug' => 'name'];

    /**
     * @var array The array of custom attribute names.
     *
     * @var array
     */
    public $attributeNames = [
        'name' => 'Название',
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'austerus_shop_suspensions';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [
        'products' => ['Austerus\Shop\Models\Product', 'table' => 'austerus_shop_product_suspension', 'key' => 'suspension_id', 'otherKey' => 'product_id']
    ];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];
}
