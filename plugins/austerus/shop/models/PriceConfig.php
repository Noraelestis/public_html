<?php namespace Austerus\Shop\Models;

use Austerus\GeoApi\Models\City;
use Model;

/**
 * PriceConfig Model
 */
class PriceConfig extends Model
{
    public $timestamps = false;
    /**
     * @var string The database table used by the model.
     */
    public $table = 'austerus_shop_price_config';

    /**
     * @var array Guarded fields
     */
    protected $guarded = [];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    protected $appends = ['dns'];


    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [
        'cities' => [City::class, 'table' => 'austerus_shop_city_price_config', 'key' => 'price_config_id', 'otherKey' => 'city_id'],
    ];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    public function getDnsAttribute(): string
    {
        if (\method_exists($this->cities, 'count') && $this->cities->count() > 0) {
            return $this->cities->map(function(City $city) {
                return $city->domain;
            })->implode(',');
        }
        return '';
    }
}
