<?php namespace Austerus\Shop\Models;

use Austerus\GeoApi\Classes\GeoPresenter;

/**
 * Product Model
 */
class CategoryGeoPresenter extends GeoPresenter
{

    /**
     * @var array
     */
    protected $geoFields = ['title', 'seo_h1', 'introduction', 'description', 'keywords', 'content'];
}