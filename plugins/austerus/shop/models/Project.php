<?php namespace Austerus\Shop\Models;

use Model;
use October\Rain\Database\Traits\Sluggable;
use October\Rain\Database\Traits\Validation;

/**
 * Project Model
 */
class Project extends Model
{
    use Validation, Sluggable;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'austerus_shop_projects';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Generate slugs for these attributes.
     */
    protected $slugs = ['slug' => 'title'];

    /**
     * @var array The rules to be applied to the data.
     */
    public $rules = [
        'slug' => 'unique:austerus_shop_projects'
    ];

    /**
     * Dynamically rules method
     */
    public function beforeValidate()
    {
        if ($this->id !== null) {
            $this->rules['slug'] = explode('|', $this->rules['slug']);
            $this->rules['slug'][1] = $this->rules['slug'][1] . ',slug,' . $this->id;
            $this->rules['slug'] = implode('|', $this->rules['slug']);
        }
    }

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [
        'products' => [Product::class, 'table' => 'austerus_shop_products_projects', 'key' => 'project_id', 'otherKey' => 'product_id'],
    ];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    /**
     *
     * @param $value
     * @return mixed
     */
    public function getImagesAttribute($value)
    {
        return (array)json_decode($value);
    }

    /**
     * @param $value
     */
    public function setImagesAttribute($value)
    {
        $this->attributes['images'] = json_encode(array_values($value));
    }
}
