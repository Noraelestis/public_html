<?php
namespace Austerus\Shop\Models;

use Model;

/**
 * Settings Model
 */
class Settings extends Model
{
    public $implement = ['System.Behaviors.SettingsModel'];

    public $settingsCode = 'shop_settings';

    public $settingsFields = 'fields.yaml';
}