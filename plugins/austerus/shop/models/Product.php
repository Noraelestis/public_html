<?php namespace Austerus\Shop\Models;

use Austerus\Shop\Classes\Facades\Geo\GeoPrice;
use Carbon\Carbon;
use October\Rain\Database\Builder;
use October\Rain\Database\Model;
use October\Rain\Database\Traits\SoftDelete;
use October\Rain\Database\Traits\Validation;
use function Composer\Autoload\includeFile;

/**
 * Product Model
 */
class Product extends Model
{
    use Validation, SoftDelete;

    /**
     * @var array The rules to be applied to the data.
     */
    public $rules = [
        'name' => 'required|max:255',
        'product_code' => 'nullable|max:100|unique:austerus_shop_products',
        'price' => 'nullable|numeric',
        'product_target_id' => 'nullable|numeric',
        'power' => 'nullable|numeric',
        'power_consumer' => 'nullable|numeric',
        'light_flow' => 'nullable|numeric',
        'slug' => 'required|unique:austerus_shop_products|unique:austerus_shop_shop_routers'
    ];

    /**
     * @var array The array of custom attribute names.
     *
     * @var array
     */
    public $attributeNames = [
        'name' => 'Название товара',
        'product_code' => 'Код товара'
    ];

    /**
     * Dynamically rules method
     */
    public function beforeValidate()
    {
        if ($this->exists) {
            $this->rules['product_code'] = $this->rules['product_code'] . ',product_code,' . $this->id;
            $this->rules['slug'] = explode('|', $this->rules['slug']);
            $this->rules['slug'][1] = $this->rules['slug'][1] . ',slug,' . $this->id;
            if ($this->route === null) {
                static::createRoute($this);
            }
            $this->rules['slug'][2] = $this->rules['slug'][2] . ',slug,' . $this->route->id;
            $this->rules['slug'] = implode('|', $this->rules['slug']);
        }
    }

    /**
     * @var string The database table used by the model.
     */
    public $table = 'austerus_shop_products';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
//    protected $fillable = [
//        'id',
//        'name',
//        'product_code',
//        'article_provider',
//        'full_name',
//        'title',
//        'description',
//        'keywords',
//        'content',
//        'slug',
//        'price',
//        'discount_price',
//        'discount_from',
//        'special_price',
//        'wholesale_price',
//        'product_target_id',
//        'power',
//        'power_consumer',
//        'light_flow',
//        'protection',
//        'colorful_temperature',
//        'voltage_from',
//        'voltage_to',
//        'height_suspension',
//        'life_time',
//        'guarantee',
//        'weight',
//        'length',
//        'width',
//        'height',
//        'sort',
//        'pdf_description',
//        'replacing_lamp',
//        'relation_name',
//        'analogue_alias',
//        'popular',
//        'active',
//        'custom_text',
//        'temperature_range',
//        'custom_title_content',
//        'custom_title_additional_options',
//        'additional_options',
//        'custom_title_benefits_led',
//        'benefits_led',
//        'custom_title_specifications',
//        'custom_title_compliance',
//        'compliance',
//        'is_novelty',
//        'is_hit',
//        'is_stock',
//        'manufacturer_id',
//        'unit_id'
//    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = ['rubric', 'active'];

    /**
     * Default image for product if not product image
     *
     * @var string
     */
    protected $defaultImage = 'images/product_empy_img.jpg';

    protected $jsonable = ['images'];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [
        'manufacturer' => Manufacturer::class,
        'unit' => Unit::class
    ];
    public $belongsToMany = [
        'categories' => [Category::class, 'table' => 'austerus_shop_category_product', 'key' => 'product_id', 'otherKey' => 'category_id'],
        'targets' => ProductTarget::class,
        'product_relations' => [Product::class, 'table' => 'austerus_product_relation', 'key' => 'product_id', 'otherKey' => 'relation_id'],
        'product_also_needs' => [Product::class, 'table' => 'austerus_product_also_need', 'key' => 'product_id', 'otherKey' => 'also_need_id'],
        'suspensions' => [Suspension::class, 'table' => 'austerus_shop_product_suspension', 'key' => 'product_id', 'otherKey' => 'suspension_id'],
        'analogues' => [Product::class, 'table' => 'austerus_shop_product_analog', 'key' => 'product_id', 'otherKey' => 'analog_id'],
    ];
    public $morphTo = [];
    public $morphOne = [
        'route' => [ShopRouter::class, 'name' => 'routeable']
    ];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    /**
     *
     * The "booting" method of the model.
     *
     * @return void
     */
    public static function boot()
    {

        parent::boot();

        static::saved(function ($model) {
            if ($model->route === null) {
                $model->route()->create(['slug' => $model->slug]);
                $model->setRelation('route', $model->route()->first());
            } else {
                $model->route->slug = $model->slug;
                $model->route->save();
            }
        });

//        static::saving(function ($model) {
//            preg_match_all('/\[key\]([^\[]+)\[\/key\]/is', $model->content, $result);
//            \Log::warning($result[1]);
//            if (isset($result[1])) {
//                $model->keywords = implode(', ', $result[1]);
//            }
//        });

        static::deleted(function ($model) {
            if ($model->route !== null) {
                $model->route->delete();
            }
        });
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    public static function scopeGetBuilder(Builder $query)
    {
        return $query;
    }

    /**
     * @return array
     */
    public static function listRelationsNames()
    {
        return ['с этим товаром покупают', 'похожие товары', 'хит продаж', 'модельный ряд'];
    }

    public static function createRoute(Product &$model)
    {
        $route = $model->route()->create(['slug' => $model->slug]);
        $model->setRelation('route', $route);
    }

    /**
     * @param Builder $query
     * @return mixed
     */
    public function scopeSort(Builder $query)
    {
        return $query->orderByRaw('ISNULL(price) ASC, price ASC');
    }

    /**
     * @param $slug
     * @return mixed
     */
    public static function getForSlug($slug)
    {
        return static::where('slug', $slug)->with('categories', 'targets')->first();
    }

    /**
     * @param $code
     * @return mixed
     */
    public static function getForCode($code)
    {
        return static::where('product_code', $code)->with('categories', 'targets')->first();
    }

    /**
     * @param array $ids
     * @return mixed
     */
    public static function getBenefits(array $ids)
    {
        $builder = static::join('category_product', 'category_product.product_id', '=', 'austerus_shop_products.id')
            ->join('austerus_shop_categories', 'category_product.category_id', '=', 'austerus_shop_categories.id');
        foreach ($ids as $id) {
            $builder->addSelect(\DB::raw("count(CASE WHEN category_id={$id} THEN true END) as total_category_{$id}"));
        }
        return $builder;
    }

    /**
     * @param int $limit
     * @return Builder
     */
    public static function getPopular($limit = 9)
    {
        return static::where('popular', '=', true)->sort()->limit($limit);
    }

    /**
     * Return data with filter
     * @param $query
     * @param array $data
     * @return mixed
     */
    public static function scopeFilter($query, array $data)
    {
        if (array_key_exists('filter_price_from', $data) && is_numeric($data['filter_price_from']) && is_array($data['filter_price_from'])) {
            $query = $query->where('price', '>=', (int)$data['filter_price_from']);
        }
        if (array_key_exists('filter_price_to', $data) && is_numeric($data['filter_price_to']) && is_array($data['filter_price_to'])) {
            $query = $query->where('price', '<=', (int)$data['filter_price_to']);
        }
        if (array_key_exists('param_1', $data) && is_array($data['param_1'])) {
            $query = $query->whereIn('param_1', $data['param_1']);
        }
        if (array_key_exists('param_2', $data) && is_array($data['param_2'])) {
            $query = $query->whereIn('param_2', $data['param_2']);
        }
        if (array_key_exists('param_3', $data) && is_array($data['param_3'])) {
            $query = $query->whereIn('param_3', $data['param_3']);
        }
        if (array_key_exists('param_4', $data) && is_array($data['param_4'])) {
            $query = $query->whereIn('param_4', $data['param_4']);
        }
        if (array_key_exists('param_5', $data) && is_array($data['param_5'])) {
            $query = $query->whereIn('param_5', $data['param_5']);
        }

        return $query;
    }

    /**
     * Return current mark for this product
     *
     * @return bool|string
     */
    public function getMarkAttribute()
    {
        if ($this->is_novelty) {
            return 'novelty';
        } elseif ($this->is_hit) {
            return 'hit';
        } elseif ($this->is_stock) {
            return 'stock';
        } elseif ($this->is_sale) {
            return 'sale';
        } elseif ($this->is_popular) {
            return 'popular';
        } elseif ($this->is_special) {
            return 'special';
        }
        return false;
    }

    /**
     * Return first image or default image
     *
     * @return string
     */
    public function getImageAttribute()
    {
        $images = $this->images;
        if (isset($images[0]['image'])) {
            return (object)$images[0];
        }
        return (object)['image' => $this->defaultImage, 'alt' => ''];
    }

    /**
     * Setter slug
     *
     * @param $value
     */
    public function setSlugAttribute($value)
    {
        if (empty($value)) {
            $value = str_slug($this->attributes['name']);
        }
        $this->attributes['slug'] = str_slug($value);
    }

    /**
     * @return bool
     */
    public function getActiveAttribute()
    {
        return ($this->deleted_at === null);
    }

    /**
     * @param $value
     */
    public function setActiveAttribute($value)
    {
        if (!$value) {
            $this->active = false;
            $this->deleted_at = Carbon::now();
        } else {
            $this->active = false;
            $this->deleted_at = null;
        }
    }

    /**
     * If title is empty, then return name
     *
     * @param $value
     * @return mixed|string
     */
    public function getTitleAttribute($value)
    {
        if (empty($value)) {
            return $this->name;
        }
        return $value;
    }

    /**
     * @param $value
     * @return string
     */
    public function getRubricAttribute($value)
    {
        if ($this->relation_name !== null) {
            return $this->listRelationsNames()[$this->relation_name];
        }
        return "";
    }

    /**
     * @param $value
     */
    public function setRubricAttribute($value)
    {
        if ($value !== "") {
            if (isset($this->listRelationsNames()[$value])) {
                $this->relation_name = $this->listRelationsNames()[$value];
            }
        }
    }

    /**
     * @return Category
     */
    public function getPrimaryCategoryAttribute()
    {
        if ($this->relationLoaded('categories')) {
            return $this->getRelation('categories')->first();
        }
        return $this->categories()->first();
    }

    /**
     * @param $value
     * @return string
     */
    public function getCustomTitleContentAttribute($value)
    {
        if (empty($value)) {
            return 'Описание и сфера применения';
        }
        return $value;
    }

    /**
     * @param $value
     * @return string
     */
    public function getCustomTitleAdditionalOptionsAttribute($value)
    {
        if (empty($value)) {
            return 'Дополнительные опции';
        }
        return $value;
    }

    /**
     * @param $value
     * @return string
     */
    public function getCustomTitleBenefitsLedAttribute($value)
    {
        if (empty($value)) {
            return 'Характеристики';
        }
        return $value;
    }

    /**
     * @param $value
     * @return string
     */
    public function getCustomTitleSpecificationsAttribute($value)
    {
        if (empty($value)) {
            return 'Технические характеристики';
        }
        return $value;
    }

    /**
     * @param $value
     * @return string
     */
    public function getCustomTitleComplianceAttribute($value)
    {
        if (empty($value)) {
            return 'Соответсвтие нормам';
        }
        return $value;
    }

    public function getAnalogueAliasAttribute($value)
    {
        if (empty($value)) {
            return 'Аналоги товаров';
        }
        return $value;
    }

    /**
     * @param $value
     */
    public function setProductCodeAttribute($value)
    {
        if (!empty($value)) {
            $this->attributes['product_code'] = $value;
        }
    }

    public function getMinimalPriceAttribute()
    {
        if (!empty($this->special_price)) {
            return $this->special_price;
        } elseif (!empty($this->wholesale_price)) {
            return $this->wholesale_price;
        }
        return $this->price;
    }

    public function getWholesalePriceAttribute($value)
    {
        return $this->getGeoPrice('wholesale_price', $value);
    }

    public function getPriceAttribute($value)
    {
        return $this->getGeoPrice('price', $value);
    }

    public function getSpecialPriceAttribute($value)
    {
        return $this->getGeoPrice('special_price', $value);
    }

    private function getGeoPrice(string $field, ?int $default): ?int
    {
        if ((int)$this->purchase_price !== 0) {
            $config = GeoPrice::getPriceConfig();
            if ($config !== null && $config->$field) {
                return $this->purchase_price + ($this->purchase_price * $config->$field / 100);
            }
        }
        return $default;
    }

    public function fieldsOut(): array
    {
        $result = [];
        $category = $this->categories->first();
        if ($category !== null) {
            for ($i=1; $i<=5;$i++){
                if ($category["is_param_{$i}"] && $this["param_{$i}"] !== '') {
                    $result[] = ['name' => $category["name_param_{$i}"], 'value' => $this["param_{$i}"]];
                }
            }

        }
        return $result;
    }
}