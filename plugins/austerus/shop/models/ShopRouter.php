<?php namespace Austerus\Shop\Models;

use Model;

/**
 * ShopRouter Model
 */
class ShopRouter extends Model
{

    /**
     * @var string The database table used by the model.
     */
    public $table = 'austerus_shop_shop_routers';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = ['slug'];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $belongsTo = [];
    public $belongsToMany = [];
    public $morphTo = [
        'routeable' => []
    ];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

    /**
     * @param $query
     * @param $slug
     * @return mixed
     */
    public static function scopeForSlug($query, $slug)
    {
        return $query->whereSlug($slug);
    }

    /**
     * @param $slug
     * @return mixed
     */
    public static function getForSlug($slug)
    {
        return static::forSlug($slug)->first();
    }

}