<?php namespace Austerus\Shop\Models;

use October\Rain\Database\Model;
use October\Rain\Database\Traits\Sortable;

/**
 * HomePageCol Model
 */
class HomePageCol extends Model
{

    use Sortable;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'austerus_shop_home_page_cols';

    /**
     * @var array Guarded fields
     */
    protected $guarded = ['*'];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    /**
     * @var array Relations
     */
    public $hasOne = [];
    public $hasMany = [];
    public $morphTo = [];
    public $morphOne = [];
    public $morphMany = [];
    public $attachOne = [];
    public $attachMany = [];

}