<?php

namespace Austerus\Shop\Classes\Facades\Geo;

use Austerus\GeoApi\Classes\GeoApiFacade;
use Austerus\Shop\Models\PriceConfig;
use October\Rain\Database\Builder;

class GeoPrice
{
    protected static $priceConfig;

    public static function getPriceConfig(): ?PriceConfig
    {
        //todo optimize it
        if (static::$priceConfig === null) {
            $city = GeoApiFacade::getCity();
            if ($city === null) {
                static::$priceConfig = PriceConfig::where('is_default', '=', true)->first();
            } else {
                static::$priceConfig = PriceConfig::whereHas('cities', function(Builder $query) use ($city) {
                    $query->whereId($city->id);
                })->first();
                if (!static::$priceConfig) {
                    static::$priceConfig = PriceConfig::where('is_default', '=', true)->first();
                }
            }
        }
        return static::$priceConfig;
    }
}