<?php

namespace Austerus\Shop\Classes\Presenters;

class ProductCartPresenter extends Presenter
{
    /**
     * @var int
     */
    protected $count = 1;

    /**
     * @return int
     */
    public function getCount(): int
    {
        return $this->count;
    }

    /**
     * @param int $count
     * @return ProductCartPresenter
     */
    public function setCount(int $count): ProductCartPresenter
    {
        $this->count = $count;

        return $this;
    }

    public function getSumPrice(): int
    {
        if ($this->id > 0) {
            return $this->model->minimalPrice * $this->count;
        } else {
            return $this->model->price * $this->count;
        }
    }

    public function toString(): string
    {
        return json_encode(['product' => $this->model->toJson(), 'count' => $this->count]);
    }
}