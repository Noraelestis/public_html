<?php namespace Austerus\Shop\Classes\Presenters;

use Austerus\Shop\Models\Product;
use Twig_Environment;
use Twig_Loader_Array;

/**
 * Product Model
 */
class ProductVariablesPresenter extends Presenter
{

    /**
     * Fields that are available as api
     *
     * @var array
     */
    protected $fields = [
        'name',
        'product_code',
        'price',
        'discount_price',
        'special_price',
        'wholesale_price',
        'minimal_price',
        'light_flow',
        'manufacturer.name',
        'power'
    ];

    /**
     * Fields in which variables are available
     *
     * @var array
     */
    protected $processingFields = [
        'title',
        'content',
        'replacing_lamp',
        'additional_options',
        'benefits_led',
        'compliance',
        'description',
        'keywords'
    ];

    /**
     * Additional variables and values for rendering
     *
     * @var array
     */
    protected $appends = [];

    /**
     * ProductVariablesPresenter constructor.
     * @param Product $model
     * @param array $appends
     */
    public function __construct(Product $model, array $appends = [])
    {
        parent::__construct($model);
        $this->appends = array_merge($this->appends, $appends);
        $this->model = $this->processing();
    }

    /**
     * Render variables
     *
     * @return mixed
     */
    public function processing(): Product
    {
        $twig = new Twig_Environment(new Twig_Loader_Array(), ['cache' => false]);
        $values = array_merge($this->appends, $this->prepareFields());
        try {
            foreach ($this->processingFields as $field) {
                $template = $twig->createTemplate($this->model->$field);
                $this->model->$field = $template->render($values);
            }
            $this->model->content = strtr($this->model->content, ['[key]' => '', '[/key]' => '']);
        } catch (\Exception $e) {
            \Log::warning($e);
        }
        return $this->model;
    }

    /**
     * Return array product values
     *
     * @return array
     */
    protected function prepareFields(): array
    {
        $variables = ['product' => []];
        foreach ($this->fields as $field) {
            $fields = explode('.', $field);
            if (isset($fields[1])) {
                $relationName = array_shift($fields);
                $relation = $this->model->$relationName;
                foreach ($fields as $item) {
                    if ($relation !== null && isset($relation->$item)) {
                        $relation = $relation->$item;
                    }
                }
                $variables['product'][$relationName] = $relation;
            } else {
                $field = $fields[0];
                $variables['product'][$field] = $this->model->$field;
            }
        }
        return $variables;
    }
}