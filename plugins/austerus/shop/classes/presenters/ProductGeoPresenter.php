<?php namespace Austerus\Shop\Classes\Presenters;

use Austerus\GeoApi\Classes\GeoPresenter;

/**
 * Product Model
 */
class ProductGeoPresenter extends GeoPresenter
{

    /**
     * @var array
     */
    protected $geoFields = ['title', 'description', 'keywords', 'content'];
}