<?php

namespace Austerus\Shop\Classes\Cart;

use Austerus\Shop\Classes\Presenters\ProductCartPresenter;
use Austerus\Shop\Models\Product;
use Cookie;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Log;
use Session;

class Cart
{
    /**
     * @var Collection
     */
    protected $products;

    public function __construct()
    {
        $this->products = $this->setProducts();
        //dd($this->products);
    }

    public function addProduct(int $id, int $count = 1)
    {
        $product = $this->products->get($id);
        if ($product !== null) {
            $this->plusCount($id);
            return $product;
        } else {
            $product = Product::find(abs($id));
            if ($product) {
                if ($id < 0) {
                    $product->id = -$product->id;
                }
                $product = new ProductCartPresenter($product);
                $product->setCount($count);
                $this->products->put($id, $product);
                $this->updateState();
                return $product;
            }
        }

        return null;
    }

    public function setCount($id, $count)
    {
        $product = $this->products->get($id);
        if ($product) {
            $product->setCount($count);
            $this->products->put($product->id, $product);
            $this->updateState();
            return $product;
        }
        return null;
    }

    public function plusCount($id)
    {
        $product = $this->products->get($id);
        if ($product) {
            $count = $product->getCount() + 1;
            $product->setCount($count);
            $this->products->put($product->id, $product);
            $this->updateState();
            return $product;
        }
        return null;
    }

    public function minusCount($id)
    {
        $product = $this->products->get($id);
        if ($product) {
            $count = $product->getCount() - 1;
            if ($count > 0) {
                $product->setCount($count);
                $this->products->put($product->id, $product);
                $this->updateState();
                return $product;
            }
        }
        return null;
    }

    public function deleteProduct(int $id)
    {
        $product = $this->products->get($id);
        if ($product) {
            $this->products = $this->products->filter(function (ProductCartPresenter $product) use ($id) {
                return $product->id !== $id;
            });
            $this->updateState();
            return true;
        } else {
            return false;
        }
    }

    public function getCount(): int
    {
        return $this->products->count();
    }

    public function getFullCount(): int
    {
        if ($this->products->count() === 0) {
            return 0;
        }
        return $this->products->reduce(function ($carry, ProductCartPresenter $product) {
            return $carry + ($product->getCount());
        });
    }

    public function getResultPrice(): int
    {
        if ($this->products->count() === 0) {
            return 0;
        }
        return $this->products->reduce(function ($carry, ProductCartPresenter $product) {
            if ($product->id > 0) {
                return $carry + ($product->minimalPrice * $product->getCount());
            } else {
                return $carry + ($product->price * $product->getCount());
            }
        });
    }

    public function getResultDiscount(): int
    {
        if ($this->products->count() === 0) {
            return 0;
        }
        return $this->products->reduce(function ($carry, ProductCartPresenter $product) {
            return $carry + (($product->price - $product->minimalPrice) * $product->getCount());
        });
    }

    /**
     * @return Collection
     */
    public function getProducts(): Collection
    {
        return $this->products;
    }

    public function setProducts()
    {
//        $productsInCart = $this->getState();
//        \Log::warning($productsInCart);
//        return Product::whereIn('id', $productsInCart->keys())->get()->map(function (Product $product) use ($productsInCart) {
//            $product = new ProductCartPresenter($product);
//            $product->setCount($productsInCart->get($product->id)['count'] ?? 1);
//            return $product;
//        })->keyBy('id');
        $productsInCart = $this->getState();
        $keys = $productsInCart->keys();
        $products = [];
        foreach ($keys as $key) {
            $absKey = abs($key);
            $product = Product::findOrFail($absKey);
            if ($key < 0) {
                $product->id = -$product->id;
            }
            $product = new ProductCartPresenter($product);
            $productInCart = $productsInCart->get($key);
            $product->setCount($productInCart['count'] ?? 1);
            $products[$key] = $product;

        }
        return Collection::make($products);
    }

    public function clearCart()
    {
        Session::forget('shopping_cart');
        Cookie::queue('shopping_cart', [], 1);
        Session::forget('shopping_cart_id');
        Cookie::queue('shopping_cart_id', null, 1);
        $this->products = new Collection([]);
    }

    public function getCartId(): int
    {
        if (Session::has('last_cart_id')) {
            $id = Session::get('last_cart_id');
        } else {
            $id = Cookie::get('last_cart_id');
        }
        if ($id) {
            return $id;
        }
        $id = (int)Cache::get('last_cart_id', 10543);
        $id++;
        Cache::forever('last_cart_id', $id);
        Session::put('last_cart_id', $id);
        Cookie::queue('last_cart_id', $id, 10080);
        return $id;
    }

    /**
     * Get cart data
     * @return Collection
     */
    public function getState(): Collection
    {
        if (Session::has('shopping_cart')) {
            $state = Session::get('shopping_cart');
        } else {
            $state = Cookie::get('shopping_cart');
        }
        if ($state === null || empty($state)) {
            $state = new Collection([]);
        }
        return $state;
    }

    protected function updateState()
    {
        $data = $this->products->map(function (ProductCartPresenter $product) {
            return [
                'id' => $product->id,
                'count' => $product->getCount(),
            ];
        });
        Session::put('shopping_cart', $data);
        Cookie::queue('shopping_cart', $data, 10080);
    }
}
