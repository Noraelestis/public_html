<?php

namespace Austerus\Shop\Classes\Cart;

use Austerus\Shop\Models\Product;
use Cookie;
use October\Rain\Database\Collection;
use Illuminate\Support\Collection as DefaultCollection;
use Session;

class Favorites
{
    /**
     * @var Collection
     */
    protected $products;

    public function __construct()
    {
        $this->setProducts();
    }

    public function addProduct(int $id)
    {
        $product = $this->products->get($id);
        if ($product !== null) {
            return $product;
        } else {
            $product = Product::find($id);
            if ($product) {
                $this->products->put($id, $product);
                $this->updateState();
                return $product;
            }
        }

        return null;
    }

    public function deleteProduct(int $id)
    {
        $result = $this->products->forget($id);
        $this->updateState();
        return $result;
    }

    public function getCount(): int
    {
        return $this->products->count();
    }

    /**
     * @return Collection
     */
    public function getProducts(): Collection
    {
        return $this->products;
    }

    public function setProducts()
    {
        $productsIds = $this->getState();
        $this->products = Product::whereIn('id', $productsIds)->get()->keyBy('id');

        return $this;
    }

    public function clearFavorites(): void
    {
        Session::forget('shopping_favorites');
        Cookie::queue('shopping_favorites', [], 1);
        $this->products = new Collection([]);
    }

    /**
     * Get cart data
     * @return DefaultCollection
     */
    public function getState(): DefaultCollection
    {
        if (Session::has('shopping_favorites')) {
            $state = Session::get('shopping_favorites');
        } else {
            $state = Cookie::get('shopping_favorites');
        }
        if ($state === null || empty($state)) {
            $state = new Collection([]);
        }

        return $state;
    }

    protected function updateState(): void
    {
        $ids = $this->products->keyBy('id')->keys();
        Session::put('shopping_favorites', $ids);
        Cookie::queue('shopping_favorites', $ids, 10080);
    }
}
