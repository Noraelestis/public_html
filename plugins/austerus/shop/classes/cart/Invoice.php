<?php

namespace Austerus\Shop\Classes\Cart;

use Austerus\Shop\Classes\Presenters\ProductCartPresenter;
use Austerus\Shop\Components\OrderComponent;
use Austerus\Shop\Models\Order;
use Cms\Classes\Controller;

class Invoice
{
    /**
     * @var OrderComponent
     */
    protected $cart;

    /**
     * @var Order
     */
    protected $order;

    /**
     * @var int
     */
    protected $count = 0;

    /**
     * @var int
     */
    protected $price = 0;

    /**
     * @var int
     */
    protected $discountPrice = 0;

    /**
     * @var array
     */
    protected $products = [];

    /**
     * @var string
     */
    protected $description = '';

    /**
     * @var int
     */
    protected $customerType = 0;

    public function __construct(OrderComponent $cart, Order $order, int $customerType = 0)
    {
        $this->cart = $cart;
        $this->order = $order;
        $this->description = $order->description;
        $this->customerType = $customerType;
    }

    public function addProduct(ProductCartPresenter $product, int $count = 1)
    {
        $this->products[] = ['model' => $product, 'count' => $count];
        $this->addCount($count);
        if ($product->id < 0) {
            $this->addPrice($product->price * $count);
        } else {
            $this->addPrice($product->minimalPrice * $count);
        }
        $this->addDiscountPrice($product->minimalPrice * $count);
    }

    /**
     * @return string
     * @throws \Cms\Classes\CmsException
     */
    public function getDetails(): string
    {
        return $this->render('@details', ['invoice' => $this]);
    }

    /**
     * @return string
     * @throws \Cms\Classes\CmsException
     */
    public function invoice(): string
    {
        return $this->render('@invoice', ['invoice' => $this]);
    }

    /**
     * @return OrderComponent
     */
    public function getCart(): OrderComponent
    {
        return $this->cart;
    }

    /**
     * @return int
     */
    public function getCount(): int
    {
        return $this->count;
    }

    /**
     * @return int
     */
    public function getDiscountPrice(): int
    {
        return $this->discountPrice;
    }

    /**
     * @return Order
     */
    public function getOrder(): Order
    {
        return $this->order;
    }

    /**
     * @return int
     */
    public function getPrice(): int
    {
        return $this->price;
    }

    /**
     * @return array
     */
    public function getProducts(): array
    {
        return $this->products;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @return int
     */
    public function getCustomerType(): int
    {
        return $this->customerType;
    }

    /**
     * @param $name
     * @param array $params
     * @return mixed
     * @throws \Cms\Classes\CmsException
     */
    public function render($name, array $params = [])
    {
        $controller = new Controller();
        $controller->setComponentContext($this->cart);
        return $controller->renderPartial($name, $params, false);
    }

    protected function addCount(int $count)
    {
        $this->count = $this->count + $count;
    }

    protected function addPrice(int $price)
    {
        $this->price = $this->price + $price;
    }

    protected function addDiscountPrice(int $discountPrice)
    {
        $this->discountPrice = $this->discountPrice + $discountPrice;
    }
}