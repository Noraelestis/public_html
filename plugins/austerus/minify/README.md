# Minify Plugin for OctoberCMS 
 
This plugin provides the mechanism to render the page using minified version of HTML. 
Minified HTML is generated using regular expression. 
 
## How Minify help 
 
Minify removes the comments and whitespace which will help to reduce the file size.  
Smaller HTML and file size reduces the page load time and improve the website performance. 
