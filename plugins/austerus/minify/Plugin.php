<?php namespace Austerus\Minify;

use Event;
use System\Classes\PluginBase;
use Austerus\Minify\Classes\Minify;
use Austerus\Minify\Models\Settings;
use System\Classes\SettingsManager;

/**
 * Minify Plugin Information File
 */
class Plugin extends PluginBase
{

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'austerus.minify::lang.plugin.name',
            'description' => 'austerus.minify::lang.plugin.description',
            'author'      => 'Sozonov Alexey (Austerus copy)',
            'icon'        => 'icon-compress'
        ];
    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {
        $settings = Settings::instance();
        if ($settings->is_enabled) {
            define('REPLACEMENT_HASH', 'MINIFYHTML' . md5($_SERVER['REQUEST_TIME']));
            Event::listen('cms.page.postprocess', function ($controller, $url, $page, $dataHolder) {
                $dataHolder->content = Minify::page($controller, $dataHolder->content);
            });
        }
    }

    /**
     * Register the settings.
     *
     * @return array
     */
    public function registerSettings()
    {
        return [

            'settings' => [
                'label'       => 'austerus.minify::lang.settings.menu_label',
                'description' => 'austerus.minify::lang.settings.menu_description',
                'category'    => SettingsManager::CATEGORY_CMS,
                'icon'        => 'icon-compress',
                'class'       => 'Austerus\Minify\Models\Settings',
                'order'       => 500
            ]

        ];
    }

}