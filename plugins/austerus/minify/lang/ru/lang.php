<?php

return [
    'plugin' => [
        'name' => 'Minify',
        'description' => 'Позволяет минимизировать html',
    ],
    'settings' => [
        'menu_label' => 'Minify',
        'menu_description' => 'Позволяет минимизировать html',
        'is_enabled' => 'Включить минимизацию HTML',
    ]
];