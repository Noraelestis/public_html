<?php

return [
    'plugin' => [
        'name' => 'Minify',
        'description' => 'Provides a mechanism to minify HTML.',
    ],
    'settings' => [
        'menu_label' => 'Minify',
        'menu_description' => 'Provides a mechanism to minify HTML.',
        'is_enabled' => 'Minify HTML',
    ]
];