<?php
namespace Austerus\Minify\Classes;

use Response;

class Minify
{
    protected static function minify($buffer) {

        /* Replace <textarea> with placeholder */
        $buffer = preg_replace_callback('/\\s*<textarea(\\b[^>]*?>[\\s\\S]*?<\\/textarea>)\\s*/i', 'self::minifyHtmlCallback', $buffer);

        /* Replace <pre> with placeholder */
        $buffer = preg_replace_callback('/\\s*<pre(\\b[^>]*?>[\\s\\S]*?<\\/pre>)\\s*/i', 'self::minifyHtmlCallback', $buffer);

        /* Replace <iframe> with placeholder */
        $buffer = preg_replace_callback('/\\s*<iframe(\\b[^>]*?>[\\s\\S]*?<\\/iframe>)\\s*/i', 'self::minifyHtmlIframeCallback', $buffer);

        /* Replace <script> with placeholder */
        $buffer = preg_replace_callback('/\\s*<script(\\b[^>]*?>[\\s\\S]*?<\\/script>)\\s*/i', 'self::minifyHtmlScriptCallback', $buffer);

        /* Replace <style> with placeholder */
        $buffer = preg_replace_callback('/\\s*<style(\\b[^>]*?>[\\s\\S]*?<\\/style>)\\s*/i', 'self::minifyHtmlStyleCallback', $buffer);

        /* Remove HTML comment */
        $buffer = preg_replace_callback('/<!--([\\s\\S]*?)-->/', 'self::minifyHtmlHtmlComment', $buffer);

        $search = array(
            '/\>[^\S ]+/s',                 // remove whitespaces after tags, except space
            '/[^\S ]+\</s',                 // remove whitespaces before tags, except space
            '/(\s)+/s',                     // shorten multiple whitespace sequences
            '/\\s+(<\\/?(?:area|base(?:font)?|blockquote|body'
            .'|caption|center|col(?:group)?|dd|dir|dl|dt|fieldset|form'
            .'|frame(?:set)?|h[1-6]|head|hr|html|legend|li|link|map|menu|meta'
            .'|ol|opt(?:group|ion)|p|param|t(?:able|body|head|d|h||r|foot|itle)'
            .'|ul)\\b[^>]*>)/i',            // remove whitespaces around block/undisplayed elements
//            '/^\\s+|\\s+$/m',               // trim each line
        );

        $replace = array(
            '>',        // remove whitespaces after tags, except space
            '<',        // remove whitespaces before tags, except space
            '\\1',      // shorten multiple whitespace sequences
            '$1',       // remove whitespaces around block/undisplayed elements
            '',         // trim each line
        );

        $buffer = preg_replace($search, $replace, $buffer);

        /* Find and replace <textarea>, <pre>, <iframe>, <script> and <style> place holders values */
        global $placeholders;
        if (!empty($placeholders)) {
            $buffer = str_replace(array_keys($placeholders), array_values($placeholders), $buffer);
        }
        $buffer = preg_replace('~[\r\n\t]<script src="(.*?)"></script>~','<script src="$1"></script>', $buffer);
        $buffer = preg_replace('~[\r\n\t]<link rel="stylesheet" property="stylesheet" href="/modules/system/assets/css/framework.extras.css">~','<link rel="stylesheet" property="stylesheet" href="/modules/system/assets/css/framework.extras.css">', $buffer);
        return $buffer;
    }


    /**
     * Remove HTML comments (not containing IE conditional comments).
     */
    protected static function minifyHtmlHtmlComment($string) {
        return (0 === strpos($string[1], '[') || false !== strpos($string[1], '<!['))
            ? $string[0]
            : '';
    }

    /*
     * Helper function to add place holder for <textarea> and <pre> tag
     */
    protected static function minifyHtmlCallback($m){
        return self::minifyReservePlace($m[0]);
    }

    /*
     * Helper function to add place holder for <iframe> tag
     */
    protected static function minifyHtmlIframeCallback($m) {
        $iframe = preg_replace('/^\\s+|\\s+$/m', '', $m[0]);
        return self::minifyReservePlace($iframe);
    }

    /*
     * Helper function to add place holder for <script> tag
     */
    protected static function minifyHtmlScriptCallback($m) {
        $search = array(
            '!/\*.*?\*/!s',     // remove multi-line comment
            '/^\\s+|\\s+$/m',   // trim each line
            '/\n(\s*\n)+/',     // remove multiple empty line
        );
        $replace = array('', "\n", "\n");
        $script = preg_replace($search, $replace, $m[0]);

        return self::minifyReservePlace($script);
    }

    /*
     * Helper function to add place holder for <style> tag
     */
    protected static function minifyHtmlStyleCallback($m) {
        $search = array(
            '!/\*.*?\*/!s',   // remove multiline comment
            '/^\\s+|\\s+$/m'  // trim each line
        );
        $replace = array('');
        $style = preg_replace($search, $replace, $m[0]);
        return self::minifyReservePlace($style);
    }

    /*
     * Helper function to add tag key and value for further replacement
     */
    protected static function minifyReservePlace($content) {
        global $placeholders;
        if ($placeholders !== null) {
            $placeholder = '%' . REPLACEMENT_HASH . count($placeholders) . '%';
            $placeholders[$placeholder] = $content;
            return $placeholder;
        }
        return $content;
    }

    public static function page($controller, $result)
    {
        if (!is_string($result)) {
            return $result;
        }

        $page = $controller->getPage();
        $statusCode = 200;
        if (!$page) {
            $statusCode = 404;
        }

        $result = self::minify($result);
        return Response::make($result, $statusCode);
    }
}