<?php namespace Pkurg\PageBuilder\Components;

use BackendAuth;
use Cms\Classes\ComponentBase;
use Cms\Classes\Content;
use Cms\Classes\Partial;
use File;
use Input;
use KubAT\PhpSimple\HtmlDomParser;
use Lang;
use Pkurg\PageBuilder\Models\Settings;
use Request;
use Twig;

class ContentBuilder extends ComponentBase
{

    public $content;
    public $file;
    public $fileMode;
    public $edit;
    public $id;
    public $jsoncontent;

    public $isbuilderedit;

    public function componentDetails()
    {
        return [
            'name' => 'ContentBuilder Component',
            'description' => 'Frontend content builder',
        ];
    }

    public function defineProperties()
    {
        return [
            'savedatato' => [
                'title' => 'Save page data to',
                'type' => 'dropdown',
                'default' => 'content',
                //                'description' => 'If you selected to use content, then you need to add to the page {% component &#x27;ContentBuilder&#x27; %}.  If you selected to use partial, then you need to add to the page
                // {% partial &#x27;mypatrial&#x27; %}',

            ],
            'file' => [
                'title' => 'File',
                'type' => 'dropdown',
                'depends' => ['savedatato'],

            ],

        ];
    }

    public function getSavedatatoOptions()
    {
        return ['content' => 'content', 'partial' => 'partial'];
    }
    public function getFileOptions()
    {

        $Code = Request::input('savedatato');

        if ($Code == 'content') {
            return Content::sortBy('baseFileName')->lists('baseFileName', 'fileName');
        }
        if ($Code == 'partial') {
            return Partial::sortBy('baseFileName')->lists('baseFileName', 'fileName');
        }

    }

    public function getHTML($html)
    {

        $html = HtmlDomParser::str_get_html($html);

        //remove slyles and scripts
        if ($html) {

            foreach ($html->find('script') as $script) {
                $script->remove();
            }

            foreach ($html->find('style') as $script) {
                $script->remove();
            }

            return $html->innertext;
        }

    }

    public function getScripts($html)
    {

        $html = HtmlDomParser::str_get_html($html);

        if ($html) {
            $scripts = '';
            foreach ($html->find('script') as $script) {
                $scripts = $scripts . $script;
            }
            return $scripts;

        }

    }

    public function onRun()
    {

        foreach ($this->assets['css'] as $value) {
            $this->page->addCss($value, 'core');
        }

        foreach ($this->assets['js'] as $value) {
            $this->page->addJs($value, 'core');
        }

        $this->page['editmode'] = Input::get('builder-edit-mode');
        $this->page['editor'] = Input::get('editor');

        $this->page['customblocks'] = Settings::get('customblocks');

        $this->id = uniqid();

        $this->edit = $this->isEdit();

        $this->page['curdir'] = url('/');

        $this->page['mediapath'] = config('cms.storage.media.path');

        $this->page['backendpath'] = config('cms.backendUri');

        $defaultdisk = \Config::get('filesystems.default');

        $path = url(\Config::get('filesystems.disks.' . $defaultdisk . '.url'));

        $files = \Storage::allFiles('media/BuilderUploader');

        $purl = parse_url($path);

        $this->page['path'] = $purl['path'];

        $this->page['files'] = $files;

        $this->page['octobermedia'] = Settings::get('useoctobermedia');

        $mediadir = \Config::get('cms.storage.media.folder');

        $this->page['mediadir'] = $purl['path'] . '/' . $mediadir;

        $this->file = $this->property('file');
        $this->fileMode = File::extension($this->property('file'));

        if ($this->property('file') == null) {

            $this->content = 'No file';

        } else {

            if ($this->property('savedatato') == 'content') {
                $this->content = $this->renderContent($this->file);
                $this->jsoncontent = json_encode($this->content);
            }

            if ($this->property('savedatato') == 'partial') {
                $this->content = $this->renderPartial($this->file);

                $fileName = $this->file;
                $template = Partial::load($this->getTheme(), $fileName);
                //dd($template->content);

                $this->jsoncontent = json_encode($template->markup);
            }

        }

    }

    public function onSaveFile()
    {
        if (!$this->isEdit()) {
            return;
        }

        $fileName = post('file');

        if ($this->property('savedatato') == 'partial') {
            $template = Partial::load($this->getTheme(), $fileName);
            $template->fill(['markup' => post('content')]);
            $template->save();
        }

        if ($this->property('savedatato') == 'content') {
            $template = Content::load($this->getTheme(), $fileName);
            $template->fill(['markup' => post('content')]);
            $template->save();
        }
    }

    public function isEdit()
    {
        $backendUser = BackendAuth::getUser();
        return $backendUser && ($backendUser->hasAccess('cms.manage_content') || $backendUser->hasAccess('rainlab.pages.manage_content'));
    }

    public function getSetting($setting)
    {

        if (Settings::get($setting)) {
            return Twig::parse(Settings::get($setting));
        } else {
            return '';

        }

    }

    ////////////////MEdia

    public $mediaManagerWidget;

    public $mediaFinder;

    public $assets = [];

    public $c;

    public function init()
    {

        if (\Input::has('builder-edit-mode')) {

            $isedit = \Input::get('builder-edit-mode');
            if ($isedit == '1') {

                $this->isbuilderedit = true;
            }
        }

        if ($this->isbuilderedit) {

            $this->assets['css'] = [
                '/modules/system/assets/ui/storm.css',
                '/modules/backend/assets/css/october.css',
            ];

            $this->assets['js'] = [
                '/modules/system/assets/js/lang/lang.' . Lang::getLocale() . '.js',
                '/modules/system/assets/ui/storm-min.js',
                '/modules/backend/assets/js/october-min.js',
            ];

            $this->c = new \Backend\Classes\Controller();
            $this->mediaManagerWidget = new \Backend\Widgets\MediaManager($this->c, $this->alias, $this->property('readOnly'));
            $this->bindHandlers($this->mediaManagerWidget);

            $this->mergeAssets();

        } else {

            $this->assets['css'] = [];

            $this->assets['js'] = [];

            $this->mergeAssets();

        }
    }

    private function removeVersionsAssets($arrData)
    {
        if (is_array($arrData)) {
            foreach ($arrData as $key => $value) {
                if (is_array($value)) {
                    $arrData[$key] = $this->removeVersionsAssets($value);
                } else {
                    $arr = explode("?v", $value);
                    $arrData[$key] = (is_array($arr)) ? $arr[0] : $value;
                }
            }

        }

        return $arrData;
    }

    private function getAssets($widget)
    {
        $assets = $this->removeVersionsAssets($widget->getAssetPaths());
        $this->assets['css'] = (array_key_exists('css', $this->assets)) ? array_merge($this->assets['css'], $assets['css']) : $this->assets['css'] = $assets['css'];
        $this->assets['js'] = (array_key_exists('js', $this->assets)) ? array_merge($this->assets['js'], $assets['js']) : $this->assets['js'] = $assets['js'];
    }

    private function mergeAssets()
    {
        $this->page['assets'] = (is_array($this->page['assets'])) ? array_merge($this->assets, $this->page['assets']) : $this->page['assets'] = $this->assets;
    }

    private function bindHandlers($widget)
    {
        if (is_object($widget)) {
            $widget->viewPath = $this->property('viewPath');
            $this->bindHandlersNoPath($widget);
        }
    }

    private function bindHandlersNoPath($widget)
    {
        if (is_object($widget)) {
            $widget->alias = $this->alias;
            //$widget->viewPath = $this->property('viewPath');
            $class_methods = get_class_methods($widget);

            foreach ($class_methods as $method_name) {
                if (strtolower(substr($method_name, 0, 2)) == 'on') {
                    //array_push($this->relationsarray,$method_name);
                    $this->addDynamicMethod($method_name, function () use ($method_name, $widget) {
                        return $widget->{$method_name}();
                    });
                }
            }

            $this->getAssets($widget);
        }
    }

}
