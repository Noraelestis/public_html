##Table of Contents
1. Backend Page Builder
2. Frontend Content Builder
3.  Custom blocks
4. Add Google Fonts
5. Add styles and scripts to the Builder canvas
6. Using components of other plugins with a Page Builder in the frontend

##1) Backend Page Builder

Video manual
!![640x360](//player.vimeo.com/video/362164143)

##2) Frontend Content Builder

Create layout.

example **builder-layout.htm** (modifed layout from demo theme)

    ==
	<!DOCTYPE html>
	<html>
	<head>
	    <meta charset="utf-8">
	    <title>October CMS - {{ this.page.title }}</title>
	    <meta name="description" content="{{ this.page.meta_description }}">
	    <meta name="title" content="{{ this.page.meta_title }}">
	    <meta name="author" content="OctoberCMS">
	    <meta name="viewport" content="width=device-width, initial-scale=1.0">
	    <meta name="generator" content="OctoberCMS">
	    <link rel="icon" type="image/png" href="{{ 'assets/images/october.png'|theme }}">
	    {% styles %}
	</head>
	<body>   
	    {% page %}
	    <script src="{{ 'assets/vendor/jquery.js'|theme }}"></script>
	    <script src="{{ 'assets/vendor/bootstrap.js'|theme }}"></script>
	    <script src="{{ 'assets/javascript/app.js'|theme }}"></script>
	    {% framework extras %}
	    {% scripts %}
	</body>
	</html>


Create content file. 
**example empty main.htm**

Create cms page and add ContentBuilder component.

**example main.htm**

	title = "main"
	url = "/"
	layout = "builder-layout"
	is_hidden = 0
	robot_index = "index"
	robot_follow = "follow"

	[ContentBuilder]
	file = "main.htm"
	==
	{% component 'ContentBuilder' %}

Go to frontend and edit your page.

Video manual
!![640x360](//player.vimeo.com/video/366377298)
!![640x360](//player.vimeo.com/video/366378242)

##3)  Custom blocks
Go to Builder settings and add your custom blocks code

![](https://i.ibb.co/w0pFZKT/screenshot-demo-pkurg-ru-2019-10-27-05-48-30.png)

![](https://i.ibb.co/sW0VYTY/screenshot-demo-pkurg-ru-2019-10-27-05-49-35.png)


**Custom Table component example** 
----

https://www.youtube.com/watch?v=TYXUikMomEo



     var blockManager = Editor.BlockManager;
            blockManager.add('table-block', {
              id: 'table',
              label: 'Table',
              category: 'Basic',
              attributes: { class: 'fa fa-table' },
              content: `<style>td { min-width: 100px; height: 20px;}</style>
                  <table class="table  table-bordered table-resizable">
                      <tr><td></td><td></td><td></td></tr>
                      <tr><td></td><td></td><td></td></tr>
                      <tr><td></td><td></td><td></td></tr>
                  </table>
                `,
            });
            var TOOLBAR_CELL = [
              {
                attributes: { class: "fa fa-arrows" },
                command: "tlb-move"
              },
              {
                attributes: { class: "fa fa-flag" },
                command: "table-insert-row-above"
              },
              
              {
                attributes: {class: 'fa fa-clone'},
                command: 'tlb-clone',
              },
              {
                attributes: {class: 'fa fa-trash-o'},
                command: 'tlb-delete',
              }
            ];
            var getCellToolbar = () => TOOLBAR_CELL;


            var components = Editor.DomComponents;
            var text = components.getType('text');
            components.addType('cell', {
              model: text.model.extend({
                defaults: Object.assign({}, text.model.prototype.defaults, {
                  type: 'cell',
                  tagName: 'td',
                  draggable: ['tr'],
                  
                }),
              },

                {
                  isComponent(el) {
                    let result;
                    var tag = el.tagName;
                    if (tag == 'TD' || tag == 'TH') {
                      result = {
                        type: 'cell',
                        tagName: tag.toLowerCase()
                      };
                    }
                    return result;
                  }
                }),
              view: text.view,
            });

            

            Editor.on('component:selected', m => {
              var compType = m.get('type');
              switch (compType) {
                case 'cell':
                  m.set('toolbar', getCellToolbar()); // set a toolbars
              }
            });



            Editor.Commands.add('table-insert-row-above', editor => {
              var selected = editor.getSelected();

              if (selected.is('cell')) {
                var rowComponent = selected.parent();
                var rowIndex = rowComponent.collection.indexOf(rowComponent);
                var cells = rowComponent.components().length;
                var rowContainer = rowComponent.parent();

                rowContainer.components().add({
                  type: 'row',
                  components: [...Array(cells).keys()].map(i => ({
                    type: 'cell',
                    content: 'New Cell',
                  }))
                }, { at: rowIndex });
              }
            });
----

##4)  Add Google Fonts

Video manual -> https://www.youtube.com/watch?v=9gG5qivNoQM

Add 'Poppins', sans-serif  example



	var head = Editor.Canvas.getDocument().head;
	head.insertAdjacentHTML('beforeend', `<style>
	@import url('https://fonts.googleapis.com/css?family=Poppins&display=swap');
	</style>`);

	Editor.on('load', function () {
	    styleManager = Editor.StyleManager;
	    
	    fontProperty = styleManager.getProperty('typography', 'font-family');
	    
	    var list = [];
	    fontProperty.set('list', list);
	    list = [                     
	        fontProperty.addOption({value: "Arial, Helvetica, sans-serif", name: 'Arial'}),
	        fontProperty.addOption({value: "Arial Black, Gadget, sans-serif", name: 'Arial Black'}),
	        fontProperty.addOption({value: "Brush Script MT, sans-serif", name: 'Brush Script MT'}),
	        fontProperty.addOption({value: "Comic Sans MS, cursive, sans-serif", name: 'Comic Sans MS'}),
	        fontProperty.addOption({value: "Courier New, Courier, monospace", name: 'Courier New'}),
	        fontProperty.addOption({value: "Georgia, serif", name: 'Georgia'}),
	        fontProperty.addOption({value: "Helvetica, serif', cursive", name: 'Helvetica'}),
	        fontProperty.addOption({value: "Impact, Charcoal, sans-serif", name: 'Impact'}),
	        fontProperty.addOption({value: "Lucida Sans Unicode, Lucida Grande, sans-serif", name: 'Lucida Sans Unicode'}),
	        fontProperty.addOption({value: "Tahoma, Geneva, sans-serif", name: 'Tahoma'}),
	        fontProperty.addOption({value: "Times New Roman, Times, serif", name: 'Times New Roman'}),
	        fontProperty.addOption({value: "Trebuchet MS, Helvetica, sans-serif", name: 'Trebuchet MS'}),
	        fontProperty.addOption({value: "Verdana, Geneva, sans-serif", name: 'Verdana'}),
	        fontProperty.addOption({value: "'Poppins', sans-serif", name: 'Poppins'}),
	        
	    ];
	    fontProperty.set('list', list);
	    styleManager.render();
	});


##5) Add styles and scripts to the Builder canvas

![](https://i.ibb.co/KV6rCVm/screenshot-devv-pkurg-ru-2020-01-19-22-28-00.png)

!![640x360](//www.youtube.com/embed/ReQtZ5-Nza8)

----
##6) Using Plugins Components with Page Builder in Frontend
!![640x360](//www.youtube.com/embed/CANIIuEjVeM)
----
##**Key features**


##Drag&Drop Built-in Blocks
Plugin comes with a set of built-in blocks, in this way you're able to build your templates faster. If the default set is not enough you can always add your own custom blocks.
##Limitless styling
Plugin implements simple and powerful Style Manager module which enables independent styling of any component inside the canvas. It's also possible to configure it to use any of the CSS properties
##Responsive design
GrapesJS gives you all the necessary tools you need to optimize your templates to look awesomely on any device. In this way you're able to provide various viewing experience. In case more device options are required, you can easily add them to the editor.
##The structure always under control
You can nest components as much as you can but when the structure begins to grow the Layer Manager comes very handy. It allows you to manage and rearrange your elements extremely faster, focusing always on the architecture of your structure.
##The code is there when you need it
You don't have to care about the code, but it's always there, available for you. When the work is done you can grab and use it wherever you want. Developers could also implement their own storage interfaces to use inside the editor.
##Asset Manager
With the Asset Manager is easier to organize your media files and it's enough to double click on the image to change it