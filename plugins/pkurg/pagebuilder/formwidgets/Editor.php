<?php namespace Pkurg\PageBuilder\FormWidgets;

use Backend\Classes\FormWidgetBase;
use Pkurg\PageBuilder\Models\Settings;
use Twig;

class Editor extends FormWidgetBase
{

    /**
     * @var string A unique alias to identify this widget.
     */
    protected $defaultAlias = 'pagebuilder';

    public function render()
    {

        //$this->vars['id'] = $this->getId();
        //$this->vars['editorname'] = uniqid();

        $this->vars['name'] = $this->getFieldName();
        $this->vars['value'] = $this->getLoadValue();

        if (Settings::get('builder_styles')) {
            $this->vars['builder_styles'] = Twig::parse(Settings::get('builder_styles'));
        } else {
            $this->vars['builder_styles'] = '';
        }

        if (Settings::get('builder_scripts')) {
            $this->vars['builder_scripts'] = Twig::parse(Settings::get('builder_scripts'));
        } else {
            $this->vars['builder_scripts'] = '';
        }

        $this->vars['curdir'] = url('/');

        $this->vars['backendpath'] = config('cms.backendUri');

        $this->vars['mediapath'] = config('cms.storage.media.path');

        $this->vars['customblocks'] = Settings::get('customblocks');

        $defaultdisk = \Config::get('filesystems.default');

        $path = url(\Config::get('filesystems.disks.' . $defaultdisk . '.url'));

        $files = \Storage::allFiles('media/BuilderUploader');

        $purl = parse_url($path);

        $this->vars['path'] = $purl['path'];

        $this->vars['files'] = $files;

        $this->vars['octobermedia'] = Settings::get('useoctobermedia');

        $mediadir = \Config::get('cms.storage.media.folder');

        $this->vars['mediadir'] = $purl['path'] . '/' . $mediadir;

        return $this->makePartial('builder');
    }

}
