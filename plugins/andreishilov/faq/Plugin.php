<?php

namespace AndreiShilov\FAQ;

use AndreiShilov\FAQ\Models\FAQItem;
use Backend\Facades\Backend;
use System\Classes\PluginBase;
use System\Classes\PluginManager;

class Plugin extends PluginBase
{
    public function pluginDetails()
    {
        return [
            'name' => 'andreishilov.faq::lang.plugin.name',
            'description' => 'andreishilov.faq::lang.plugin.description',
            'author' => 'andreishilov.faq::lang.plugin.author',
            'icon' => 'icon-question'
        ];
    }

    public function registerComponents()
    {
        return [
            'AndreiShilov\FAQ\Components\FAQ' => 'faq',
        ];
    }

    public function registerPermissions()
    {
        return [
            'andreishilov.faq.edit' => [
                'label' => 'andreishilov.faq::lang.plugin.name',
            ],
        ];
    }

    public function registerNavigation()
    {
        return [
            'faq' => [
                'label' => 'FAQ',
                'url' => Backend::url('andreishilov/faq/faqitems'),
                'icon' => 'icon-question',
                'iconSvg' => '/plugins/andreishilov/faq/assets/images/faq-icon.svg',
                'permissions' => ['andreishilov.faq.*'],
            ],
        ];
    }

    public function boot()
    {
        $pluginManager = PluginManager::instance();
        if (
            $pluginManager->hasPlugin('RainLab.Translate') &&
            !$pluginManager->isDisabled('RainLab.Translate')
        ) {
            FAQItem::extend(function ($model) {
                $model->implement[] = 'RainLab.Translate.Behaviors.TranslatableModel';
            });
        }
    }
}
