<?php

namespace AndreiShilov\FAQ\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateTables extends Migration
{
    public function up()
    {
        Schema::create('andreishilov_faq_faqitems', function ($table) {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->boolean('is_active')->default(1);
            $table->text('question');
            $table->text('answer');
            $table->integer('sort_order')->default(0);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('andreishilov_faq_faqitems');
    }
}