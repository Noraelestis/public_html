<?php

namespace AndreiShilov\FAQ\Models;

use October\Rain\Database\Traits\Sortable;

class FAQItem extends \Model
{
    use Sortable;

    public $table = 'andreishilov_faq_faqitems';
    public $implement = [];
    public $translatable = ['question', 'answer'];

    public function getQuestionReorderAttribute()
    {
        return strip_tags($this->question);
    }

    public function scopeIsActive($query)
    {
        return $query->where('is_active', 1);
    }
}
