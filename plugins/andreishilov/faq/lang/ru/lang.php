<?php return [
    'plugin' => [
        'name' => 'FAQ',
        'description' => 'Плагин реализует раздел FAQ',
        'author' => 'Andrei Shilov',
    ],
    'components' => [
        'faq' => [
            'name' => 'FAQ',
            'description' => 'Компонент отображает FAQ на странице',
            'params' => [
                'add_jsonld' => [
                    'name' => 'Добавлять JSON-LD',
                    'description' => 'Добавлять JSON-LD FAQ схему в секцию head'
                ]
            ]
        ],
    ],
    'models' => [
        'faqitem' => [
            'name' => 'FAQ Элемент',
            'name_plural' => 'FAQ Элементы',
            'attributes' => [
                'id' => 'id',
                'is_active' => 'Активен',
                'question' => 'Вопрос',
                'answer' => 'Ответ',
            ],
            'fields' => [
                'is_active' => [
                    'label' => 'Активен'
                ],
                'question' => [
                    'label' => 'Вопрос'
                ],
                'answer' => [
                    'label' => 'Ответ'
                ],
            ],
            'columns' => [
                'is_active' => [
                    'label' => 'Активен'
                ],
                'question' => [
                    'label' => 'Вопрос'
                ],
                'answer' => [
                    'label' => 'Ответ'
                ],
            ],
        ],
    ]
];