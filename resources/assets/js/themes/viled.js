var $ = window.$ = window.jQuery = require('jquery.2');
require('bootstrap-sass');
require('jquery.inputmask');
require('fancybox')($);
$('.fancybox').fancybox();
require('./../../../../node_modules/waypoints/lib/jquery.waypoints.min.js');
var FormValidator = require('./../partials/validate.js');
require('./../partials/viled/price-form.js');
if ($('.main').hasClass('page-free-project')) {
    require('./../partials/viled/free-projects-form.js');
}
require('./../partials/search_form.js');
require('./../partials/animate_button.js');
require('./../partials/lp_form_label');
require('./../partials/scroll-to-top');
var slick = require('slick-carousel-browserify');

require('./../partials/geoapi/list').init();

//run slick slider
$(document).ready(function () {
    var slider = $('.plan_banner');
    if (slider.length !== 0) {
        slick(slider, {
            infinite: true,
            slidesToShow: 2,
            slidesToScroll: 1,
            dots: true,
            responsive: [
                {
                    breakpoint: 750,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        });
    }
});

$(document).ready(function () {

    require('./../partials/filter').init();
    if ($('#order-form-modal').length > 0) {
        require('./../partials/order_form').init({currencyView: '<i class="icon-rouble"></i>'});
    }
});


if ($('.table-products').length > 0) {
    var dt = require('datatables.net')(window, $);
    $(document).ready(function () {
        $('.table-products').DataTable({
            "bFilter": false,
            "bPaginate": false,
            "bInfo": false,
            "order": [[5, "asc", "numeric"]],
            "language": {
                "emptyTable": "Данные отсутствуют, по Вашему запросу ничего не найдено."
            },
            "columnDefs": [
                {
                    "targets": 6,
                    "orderable": false
                }]
        });
    });
}

if ($('.card_slider').length > 0) {
    require('./../partials/portal/carousel').init();
}

require('./../partials/per_page_switcher')('#full-products__per-page');
Inputmask("+7-(999)-999-99-99").mask($(".form_phone"));
require('./../partials/fix_header');

$(document).ready(function () {
    $('.capture-contacts__button').click(function () {
        jivo_api.open();
    });
});