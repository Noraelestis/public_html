window.$ = window.jQuery = require('jquery.2');
$(document).ready(function () {
    require('bootstrap-sass');
    require('jquery.inputmask');
    require('fancybox')($);
    $('.fancybox').fancybox();
    require('./../../../../node_modules/waypoints/lib/jquery.waypoints.min.js');
    var FormValidator = require('./../partials/validate.js');
    require('./../partials/viled/price-form.js');
    if ($('.main').hasClass('page-raschet_osvecheniya')) {
        require('./../partials/viled/free-projects-form.js');
    }
    require('./../partials/search_form.js');
    require('./../partials/animate_button.js');
    require('./../partials/lp_form_label');
    require('./../partials/scroll-to-top');
    require('./../partials/geoapi/list').init();
    require('./../partials/get-form');

    $(document).ready(function () {

        require('./../partials/filter').init();
        if ($('#order-form-modal').length > 0) {
            require('./../partials/order_form').init({currencyView: '<i class="icon-rouble"></i>'});
        }
    });

    if ($('.card_slider').length > 0) {
        require('./../partials/portal/carousel').init();
    }

    require('./../partials/per_page_switcher')('#full-products__per-page');

    require('./../partials/fix_header');

    if ($('.tabs-grid').length > 0) {
        var Masonry = require('masonry-layout');
        new Masonry('.tabs-grid', {});
    }
    $('.capture-contacts__button').click(function () {
        jivo_api.open();
    });
    if(document.getElementsByClassName("contacts_form_input").length > 0) {
        Inputmask("+7-(999)-999-99-99").mask(document.getElementsByClassName("contacts_form_input")[1]);
    }
});
