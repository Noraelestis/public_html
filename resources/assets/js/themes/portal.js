var $ = window.$ = window.jQuery = require('jquery.2');
require('bootstrap-sass');
require('jquery.inputmask');
require('fancybox')($);
$('.fancybox').fancybox();
require('./../../../../node_modules/waypoints/lib/jquery.waypoints.min.js');
var FormValidator = require('./../partials/validate.js');
require('./../partials/get-form.js');
require('./../partials/search_form.js');
require('./../partials/animate_button.js');
require('./../partials/lp_form_label');
require('./../partials/scroll-to-top');
require('./../libs/dataTables/jquery.dataTables.min');
require('./../partials/geoapi/list').init();
var slick = require('slick-carousel-browserify');

//run slick slider
$(document).ready(function () {
    slick($('.manufacturers-slider'), {
        infinite: true,
        slidesToShow: 6,
        slidesToScroll: 5,
        dots: true,
        responsive: [
            {
                breakpoint: 1300,
                settings: {
                    slidesToShow: 6,
                    slidesToScroll: 5
                }
            },
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 5,
                    slidesToScroll: 4
                }
            },
            {
                breakpoint: 858,
                settings: {
                    slidesToShow: 4,
                    slidesToScroll: 3
                }
            },
            {
                breakpoint: 600,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 2
                }
            },
            {
                breakpoint: 480,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1
                }
            }
        ]
    });
});

require('./../partials/filter').init();
$(document).ready(function () {
    if ($('#order-form-modal').length > 0) {
        require('./../partials/order_form').init();
    }
});
if ($('.table-products').length > 0) {
    var dt = require('datatables.net')(window, $);
    $(document).ready(function () {
        $('.table-products').DataTable({
            "bFilter": false,
            "bPaginate": false,
            "bInfo": false,
            "order": [[5, "asc", "numeric"]],
            "language": {
                "emptyTable": "Данные отсутствуют, по Вашему запросу ничего не найдено."
            },
            "columnDefs": [
                {
                    "targets": 6,
                    "orderable": false
                }]
        });
    });
}

if ($('.card_slider').length > 0) {
    require('./../partials/portal/carousel').init();
}

require('./../partials/portal/tree-menu');

require('./../partials/per_page_switcher')('#full-products__per-page');

$('#header_row_utp > .icon').click(function () {
    $(this).parent().remove();
});

$('#top-navigation--menu-activator').click(function () {
    $('body').toggleClass('menu-opened');
});

$('#top-navigation--menu-deactivator').click(function () {
    $('#top-navigation--menu-activator').click();
});

require('./../partials/fix_header');

$(document).ready(function () {
    $('.capture-contacts__button').click(function () {
        jivo_api.open();
    });
});