$(document).ready(function () {
    require('./../plugins/ShoppingCart/quantity')();
    var Inputmask = require('inputmask');
    Inputmask("+7-(999)-999-99-99").mask(document.getElementsByClassName('form_phone'));
    require('./../partials/geoapi/list').init();

    var svFilter = window.svFilter = require('./../partials/filter/svFilter');
    $('#block-filter').trigger('ready');
    $('#filter_submit').click(function () {
        svFilter.submit();
    });
    $('#filter_reset').click(function () {
        window.location.href = window.location.origin + window.location.pathname;
    });

    $('[data-src="#one-click"]').click(function () {
        var title = $('.product-title').text();
        var submitBtn = $('#forma-kupit-v-odin-klik button[type="submit"]');
        if (this.dataset.target === 'product') {
            $('#one-click-title').html('Купить ' + title);
            submitBtn.text('Купить');
        } if (this.dataset.target === 'opt') {
            $('#one-click-title').html('Запрос оптовой цены на ' + title);
            submitBtn.text('Отправить');
        } if (this.dataset.target === 'small-cart') {
            $('#one-click-title').html('Купить в 1 клик');
            submitBtn.text('Купить');
        }
        $('#one-click-product').val(title);
        $('#one-click-type').val(this.dataset.target);
    });

    $('[data-shopcart-typecustomer]').click(function () {
        if (parseInt($(this).val()) === 1) {
            $('[data-shopcart-uploader]').hide();
        } else {
            $('[data-shopcart-uploader]').show();
        }
    });

    $('[data-shopcart-quantityupdate]').on('change', function () {
        var id = this.dataset.id;
        $('[data-shopcart-add][data-id="' + id + '"]').attr('data-request-data', 'id: ' + id + ', count: ' + $(this).val());
    });
    $('.capture-contacts__button').click(function () {
        jivo_api.open();
    });

    (function () {
        var cartBlock = $('#cart-hiden-block');
        var cart = $(".sv-shopping-cart");
        var favorites = $('.sv-favorites');
        if (typeof window.shop === 'undefined') {
            window.shop = {};
        }
        window.shop.openCart = function () {
            cartBlock.removeClass('hiden').addClass('visible');
            cartBlock.animate({
                right: "-5px"
            }, 400);
            if (cart.hasClass('hidden')) {
                cart.removeClass('hidden').addClass('visible');
                favorites.removeClass('visible').addClass('hidden');
            }
        };
        window.shop.openFavorites = function () {
            cartBlock.removeClass('hiden').addClass('visible');
            cartBlock.animate({
                right: "-5px"
            }, 400);
            if (favorites.hasClass('hidden')) {
                favorites.removeClass('hidden').addClass('visible');
                cart.removeClass('visible').addClass('hidden');
            }
        };
        $('#favorit').click(function () {
            window.shop.openFavorites();
        });

        $('#cart').click(function () {
            window.shop.openCart();
        });
        $('#closeCardButton').click(function () {
            $('#cart-hiden-block').removeClass('visible').addClass('hiden');
            $('#cart-hiden-block').animate({
                right: "-360px"
            }, 400);
        });

    })();
});

window.svCart = require('./../plugins/ShoppingCart/index');
window.svOrder = require('./../plugins/OrderForm/index');
window.svFavorites = require('./../plugins/Favorites/index');
