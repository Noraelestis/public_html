module.exports = {
    afterSubmit: function (form, context, data) {
        form.reset();
        $('[data-shopcart-uploader]').hide();
        $(form).find('.dz-preview').remove();
        if (data.status === 'success') {
            window.svCart.afterReset();
            $('[data-orderform-result-success]').html(data.result);
            $('html, body').animate({scrollTop: 0}, 500);
            dataLayer.push({'event':'cart_send', 'eventCategory':'order', 'eventAction':'cart_send'});
        } else {
            $('[data-orderform-result-error]').html(data.result);
        }
    },

    afterSubmitLowPrice: function (form, context, data) {
        if (data.status === 'success') {
            form.reset();
            $('.modal-body-low-price .info').hide();
            $('.modal-body-low-price img').hide();
            $('.modal-body-low-price .contacts').hide();
            $('#modal-content-low-price .modal-footer1').html('');
            $('.modal-body-low-price .success').show();
            dataLayer.push({'event':'get_cheap', 'eventCategory':'order', 'eventAction':'get_cheap'});
            // console.log(data)
        } else {
            $('#modal-body-low-price').html('Что-то пошло не так...<br>Попробуйте позже.')
            $('#modal-content-low-price .modal-footer1').html('');
        }
    },

    afterSubmitSpecialPrice: function (form, context, data) {
        if (data.status === 'success') {
            form.reset();
            $('.modal-body-special-price .info').hide();
            $('.modal-body-special-price img').hide();
            $('.modal-body-special-price .contacts').hide();
            $('#modal-content-low-price .modal-footer2').html('');
            $('.modal-body-special-price .success').show();
            dataLayer.push({'event':'get_special_price', 'eventCategory':'order', 'eventAction':'get_special_price'});
            // console.log(data)
        } else {
            $('.modal-body-special-price').html('Что-то пошло не так...<br>Попробуйте позже.')
            $('#modal-content-low-price .modal-footer1').html('');
        }
    }
};
