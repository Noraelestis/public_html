var priceFormatting = require('./../../partials/priceFormatting');

module.exports = {
    afterAdd: function (context, data) {
        window.shop.openFavorites();
        var id = this.dataset.id;
        if ($('[data-shopfavorites-item][data-id="' + id + '"]').length > 0) {
            $('[data-shopfavorites-quantity][data-id="' + id + '"]').val(data.quantity);
        } else {
            $('[data-shopfavorites-area]').append(data.render);
            $('[data-shopfavorites-count]').text(data.count);
            $('[data-shopfavorites-reset]').prop('disabled', false);
            $('[data-shopfavorites-label-product]').text(data.labels.product);
        }
    },
    afterDelete: function (context, data) {
        var product = $('[data-shopfavorites-item][data-id="' + this.dataset.id + '"]');
        product.hide(200, function () {
            product.remove();
            if ($('[data-shopfavorites-item]').length === 0) {
                window.svFavorites.afterReset();
            }
        });
        $('[data-shopfavorites-count]').text(data.count);
        $('[data-shopfavorites-label-product]').text(data.labels.product);
    },
    afterReset: function () {
        $('[data-shopfavorites-area]').html('');
        $('[data-shopfavorites-count]').text(0);
        $('[data-shopfavorites-reset]').prop('disabled', true);
        $('[data-shopfavorites-submit]').prop('disabled', true);
        var label = $('[data-shopfavorites-label-product]');
        label.text(label.data('label-default'));
    }
};
