var debounce = require('debounce');
var recount = debounce(function (data) {
    $.request('onChangeProductCount', {
        data: data,
        success: function (result) {
            result.id = data.id;
            window.svCart.afterUpdateCount(null, result);
        }
    });
}, 250);

module.exports = function (id) {
    var input;
    if (typeof id === 'undefined') {
        input = $('[data-shopcart-quantity]');
    } else {
        input = $('[data-shopcart-quantity][data-id="' + id + '"]');
    }

    input.change(function () {
        var input = $(this);
        recount({id: input.data('id'), count: input.val()});
    });
};
