var inputNumber = require('./../../partials/button_number');
var priceFormatting = require('./../../partials/priceFormatting');

function createCountNumbers() {
    let count = 1;
    let items = $('.order-form__products-item');
    items.map(i => {
        let item = items[i];
        $(".basket-sm__number", item).text(count);
        count++;
    });
}

function checkEmptyCart() {
    let location = document.location.href;
    location = location.slice(-5);
    if (location === '/cart') {
        if ($('[data-shopcart-item]').length === 0) {
            $('.emptyCartError').css('display', 'flex');
            $('.fullCartProductAreaForHidden').css('display', 'none')
        } else {
            $('.emptyCartError').css('display', 'none');
            $('.fullCartProductAreaForHidden').css('display', 'flex')
        }
    }
}


module.exports = {
    afterAdd: function (context, data) {
        window.shop.openCart();
        var id = this.dataset.id;
        if ($('[data-shopcart-item][data-id="' + id + '"]').length > 0) {
            $('[data-shopcart-quantity][data-id="' + id + '"]').val(data.quantity);
        } else {
            $('[data-shopcart-area]').append(data.render);
            inputNumber({
                btn: '.btn-number[data-field="quantity[' + id + ']"]',
                input: '.input-number[name="quantity[' + id + ']"]'
            });
        }
        $('[data-shopcart-result]').text(priceFormatting(data.priceResult));
        $('[data-shopcart-count]').text(data.countFull);
        $('[data-shopcart-sum][data-id="' + id + '"]').text(priceFormatting(data.sum));
        $('[data-shopcart-reset]').prop('disabled', false);
        $('[data-shopcart-submit]').prop('disabled', false);
        $('[data-shopcart-label-product]').text(data.labels.product);
        require('./quantity')(id);
        // console.log(id);
        createCountNumbers();
        checkEmptyCart();
    },
    afterDelete: function (context, data) {
        var product = $('[data-shopcart-item][data-id="' + this.dataset.id + '"]');
        product.hide(200, function () {
            product.remove();
            createCountNumbers();
            if ($('[data-shopcart-item]').length === 0) {
                window.svCart.afterReset();
            }
        });
        $('[data-shopcart-result]').text(priceFormatting(data.priceResult));
        $('[data-shopcart-count]').text(data.countFull);
        $('[data-shopcart-discount]').text(priceFormatting(data.discount) + ' р.');
        $('[data-shopcart-label-product]').text(data.labels.product);

    },
    afterUpdateCount: function (context, data) {
        var id = data.id;
        if (id === 'undefined') {
            id = this.dataset.id;
        }
        $('[data-shopcart-result]').text(priceFormatting(data.priceResult) + ' р.');
        $('[data-shopcart-count]').text(data.countFull);
        if (data.discount) {
            $('[data-shopcart-discount]').text(priceFormatting(data.discount) + ' р.');
        }
        $('[data-shopcart-sum][data-id="' + id + '"]').text(priceFormatting(data.sum));
        createCountNumbers();
        checkEmptyCart();
    },
    afterReset: function () {
        $('[data-shopcart-area]').html('');
        $('[data-shopcart-result]').text(0);
        $('[data-shopcart-count]').text(0);
        $('[data-shopcart-reset]').prop('disabled', true);
        $('[data-shopcart-submit]').prop('disabled', true);
        var label = $('[data-shopcart-label-product]');
        label.text(label.data('label-default'));
        createCountNumbers();
        checkEmptyCart();
    },
    afterFastAdd: function (context, data) {
        var id = this.dataset.id;
        if ($('[data-shopcart-item][data-id="' + id + '"]').length > 0) {
            $('[data-shopcart-quantity][data-id="' + id + '"]').val(data.quantity);
        } else {
            $('.fullCartProductArea').append(data.renderFull);
            $('.sideCartProductArea').append(data.render);
            inputNumber({
                btn: '.btn-number[data-field="quantity[' + id + ']"]',
                input: '.input-number[name="quantity[' + id + ']"]'
            });
        }
        $('[data-shopcart-result]').text(priceFormatting(data.priceResult));
        $('[data-shopcart-count]').text(data.countFull);
        $('[data-shopcart-sum][data-id="' + id + '"]').text(priceFormatting(data.sum));
        $('[data-shopcart-reset]').prop('disabled', false);
        $('[data-shopcart-submit]').prop('disabled', false);
        $('[data-shopcart-discount]').text(priceFormatting(data.discount) + ' р.');
        $('[data-shopcart-label-product]').text(data.labels.product);
        require('./quantity')(id);
        createCountNumbers();
        checkEmptyCart();
    }
};


$(document).ready(() => {
    checkEmptyCart()
    $('#fastAddingProductSelect').on('click', ".fastAddProductItem", function () {
        $('#inputFastAdding').val('')
    });
    $('.fastAddingForm').focusout(function () {
        $('#fastAddingProductSelect').hide(200, () => {
            $('#fastAddingProductSelect').css('display', 'none');
        })
    });


    $(document).on('submit', '.fastAddProductItem', function documentOnSubmit() {
        $(this).request();
        return false
    });

    $("#fastAddingForm").submit(function () {
        const input = $("#inputFastAdding").val();
        $.request('onType', {
            data: {
                fastAddingInput: input,
            },
            success: function (data) {
                const products = Object.values(data.products);
                if (products.length > 0) {
                    $('#fastAddingProductSelect').html('');
                    $('#fastAddingProductSelect').css('display', 'flex');

                    products.map(prod => {
                        $('#fastAddingProductSelect').append("<div><a " +
                            "data-request='onAddProductToCart' " +
                            // "data-request-success='svCart.afterAdd.call(this, context, data)' " +
                            "data-request-success='svCart.afterFastAdd.call(this, context, data)' " +
                            "data-id='" + prod.id + "'" +
                            "data-request-data='id: " + prod.id + "," + "full: " + true + "'" +
                            "class='fastAddProductItem' " +
                            "href='javascript: window.location.reload(true)'>"
                            + prod.name + "</a></div>")
                    })
                }
            }
        });
        return false;
    });

});

