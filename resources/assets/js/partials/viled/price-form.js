$(document).ready(function () {
    validateIt('get-price_form');
    maskForm('get-price__phone');
});
function validateIt(formId) {

    document.getElementById(formId).onsubmit = function () {
        return false;
    };
    if (typeof FormValidator === 'undefined') {
        var FormValidator = require('./../validate')
    }
    var validator = require('./../verifier');
    new FormValidator(formId, [
        {
            name: 'phone',
            display: 'Номер телефона',
            rules: 'required|regExp:\\+\\d\\(\\d{3}\\)\\d{3}\\-\\d{2}-\\d{2}'
        }], validator(function (errors, e, validator) {
        var form = $('#' + formId);
        $('#' + formId + ' .submit').attr('disabled', 'disabled');
        var spin = $('#' + formId + ' .animate-spin');
        spin.show(20);
        form.request('onPostPriceForm', {
            beforeUpdate: function () {
                spin.hide(20);
            },
            update: {'partials/get-price_result': '#get-price_result'},
            complete: function () {
                setTimeout(function () {
                    $('#modal-get-price').modal('hide');
                }, 4000)
            }
        });
    }));
}
function maskForm(element) {
    if (typeof Inputmask === 'undefined') {
        require('jquery.inputmask');
    }
    Inputmask("+7(999)999-99-99").mask(document.getElementById(element));
}