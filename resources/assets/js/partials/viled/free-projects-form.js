$(document).ready(function () {
    validateIt('get-free-project_form');
    maskForm('get-project__phone');
});
function validateIt(formId) {

    if (typeof FormValidator === 'undefined') {
        var FormValidator = require('./../validate')
    }
    var validator = require('./../verifier');
    new FormValidator(formId, [
        {
            name: 'phone',
            display: 'Номер телефона',
            rules: 'required|regExp:\\+\\d\\(\\d{3}\\)\\d{3}\\-\\d{2}-\\d{2}'
        }], validator(function (errors, e, validator) {
        $('#' + formId + ' .submit').attr('disabled', 'disabled');
        var spin = $('#' + formId + ' .animate-spin');
        spin.show(20);
        if ($('a[href="#inside"]').parent().hasClass('active')) {
            $('#get-project__type').val('inside');
        }
        else {
            $('#get-project__type').val('outside');
        }

    }));
}
function maskForm(element) {
    if (typeof Inputmask === 'undefined') {
        require('jquery.inputmask');
    }
    Inputmask("+7(999)999-99-99").mask(document.getElementById(element));
}