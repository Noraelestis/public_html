module.exports = function (callback) {

    function getContainer(input, wrapSelector) {
        var container = input.parentNode;
        var counter = 0;
        do {
            container = input.parentNode;
            counter++;
        }
        while (counter < 6 && container.parentNode.getAttribute('class') === wrapSelector);

        return container;
    }

    return function (errors, evt, validator) {
        if (errors.length > 0) {
            var config = {
                wrapSelector: "form-group",
                className: "help-block",
                errorClass: "has-error",
                successClass: "has-success"
            };
            var boxes = document.getElementsByClassName(config.className),
                wrappers = document.getElementsByClassName(config.wrapSelector + " " + config.errorClass);
            for (var i = 0, len = boxes.length; i < len; i++) {
                boxes[i].innerHTML = "";
                if (wrappers[i] !== undefined) {
                    wrappers[i].classList.remove(config.errorClass);
                }
            }
            var errorLength;
            for (i = 0, errorLength = errors.length; i < errorLength; i++) {
                var errorBox;
                var input = errors[i].element;
                var container = getContainer(input, config.wrapSelector);

                if (container.getAttribute('class') === config.wrapSelector) {
                    container = input.parentNode;
                }
                container.classList.add(config.errorClass);
                if (container.lastElementChild.className === config.className) {
                    errorBox = container.lastElementChild;
                } else {
                    errorBox = document.createElement('span');
                    errorBox.className = config.className;
                }
                errorBox.innerHTML = '<strong>' + errors[i].message + '</strong>';
                container.appendChild(errorBox);
                input.addEventListener("blur", function () {
                    var val = this.value;
                    var field = validator.fields[this.name];
                    var container = getContainer(this, config.wrapSelector);
                    field.value = val;
                    var validate = validator._validateField(field);
                    if (validate === false) {
                        try {
                            container.classList.remove(config.errorClass);
                            container.getElementsByClassName('help-block')[0].remove();
                            if (field.value !== '') {
                                container.classList.add(config.successClass);
                            }
                        }
                        catch (e) {

                        }
                    }
                    else {
                        container.getElementsByClassName('help-block')[0].innerHTML = '<strong>' + validate + '</strong>';
                    }
                });
            }
        }
        else {
            callback(errors, evt, validator);
        }
    };
};