module.exports = new function () {

    var self = this;

    this._active = 1;

    this.selectors = {
        wrapper: '.card_slider',
        switchers: '.card_slider-slide__min',
        switcherActive: '.card_slider-slide__min-active',
        dataUrl: 'url',
        area: '.card_slider-slide',
        active: '.card_slider-active_slide'
    };

    this.change = function (element) {
        var url = element.data(self.selectors.dataUrl);
        $(self.selectors.area).html('<i class="icon-spin4 animate-spin">');
        var img = $('<a href="' + url + '" class="fancybox"><img src="' + url + '" alt=""></a>');
        img.load(function () {
            $(self.selectors.area).find('.icon-spin4').remove();
        });
        $(self.selectors.area).html(img);
        $(self.selectors.switchers).removeClass(self.selectors.switcherActive.replace('\.', ''));
        element.addClass(self.selectors.switcherActive.replace('\.', ''));
        $(self.selectors.active + ' a').fancybox();
    };

    this.init = function () {

        $(self.selectors.switchers).click(function () {
            if (self._active !== this) {
                self.change($(this));
                self._active = this;
            }
        });
        var slick = require('slick-carousel-browserify');
        slick($('.card_slider-slides'), {
            infinite: false,
            slidesToShow: 5,
            slidesToScroll: 4,
            vertical: true,
            verticalSwiping: true,
            touchMove: true
        });
    }
};