$('.menu-categories__list__link > .icon').click(function (e) {
    e.preventDefault();
    if($(this).hasClass('icon-plus')) {
        $(this).attr('class', 'icon icon-minus');
        $(this).parent().next('.menu-categories__list-collapsed').removeClass('menu-categories__list-collapsed');
    }
    else {
        $(this).attr('class', 'icon icon-plus');
        $(this).parent().next('.menu-categories__list').addClass('menu-categories__list-collapsed');
    }
});