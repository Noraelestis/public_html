module.exports = function (selectors) {
    $(selectors).change(function () {
        var loc = window.location.href;
        var per = 'per_page=' + $(this).val();
        var reg = /(\?|&)per_page=[\d]+/;
        loc = loc.replace(/(\?|&)page=[\d]+/, '');

        if (loc.search(/\?/) == -1) {
            per = '?' + per;
            loc = loc.replace(new RegExp(reg), '');
            loc = loc + per;
        }
        else {
            loc = loc.replace(new RegExp(reg), '');
            if (loc.search(/\?/) == -1) {
                loc = loc + '?' + per;
            }
            else {
                loc = loc + '&' + per;
            }
        }

        window.location.href = loc;
    });
};