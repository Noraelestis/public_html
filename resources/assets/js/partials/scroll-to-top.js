module.exports = (function () {

    $(document).ready(function () {

        var savePosition = 0;

        if($(window).scrollTop() > 100) {
            $('.scrollToTop').fadeIn();
        }

        $(window).scroll(function () {
            if ($(this).scrollTop() > 100) {
                if($(this).scrollTop() > savePosition) {
                    savePosition = 0;
                    $('.scrollToTop').children('.icon-down-open').attr('class', 'icon-up-open');
                }
                $('.scrollToTop').fadeIn();
            } else if (savePosition === 0) {
                $('.scrollToTop').fadeOut();
            }
        });

        //Click event to scroll to top
        $('.scrollToTop').click(function () {
            var y = 0;
            if (savePosition !== 0) {
                $(this).children('.icon-down-open').attr('class', 'icon-up-open');
                y = savePosition;
                savePosition = 0;
            }
            else {
                savePosition = window.pageYOffset ? window.pageYOffset : document.body.scrollTop;
                $(this).children('.icon-up-open').attr('class', 'icon-down-open');
                y = 0;
            }
            $('html, body').animate({scrollTop: y}, 800);
            return false;
        });

    });

})();