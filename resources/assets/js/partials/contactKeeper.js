module.exports = function (config) {

    var ContactKeeper = new function () {

        /**
         * Local this
         * @type {ContactKeeper}
         */
        var self = this;

        /**
         * How long to wait before sending
         * @type {number}
         */
        this.delay = 2000;

        /**
         * @type {number}
         * @private
         */
        this._timer = 0;

        /**
         * @type {Array}
         * @private
         */
        this._elements = [];

        /**
         * Set custom handler
         * @type {boolean}
         */
        this.handler = false;

        /**
         * Init module
         * @param elements
         */
        this.init = function (elements) {
            this._elements = elements;
            var callback;

            if (!this.handler) {
                callback = this.setCookie;
            }
            else {
                callback = this.handler;
            }

            for (var index in elements) {
                this.deferred(elements[index], callback);
            }

        };

        /**
         * Save data to cookie
         */
        this.setCookie = function () {
            try {
                var cookie = require("js.cookie");
                var data = {};
                self._elements.forEach(function (item) {
                    data[item] = $(item).val();
                });
                var old = cookie.get('ContactKeeper');
                if (old !== null) {
                    old = JSON.parse(old);
                }
                else{
                    old = {};
                }
                old[self._elements.join('_')] = data;
                cookie.set('ContactKeeper', JSON.stringify(old));
            }
            catch (e){
            }
        };

        /**
         * Keyup listner for elements
         * @param element
         * @param callback
         */
        this.deferred = function (element, callback) {
            $(element).on('keyup change', function (e) {
                self.onChange(this);
                if (self._timer === 0) {
                    watcher(callback, this);
                }
                self._timer = self.delay;
            });
        };

        /**
         * Call on change val elem
         * @param elements
         */
        this.onChange = function (elements) {
        };

        /**
         * Watcher for this.delay
         * @param callback
         * @param elem
         */
        var watcher = function (callback, elem) {
            setTimeout(function () {
                if (self._timer === 0) {
                    callback(elem);
                }
                else {
                    self._timer = 0;
                    watcher(callback, elem);
                }
            }, self.delay);

        };

    };

    if (config !== undefined) {
        var prop;
        for (prop in config) {
            ContactKeeper[prop] = config[prop];
        }
    }

    return ContactKeeper;
};


