module.exports = new function (options) {

    var self = this;

    this.actions = {
        getList: {
            bind: $('.geoapi__action--get-list'),
            element: $('.geoapi__cities-list__wrapper'),
            closeButton: $('.geoapi__cities-list__close'),
            init: function () {
                var element = this.element;
                this.bind.click(function (e) {
                    element.toggleClass('hidden');
                    if (element.hasClass('hidden')) {
                        $('.modal-backdrop.fade').remove();
                    }
                    else {
                        $('body').append('<div class="modal-backdrop fade in geoapi__cities-list__backdrop"></div>');
                        $('.geoapi__cities-list__backdrop').click(function () {
                            element.addClass('hidden');
                            $('.modal-backdrop.fade').remove();
                        });
                    }
                });
                this.closeButton.click(function (e) {
                    element.toggleClass('hidden');
                    $('.modal-backdrop.fade').remove();
                });
            }
        },
        searchList: {
            bind: $('#geoapi__cities-search-form__input'),
            elements: $('.geoapi__cities-list_item a'),
            init: function () {
                var elements = this.elements;
                this.bind.keyup(function () {
                    var input = this;
                    elements.each(function () {
                        console.log(this.innerHTML.toLowerCase().indexOf(input.value.toLowerCase()));
                        if (this.innerHTML.toLowerCase().indexOf(input.value.toLowerCase()) === 0) {
                            $(this).parent().show();
                        }
                        else {
                            $(this).parent().hide();
                        }
                    });
                });
            }
        }
    };

    this.init = function () {
        for (var action in self.actions) {
            self.actions[action].init();
        }
    }
};