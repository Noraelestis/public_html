module.exports = new function () {

    var self = this;

    /**
     *
     * @type {Array}
     */
    this.slider = [];

    this.sliderDefaults = {
        style: 'btn-default',
        iconBase: '',
        tickIcon: 'icon-ok'
    };

    this.selects = function () {
        require('bootstrap-select/js/bootstrap-select');
        $('.selectpicker').selectpicker(self.sliderDefaults);
    };

    this.sliderInit = function () {
        var Slider = require("bootstrap-slider");
        if ($('.portal-filter__slider__wrapper').length > 0) {
            $(".portal-filter__slider-slide").each(function () {
                var input = {
                    start: $('.portal-filter__form-' + $(this).data('target') + '-from'),
                    end: $('.portal-filter__form-' + $(this).data('target') + '-to')
                };
                var slider = new Slider(this, {
                    handle: 'square',
                    value: [parseInt(input.start.val()), parseInt(input.end.val())],
                    min: $(this).data('min'),
                    max: $(this).data('max')
                });
                self._associate(this, slider, input);
                self.slider.push(slider);
            });
        }
    };

    this._associate = function (context, slider, input) {
        if(typeof input === 'undefined') {
            input = {
                start: $('.portal-filter__form-' + $(context).data('target') + '-from'),
                end: $('.portal-filter__form-' + $(context).data('target') + '-to')
            };
        }
        slider.on('change', function (values) {
            input.start.val(values.newValue[0]);
            input.end.val(values.newValue[1]);
        });
        input.start.on('change', function () {
            slider._state.value[0] = parseInt($(this).val());
            slider.refresh();
        });
        input.end.on('change', function () {
            slider._state.value[1] = parseInt($(this).val());
            slider.refresh();
        });
    };

    this.reset = function () {
        $('.portal-filter__form-reset').click(function () {

            var url = window.location.href;
            url = url.replace(/filter_[^&]*[&]*/g, '');
            url = url.replace(/\?$/, '');
            window.location.href = url;
        });
    };

    this.init = function () {
        self.selects();
        self.sliderInit();
        self.reset();
    }
};