$(document).ready(function () {
    $('#search-form__button').click(function () {
        submitForm();
    });
    $("#search-form__input").keyup(function(event){
        if(event.keyCode == 13){
            submitForm();
        }
    });
    function submitForm(){
        var query = $('#search-form__input').val();
        if(query !== ""){
            window.location = "/product/code/"+query;
        }
    }
});