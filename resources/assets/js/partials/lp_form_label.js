$(document).ready(function () {

    $('.lp__form__label + .form-control').each(function () {
        if ($(this).val() !== '') {
            $(this).siblings(".lp__form__label").addClass('shifted__label');
        }
    });

    $('.lp__form__label').click(function () {
        $(this).siblings(".form-control").focus();
    });

    $('.lp__form__input').on('focus', function () {
        var label = $(this).siblings(".lp__form__label");
        label.addClass('shifted__label');
        $(this).addClass('lp__form__onfocus');
    });

    $('.lp__form__input').on('blur', function () {
        if ($(this).val() === "") {
            var label = $(this).siblings(".lp__form__label");
            label.removeClass('shifted__label');
        }
        $(this).removeClass('lp__form__onfocus');
    });

    $('.lp__form__input').on('change', function () {
        var input = $(this);
        if($(this).val() !== '') {
            input.trigger('focus');
        }
        else {
            input.trigger('blur');
        }
    });
});
