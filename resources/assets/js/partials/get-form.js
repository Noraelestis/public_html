$(document).ready(function () {
    feedbackForm('.get-price-form', 'Заказать прайс', 'price');
    feedbackForm('.get-bill-form', 'Получить счет', 'bill');
    feedbackForm('.get-callback-form-engineer', 'Заказ обратного звонка', 'call-engineer');
    feedbackForm('.get-callback-form-manager', 'Заказ обратного звонка', 'call-manager');
    feedbackForm('.payment-order', 'Заказать бесплатный расчет', 'payment-order');

});
function feedbackForm(activator, title, type) {
    $(activator).click(function (e) {
        e.preventDefault();
        $('.callback-form_description').removeClass('hidden');
        $('#feedback-form__result').children('.alert').remove();
        $('#callback-form_title').html(title);
        $('input[name="type"]').val(type);
        $('#callback-form-modal').modal();
        $('.modal-dialog').click(function () {
            $('#callback-form-modal').modal("hide")
        });
        $('form[name="feedback-form"]').click(function (e) {
            e.stopPropagation()
        });
        vadilateIt();
        maskForm();
    });
}
function vadilateIt() {
    document.getElementById('feedback-form').onsubmit = function () {
        return false;
    };
    var validator = require('./verifier');
    new FormValidator('feedback-form', [{
        name: 'email',
        display: 'Ваша почта',
        rules: 'required|email'
    }, {
        name: 'phone',
        display: 'Номер телефона',
        rules: 'required'
    }], validator(function () {
        $('#squaresWaveG').removeClass('hidden');
        $('#feedback-form').request('onSubmitModalForm', {
            beforeUpdate: function () {
                $('#squaresWaveG').addClass('hidden');
            },
            update: {'partials/feedback_result': '#feedback-form__result'}
        });
    }));
}
function maskForm() {
    Inputmask("+7-(999)-999-99-99").mask(document.getElementById("feedback-form_phone"));
}