var debounce = require('debounce');
var serialize = require('./../../libs/serialize');

var svFilter = {
    state: {},
    put: function (values) {
        for (var key in values) {
            if (values.hasOwnProperty(key)) {
                this.state[key] = values[key];
            }
        }
    },
    debounceSubmit: function () {
        debounce(this.submit, 500)();
    },
    submit: function () {
        window.location.href = window.location.origin + window.location.pathname + '?' + serialize(svFilter.state);
    }
};

module.exports = svFilter;