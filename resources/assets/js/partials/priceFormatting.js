function priceFormatting(price) {
    return price.toString().replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
}

if (typeof module.exports !== 'undefined') {
    module.exports = priceFormatting;
}
