module.exports = (function () {
    $(document).ready(function () {
        var header = $('#header');
        var state = 0;
        $(window).on('scroll', function () {
            if ($('body').scrollTop() > 0 && state === 0) {
                $('#header').addClass('header-fixed');
                state = 1;
            } else if ($('body').scrollTop() <= 0 && state === 1) {
                $('#header').removeClass('header-fixed');
                state = 0;
            }
        });
        //fix bootstrap modal
        $('.modal').on('shown.bs.modal', function () {
            $('#header').removeClass('header-fixed');
        });
        $('.modal').on('hide.bs.modal', function () {
            if ($('body').scrollTop() > 0) {
                $('#header').addClass('header-fixed');
            }
        });
    });
})();