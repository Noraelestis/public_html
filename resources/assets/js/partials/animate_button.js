$(document).ready(function(){
    if(document.getElementsByClassName("checkout-form").length!==0){
        $('.checkout-form').addClass('animated bounceIn');
        setTimeout(clearAnimation, 750);
        var waypoint = new Waypoint({
            element: document.getElementsByClassName('checkout-form')[0],
            handler: function(direction) {
                $(this.element).addClass('animated bounceIn');
                setTimeout(clearAnimation, 750);
            }
        })
    }
});
function clearAnimation(){
    $('.checkout-form').removeClass('animated bounceIn');
}