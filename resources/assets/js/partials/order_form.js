module.exports = (function () {

    return new function () {

        var self = this;

        var contactKeeper = require('./contactKeeper');

        this.priceField = $('.order-form__full-price__price, .sv-shopping-cart__price, .sv-products__price');

        this.currencyView = 'руб.';

        this.submitButton = $('#order-form__submit');

        this.errorPhrase = 'Извините на сервере произошла ошибка, свяжитесь с нами по email или попробуйте позднее';

        this.openCartModal = function () {
            $('#order-form-modal').modal();
        };

        this._requestData = false;

        /**
         * Calc price result
         * @returns {number}
         */
        this.calcPrice = function () {
            var full = 0;
            var count = 1;
            $('.order-form__wrapper .order-form__product-price').each(function () {
                count = parseInt($(this).parents('.order-form__products-item').find('.order-form__product-count').find('.order-form__quantity').val());
                var discount_from = $(this).data('discount_from');
                if (discount_from !== '' && count >= discount_from) {
                    full = full + ($(this).data('discount_price') * count)
                }
                else {
                    full = full + (parseInt($(this).data('price')) * count);
                }
            });
            if (isFinite(full)) {
                return self.priceFormatting(full);
            }
            return 0;
        };

        /**
         * @param {integer} price
         * @returns {*}
         */
        this.priceFormatting = function (price) {
            return price.toString().replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');
        };

        /**
         * @param val
         */
        this.updateFullPrice = function (val) {
            self.priceField.html(val + ' ' + self.currencyView);
        };

        /**
         * @param name
         * @param data
         * @returns {string}
         */
        this.renderTemplate = function (name, data) {
            var template = document.getElementById(name).innerHTML;

            for (var property in data) {
                if (data.hasOwnProperty(property)) {
                    var search = new RegExp('{' + property + '}', 'g');
                    template = template.replace(search, data[property]);
                }
            }
            return template;
        };

        /**
         * @param string
         * @returns {string}
         */
        this.encodeString = function (string) {
            return '{' + string.replace(/\"/g, '\\"').replace(/\'/g, '"') + '}';
        };

        /**
         * Set data
         * @type {any}
         */
        this.requestData = function (button) {
            if (typeof button === 'undefined') {
                button = $('.checkout-form');
            }
            self._requestData = JSON.parse(self.encodeString(button.attr('data-request-data')));
            var count;
            count = button.attr('data-count');
            if (typeof count === 'undefined') {
                count = 1;
            }
            self._requestData.count = count;
            return self._requestData;
        };

        /**
         * Count for shopping cart
         */
        this.recountToCart = function () {
            var count = $('.order-form__wrapper  .order-form__products-item').length;
            $('.sv-shopping-cart__count, .sv-products__count').html(count);
            if (count === 0) {
                $('.order-form__full-price').hide(50);
                $('.order-form__full-price').after(self.renderTemplate('order-form__empty-cart-tpl'));
                $('.sv-shopping-cart__count').addClass('sv-shopping-cart__count-empty');
                $('.sv-shopping-cart__products').hide();
                self.submitButton.prop('disabled', true);
            }
            else {
                $('.order-form__full-price').show(50);
                $('.order-form__empty-cart').remove();
                $('.sv-shopping-cart__count').removeClass('sv-shopping-cart__count-empty');
                $('.sv-shopping-cart__products').removeAttr('style');
                self.submitButton.prop('disabled', false);
            }
        };

        /**
         * Add product to order form
         */
        this.addToForm = function (button) {

            var discount = '';

            var requestData = self.requestData(button);

            if (requestData.discount_price !== '' && requestData.discount_from !== '') {

                discount = self.renderTemplate('order-form__discount-tpl', {
                    id: requestData.id,
                    discount_price: requestData.discount_price,
                    discount_from: requestData.discount_from,
                    saving_price: self.priceFormatting(((requestData.price * requestData.discount_from) - (requestData.discount_price * requestData.discount_from)))
                });
            } else {
                discount = self.priceFormatting(requestData.price);
            }

            var html = self.renderTemplate('order-form__product-tpl', {
                id: requestData.id,
                name: requestData.name,
                slug: requestData.slug,
                price: requestData.price,
                price_view: self.priceFormatting(requestData.price),
                discount_price: requestData.discount_price,
                discount_from: requestData.discount_from,
                img: requestData.image,
                discount: discount,
                count: requestData.count
            });

            var lastItem = {
                small: $('.sv-shopping-cart__products > .order-form__products-item'),
                full: $('.order-form__produts-area > .order-form__products-item')
            };

            if (lastItem.full.length === 0) {
                $('.order-form__produts-area').prepend(html);
                $('.sv-shopping-cart__products').prepend(html);
            }
            else {
                $(lastItem.small[lastItem.small.length - 1]).after(html);
                $(lastItem.full[lastItem.full.length - 1]).after(html);
            }

            self.updateFullPrice(self.calcPrice());

            self.bindDeleteButton('.order-form__product-delete[data-id="' + requestData.id + '"]');

            var elementAddEvent = new CustomEvent("order-form__add-product", {
                detail: {
                    id: requestData.id
                }
            });
            document.dispatchEvent(elementAddEvent);

            self.bindPlusMinusButton(['.order-form__quantity[data-id="' + requestData.id + '"]']);
            self.bindDiscountButton('.order-form__product-discount[data-id="' + requestData.id + '"]');
            self.bindShowHideDiscount('.btn-number[data-id="' + requestData.id + '"]');

            self.recountToCart();

            if (parseInt(requestData.discount_from) === 1) {
                self.togglesShowHideDiscount
                    .plus($('.order-form__product-discount[data-id="' + requestData.id + '"]'),
                        $('.order-form__products-item[data-id="' + requestData.id + '"]').find('.order-form__product-price'));
            }
        };

        /**
         * Bind delete button logic
         * @param selector
         */
        this.bindDeleteButton = function (selector) {
            $(selector).click(function (e) {
                e.preventDefault();
                var item = $(this).parents('.order-form__products-item').first();
                item.hide(100);
                $('.checkout-form').request('onDeleteProductToForm', {
                    data: {'id': item.data('id')},
                    success: function () {
                        $('.order-form__products-item[data-id="' + item.data('id') + '"]').remove();
                        self.updateFullPrice(self.calcPrice());
                        self.recountToCart();
                    },
                    error: function () {
                        item.show(10);
                    }
                });
            });
        };

        /**
         * Bind logic for plus minus buttons
         * @param selectors
         */
        this.bindPlusMinusButton = function (selectors) {

            var update = contactKeeper({
                onChange: function () {
                    self.updateFullPrice(self.calcPrice());
                },
                handler: self.onChangeCountProduct,
                delay: 400
            });
            if (selectors === undefined) {
                update.init(['.order-form__quantity']);
            }
            else {
                update.init(selectors);
            }
        };

        /**
         * Handler for change count product
         * @param e
         */
        this.onChangeCountProduct = function (e) {
            var input = $(e);
            var id = input.data('id');
            $('form').request('onChangeCountProduct', {
                data: {'id': id, 'count': input.val()}
            });
        };

        /**
         * Bind to discountButton
         * @param selector
         */
        this.bindDiscountButton = function (selector) {
            if (selector === undefined) {
                selector = $('.order-form__product-discount');
            }
            else {
                selector = $(selector);
            }
            selector.click(function () {
                var input = $('input[name="quantity[' + $(this).data('id') + ']"]');
                $('.btn-minus[data-id="' + $(this).data('id') + '"]').removeAttr('disabled');
                input.val($(this).data('count'));
                input.trigger('change');
                self.onChangeCountProduct(input);
                $(this).hide();
                var price = input.parents('.order-form__product-text').children('.order-form__product-price');
                self.togglesShowHideDiscount.plus($(this), price);
                self.bindShowHideDiscount('.btn-number[data-id="' + $(this).data('id') + '"]');
            });
        };

        /**
         * Bind show hide logic
         * @param bind
         */
        this.bindShowHideDiscount = function (bind) {
            var btn = {
                minus: 'btn-minus',
                plus: 'btn-plus'
            };
            if (bind === undefined) {
                bind = '.btn-number';
            }
            $(bind).click(function () {
                var id = $(this).data('id');
                var selector = $('.order-form__product-discount[data-id="' + id + '"]');
                var input = $('.order-form__quantity[data-id="' + id + '"]');
                var price = input.parents('.order-form__product-text').children('.order-form__product-price');
                if (price.data('discount_from') !== '') {
                    if ($(this).hasClass(btn.minus)) {
                        if (input.val() < price.data('discount_from')) {
                            self.togglesShowHideDiscount.minus(selector, price);
                        }
                    }
                    else {
                        if (input.val() >= price.data('discount_from')) {
                            self.togglesShowHideDiscount.plus(selector, price);
                        }
                    }
                }
            });
        };

        this.togglesShowHideDiscount = {
            minus: function (selector, price) {
                selector.show(500);
                if (price.find('.order-form__product-discount-price__container').length > 0) {
                    price.removeClass('order-form__product-discount-price__discounted');
                    price.find('.order-form__product-discount-price__container').remove();
                }
            },
            plus: function (selector, price) {
                selector.hide(500);
                if (price.find('.order-form__product-discount-price__container').length === 0) {
                    price.addClass('order-form__product-discount-price__discounted');
                    price.append('<span class="order-form__product-discount-price__container">' + self.priceFormatting(price.data('discount_price')) + ' ' + self.currencyView + '</span>');
                }
            }
        };

        this.externalCounter = function () {
            $('.order-form__quantity-mass').change(function () {
                $('.checkout-form[data-id="' + $(this).data('id') + '"]').attr('data-count', $(this).val());
            });
        };

        /**
         * Handler for for error
         * @returns {jQuery|HTMLElement|*}
         */
        this.errorHandler = function () {
            self.submitButton.removeClass('btn-warning');
            self.submitButton.addClass('btn-danger');
            self.submitButton.parent('.form-group').addClass('has-error');
            self.submitButton.after('<span class="help-block text-center"><strong>' + self.errorPhrase + '</strong></span>');
            return self.submitButton;
        };

        this.bindResetCart = function () {
            $('.order-form__reset').click(function () {
                $('#order-form').request('onRestOrderForm', {
                    success: function () {
                        $('.order-form__products-item').remove();
                        self.recountToCart();
                        self.updateFullPrice(self.calcPrice());
                    }
                });
            });
        };

        /**
         * send form
         */
        this.sendForm = function () {
            document.getElementById('order-form').onsubmit = function () {
                return false;
            };
            var validator = require('./verifier');

            new FormValidator('order-form', [
                {
                    name: 'phone',
                    display: 'Номер телефона',
                    rules: 'required|regExp:\\+\\d\-\\(\\d{3}\\)\\-\\d{3}\\-\\d{2}-\\d{2}'
                },
                {
                    name: 'email',
                    display: 'Email',
                    rules: 'email'
                },
                {
                    name: 'accept',
                    display: 'соглашение',
                    message: '{"required": "Для отправки формы вы должны согласиться с условиями."}',
                    rules: 'required'
                }], validator(function () {
                $('#order-form__submit .animate-spin').show(20);
                self.submitButton.attr('disabled', 'disabled');
                $('#order-form').request('onSendOrderForm', {
                    beforeUpdate: function (data, status, jqXHR) {
                        if (data.status !== 'error') {
                            dataLayer.push({
                                'event': 'zakaz',
                                'eventCategory': 'zakaz_category',
                                'eventAction': 'zakaz',
                                'eventLabel': $('#phone').val()
                            });
                            $('#order-form__submit .animate-spin').hide(20);
                            $('.order-form__products-item').remove();
                            self.recountToCart();
                            self.updateFullPrice(self.calcPrice());
                            $('#order_form-result__container').html('Спасибо за Ваш заказ!');
                        }
                        else {
                            var button = self.errorHandler();
                            button.parent('.form-group').addClass('has-error');
                            jqXHR.responseJSON["partials/order_form_result"] = button.parent('.form-group').html();
                        }
                    },
                    update: {'partials/order_form_result': '#order_form-result__container'},
                    error: function () {
                        self.errorHandler();
                    }
                });
            }));
        };

        this.init = function (options) {
            //init
            if (typeof options !== 'undefined') {
                for (var option in options) {
                    if (options.hasOwnProperty(option)) {
                        this[option] = options[option];
                    }
                }
            }
            var inputNumber = require('./../partials/button_number');
            $(document).on('order-form__add-product', function (e) {
                var id = e.detail.id;
                inputNumber({
                    btn: '.btn-number[data-field="quantity[' + id + ']"]',
                    input: '.input-number[name="quantity[' + id + ']"]'
                });
            });
            self.externalCounter();
            self.updateFullPrice(self.calcPrice());
            self.bindDeleteButton('.order-form__product-delete');
            self.bindPlusMinusButton();
            self.bindDiscountButton();
            self.bindShowHideDiscount();
            self.bindResetCart();
            contactKeeper({delay: 600}).init(['#name', '#email', '#phone']);
            var cookie = require("js.cookie");
            var contacts = cookie.get('ContactKeeper');
            if (contacts !== null) {
                try {
                    contacts = JSON.parse(contacts);
                    if (contacts['#name_#email_#phone'] !== undefined) {
                        $('#name').val(contacts['#name_#email_#phone']['#name']);
                        $('#email').val(contacts['#name_#email_#phone']['#email']);
                        $('#phone').val(contacts['#name_#email_#phone']['#phone']);
                    }
                }
                catch (e) {
                }
            }

            if (typeof Inputmask !== 'undefined') {
                Inputmask("+7-(999)-999-99-99").mask(document.getElementById("phone"));
            }

            $('.md-checkbox--fake').on('click', function () {
                var input = $($(this).prev('input')),
                    wrapper = input.parent();
                if (!input.prop('checked')) {
                    wrapper.children('.help-block').remove();
                }
                input.trigger('blur');
            });

            /**
             * Show form
             */
            $('.sv-shopping-cart__price, .sv-shopping-cart__icon, .sv-open-full-cart').click(function (e) {
                e.preventDefault();
                self.openCartModal();
            });

            /**
             * Activate form
             */
            $('.checkout-form').click(function (e) {
                e.preventDefault();
                if ($('.order-form__produts-area > .order-form__products-item[data-id="' + self.requestData($(this)).id + '"]').length === 0) {
                    self.addToForm($(this));
                }
                $(this).request('onAddProductToForm', {});
                $('.order-form__full-price').show();
                self.openCartModal();
            });


            this.sendForm();

            $('#order-form-modal').on('shown.bs.modal', function () {
                $('#name').trigger('change');
                $('#email').trigger('change');
                $('#phone').trigger('change');
            });

            self.recountToCart();

            return this;
        };
    };
})();