$(document).ready(function () {

    const printButton = $('#createProductPDF');
    const opt = {
        margin: 0.1,
        image: {type: 'jpeg', quality: 0.98},
        html2canvas: {scale: 2},
        jsPDF: {unit: 'in', format: 'letter', orientation: 'portrait'}
    };
    printButton.on('click', function (e) {
        e.preventDefault();
        var contentTarget = $('#thml2pdf').html();
        const productName = $('.product-title').text().replace(/\s/g, '-');
        let pdfContent = $('.pdfContent');
        pdfContent.append($('#header').html());
        pdfContent.append(contentTarget);
        let images = $('.pdfContent [data-resource-image]');
        $('.pdfContent .lSSlideOuter').html(images[1]);

        $('.pdfContent .lSSlideOuter img').css('width', '400px');
        $('.pdfContent .forInlineInPDF').css('max-width', '600px');
        $('.pdfContent .forInlineInPDF .price ').css({'margin-right':'20px', 'width':'400px', 'font-size':'18px'});
        $('.pdfContent .product-info').removeClass('col-md-5').css('margin-left', '10%');
        $('.pdfContent .product-page').removeClass('col-md-7').css('margin-left', '10px');
        $('.pdfContent .tabs-block').prepend('<br>', '<br>', '<br>', '<br>', '<br>');


        $('.pdfContent #characteristics-1').removeClass(['active', 'show']);
        $('.pdfContent .card-price').removeClass('d-md-block');
        $('.pdfContent .noPager').removeClass('lSSlideOuter');
        $('.pdfContent .products__opportunities').removeClass('col-lg-7 mt-0').addClass('row');
        $('.pdfContent .products__block').removeClass('row align-items-center').addClass('col d-flex justify-content-center');
        $('.pdfContent #characteristics-2').addClass(['active', 'show']);


        // Не нужные элементы в PDF
        $('.pdfContent .fill-application').remove();
        $('.pdfContent .download__list').remove();
        $('.pdfContent .card-body__col_right').remove();
        $('.pdfContent #myTab').remove();
        // $('.pdfContent .save-and-print__block').remove();
        // $('.pdfContent #characteristics').remove();
        $('.pdfContent .question').remove();
        $('.pdfContent .question__icon').remove();
        $('.pdfContent .btn-consul').remove();
        $('.pdfContent .add-to-cart').remove();
        $('.pdfContent .breadcrumb').remove();
        $('.pdfContent iframe').remove();
        $('.pdfContent .main-header').remove();
        $('.pdfContent #sub-catalog').remove();
        $('.pdfContent .product-analogues').remove();
        $('.pdfContent .withThisProduct').remove();
        $('.pdfContent .input_number').remove();
        $('.pdfContent .credit-block').remove();
        $('.pdfContent .for-order').remove();
        $('.pdfContent .for-stock').remove();
        $('.pdfContent .sp-buttons').remove();
        $('.pdfContent .modal').remove();
        $('.pdfContent .also-need').remove();

        html2pdf()
            .from("<div class='pdfContent'>" + pdfContent.html() + "</div>")
            .set(opt)
            .save(`${productName}.pdf`);

        // $('body').html("<div class='pdfContent' style='display: flex'>" + pdfContent.html() + "</div>");
        // console.log('html');
    })
});
