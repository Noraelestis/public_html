if (typeof window.jQuery === 'undefined') {
    window.jQuery = $;
}
$(document).ready(function () {

    var screenTop = $(document).scrollTop();

    if (screenTop >= 250) {
        $('#scrlTop').css('display', 'block');
    } else {
        $('#scrlBotm').css('display', 'block');
    }

    $(window).scroll(function () {
        if ($(window).scrollTop() > 250) {
            $('#scrlTop').fadeIn(500);
            $('#scrlBotm').fadeOut(500);
        } else {
            $('#scrlTop').fadeOut(500);
            $('#scrlBotm').fadeIn(500);
        }
    });


    $('#scrlBotm').click(function () {
        $('html, body').animate({
                scrollTop: $(document).height()
            },
            1500);

        return false;

    });

    $('#scrlTop').click(function () {
        $('html, body').animate({
                scrollTop: '0px'
            },
            1500);

        return false;

    });

    if ($('#vertical').length) {

        $('#vertical').lightSlider({
            gallery: true,
            item: 1,
            loop: true,
            adaptiveHeight: true,
            vThumbWidth: 110,
            thumbItem: 6,
            thumbMargin: 30,
            slideMargin: 0,
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        item: 1,
                        slideMove: 1,
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        item: 1,
                        slideMove: 1,
                    }
                }
            ]
        });

    }

    $('[data-toggle="tooltip"]').tooltip();

    $('.owl-carousel').owlCarousel({
        loop: true,
        margin: 0,
        nav: false,
        items: 1,
        dots: false,
        autoplay: true
    });

    $('#go-page').click(function () {
        $('#cart-hiden-block').removeClass('visible').addClass('hiden');
        $('#cart-hiden-block').animate({
            right: "-360px"
        }, 400);
    });
    var $win = $(window); // or $box parent container
    var $box = $("#cart");
    var $box_hiden = $('#cart-hiden-block');

    $win.on("click.Bst", function (event) {
        if ($box.has(event.target).length == 0 && !$box.is(event.target) && $box_hiden.has(event.target).length == 0 && !$box_hiden.is(event.target)) {
            $('#cart-hiden-block').removeClass('visible').addClass('hiden');
            $('#cart-hiden-block').animate({
                right: "-360px"
            }, 400);
        } else {

        }
    });


    $('.c-filter-slider').each(function() {
        var currentSlider = noUiSlider.create(this, {
            start: [this.dataset.from, this.dataset.to],
            connect: true,
            range: {
                'min': parseInt(this.dataset.min),
                'max': parseInt(this.dataset.max)
            },
            pips: {
                mode: 'positions',
                values: [0, 50, 100],
                density: 3
            }
        });

        var target = {
            min: $('#' + this.dataset.targetMin),
            max: $('#' + this.dataset.targetMax)
        };

        var cb = function () {
            var input = $(this);
            var val = [input.val(), null];
            if (input.data('svfilter-type') === 'max') {
                val = [null, input.val()];
            }
            currentSlider.set(val);
        };
        target.min.change(cb);
        target.max.change(cb);

        this.noUiSlider.on('change', function (val) {
            var target = {
                min: $('#' + this.target.dataset.targetMin),
                max: $('#' + this.target.dataset.targetMax)
            };
            val = [Math.round(val[0]), Math.round(val[1])];
            filterPut(val, this.target);
            target.min.val(val[0]);
            target.max.val(val[1]);
        });
    });

    $('#block-filter').on('ready', function () {
        var sliders = document.getElementsByClassName('c-filter-slider');
        for (var i = 0; i < sliders.length; i++) {
            filterPut(sliders[i].noUiSlider.get(), sliders[i]);
        }
    });

    $('input[name="filter_manufacturer[]"]').click(function () {
        var values = {};
        $('input[name="filter_manufacturer[]"]:checked').each(function (i) {
            values['filter_manufacturer[' + i + ']'] = $(this).val();
        });
        window.svFilter.put(values);
    });

    function filterPut(val, slider) {
        var values = {};
        values[slider.dataset.name + '_from'] = val[0];
        values[slider.dataset.name + '_to'] = val[1];
        window.svFilter.put(values);
    }

});