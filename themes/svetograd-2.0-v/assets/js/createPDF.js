$(document).ready(function () {

    const printButton = $('#createProductPDF');
    const opt = {
        margin: 0.1,
        image: {type: 'jpeg', quality: 0.98},
        html2canvas: {scale: 2},
        jsPDF: {unit: 'in', format: 'letter', orientation: 'portrait'}
    };
    printButton.on('click', function (e) {
        e.preventDefault();
        var contentTarget = $('#thml2pdf').html();
        const productName = $('.product-title').text().replace(/\s/g, '-');
        let pdfContent = $('.pdfContent');
        pdfContent.append($('#header').html());
        pdfContent.append(contentTarget);
        let images = $('.pdfContent [data-resource-image]');
        $('.pdfContent .lSSlideOuter').html(images[1]);

        $('.pdfContent .lSSlideOuter img').css('width', '600');
        $('.pdfContent .forInlineInPDF').css('max-width', '600px');
        $('.pdfContent .forInlineInPDF .price ').css('margin-right', '20px');
        $('.pdfContent .forInlineInPDF .price ').css('width', '400px');
        $('.pdfContent .forInlineInPDF .price ').css('font-size', '18px');


        $('.pdfContent #characteristics-1').removeClass('active');
        $('.pdfContent #characteristics-1').removeClass('show');
        $('.pdfContent #characteristics-2').addClass('active');
        $('.pdfContent #characteristics-2').addClass('show');


        // Не нужные элементы в PDF
        $('.pdfContent .fill-application').remove();
        $('.pdfContent .download__list').remove();
        $('.pdfContent .card-body__col_right').remove();
        $('.pdfContent #myTab').remove();
        $('.pdfContent .save-and-print__block').remove();
        $('.pdfContent #characteristics').remove();
        $('.pdfContent .question').remove();
        $('.pdfContent .btn-consul').remove();
        $('.pdfContent .add-to-cart').remove();
        $('.pdfContent .breadcrumb').remove();

        html2pdf()
            .from("<div class='pdfContent'>" + pdfContent.html() + "</div>")
            .set(opt)
            .save(`${productName}.pdf`);

        // $('body').html("<div class='pdfContent' style='display: flex'>" + pdfContent.html() + "</div>");
        // console.log('html');
    })
});
